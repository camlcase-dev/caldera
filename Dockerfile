FROM fpco/stack-build:lts-16.31 as build
RUN mkdir -p /opt/build
COPY . /opt/build/
WORKDIR /opt/build
RUN stack build
FROM ubuntu:18.04
WORKDIR /app
RUN apt-get update && apt-get install -y \
  ca-certificates \
  libgmp-dev \
  libpq-dev \
  build-essential \
  liblzma-dev && \
  rm -rf /var/apt/lists
COPY --from=build /opt/build/sync-config.yml /app/
COPY --from=build /opt/build/.stack-work/install/x86_64-linux/*/8.8.4/bin /app/
