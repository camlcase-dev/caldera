{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -fno-warn-unused-binds -fno-warn-unused-imports #-}

module Dexter.Types (
  Contract (..),
  Error (..),
  HistoricLiquidity (..),
  HistoricPrice (..),
  HistoricVolume (..),
  Liquidity (..),
  Prices (..),
  Volume (..),
  ) where

import Data.List (stripPrefix)
import Data.Maybe (fromMaybe)
import Data.Aeson (Value, FromJSON(..), ToJSON(..), genericToJSON, genericParseJSON)
import Data.Aeson.Types (Options(..), defaultOptions)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Map as Map
import GHC.Generics (Generic)
import Data.Function ((&))


-- | 
data Contract = Contract
  { contractName :: Text -- ^ 
  , contractBigMap :: Int -- ^ 
  , contractContract :: Text -- ^ 
  , contractTokenAddress :: Text -- ^ 
  , contractManager :: Text -- ^ 
  , contractFreezeBaker :: Bool -- ^ 
  , contractLqtTotal :: Text -- ^ 
  , contractTokenPool :: Text -- ^ 
  , contractXtzPool :: Text -- ^ 
  , contractTimestamp :: Text -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON Contract where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "contract")
instance ToJSON Contract where
  toJSON = genericToJSON (removeFieldLabelPrefix False "contract")

-- | 
data Error = Error
  { errorError :: Text -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON Error where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "error")
instance ToJSON Error where
  toJSON = genericToJSON (removeFieldLabelPrefix False "error")

-- | 
data HistoricLiquidity = HistoricLiquidity
  { historicLiquidityLqtTotal :: Text -- ^ 
  , historicLiquidityTokenPool :: Text -- ^ 
  , historicLiquidityXtzPool :: Text -- ^ 
  , historicLiquidityXtzPoolValue :: Text -- ^ 
  , historicLiquidityTokenPoolValue :: Text -- ^ 
  , historicLiquidityLqtTotalValue :: Text -- ^ 
  , historicLiquidityTimestamp :: Text -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON HistoricLiquidity where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "historicLiquidity")
instance ToJSON HistoricLiquidity where
  toJSON = genericToJSON (removeFieldLabelPrefix False "historicLiquidity")

-- | 
data HistoricPrice = HistoricPrice
  { historicPriceOpen :: Float -- ^ 
  , historicPriceClose :: Float -- ^ 
  , historicPriceHigh :: Float -- ^ 
  , historicPriceLow :: Float -- ^ 
  , historicPriceVolume :: Float -- ^ 
  , historicPriceTimestamp :: Float -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON HistoricPrice where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "historicPrice")
instance ToJSON HistoricPrice where
  toJSON = genericToJSON (removeFieldLabelPrefix False "historicPrice")

-- | 
data HistoricVolume = HistoricVolume
  { historicVolumeTotal :: Text -- ^ 
  , historicVolumeSold :: Text -- ^ 
  , historicVolumeBought :: Text -- ^ 
  , historicVolumeValue :: Text -- ^ 
  , historicVolumeTimestamp :: Text -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON HistoricVolume where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "historicVolume")
instance ToJSON HistoricVolume where
  toJSON = genericToJSON (removeFieldLabelPrefix False "historicVolume")

-- | 
data Liquidity = Liquidity
  { liquidityName :: Text -- ^ 
  , liquidityContract :: Text -- ^ 
  , liquidityLqtTotal :: Text -- ^ 
  , liquidityTokenPool :: Text -- ^ 
  , liquidityXtzPool :: Text -- ^ 
  , liquidityXtzPoolValue :: Text -- ^ 
  , liquidityTokenPoolValue :: Text -- ^ 
  , liquidityLqtTotalValue :: Text -- ^ 
  , liquidityTimestamp :: Text -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON Liquidity where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "liquidity")
instance ToJSON Liquidity where
  toJSON = genericToJSON (removeFieldLabelPrefix False "liquidity")

-- | 
data Prices = Prices
  { pricesName :: Text -- ^ 
  , pricesContract :: Text -- ^ 
  , pricesUSD :: Float -- ^ 
  , pricesEUR :: Float -- ^ 
  , pricesGDP :: Float -- ^ 
  , pricesJPY :: Float -- ^ 
  , pricesCNY :: Float -- ^ 
  , pricesUSDtz :: Float -- ^ 
  , pricesXTZ :: Float -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON Prices where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "prices")
instance ToJSON Prices where
  toJSON = genericToJSON (removeFieldLabelPrefix False "prices")

-- | 
data Volume = Volume
  { volumeName :: Text -- ^ 
  , volumeContract :: Text -- ^ 
  , volumeTotal :: Text -- ^ 
  , volumeSold :: Text -- ^ 
  , volumeBought :: Text -- ^ 
  , volumeValue :: Text -- ^ 
  , volumeTimestamp :: Text -- ^ 
  } deriving (Show, Eq, Generic)

instance FromJSON Volume where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "volume")
instance ToJSON Volume where
  toJSON = genericToJSON (removeFieldLabelPrefix False "volume")

-- Remove a field label prefix during JSON parsing.
-- Also perform any replacements for special characters.
removeFieldLabelPrefix :: Bool -> String -> Options
removeFieldLabelPrefix forParsing prefix =
  defaultOptions
  {fieldLabelModifier = fromMaybe (error ("did not find prefix " ++ prefix)) . stripPrefix prefix . replaceSpecialChars}
  where
    replaceSpecialChars field = foldl (&) field (map mkCharReplacement specialChars)
    specialChars =
      [ ("@", "'At")
      , ("\\", "'Back_Slash")
      , ("<=", "'Less_Than_Or_Equal_To")
      , ("\"", "'Double_Quote")
      , ("[", "'Left_Square_Bracket")
      , ("]", "'Right_Square_Bracket")
      , ("^", "'Caret")
      , ("_", "'Underscore")
      , ("`", "'Backtick")
      , ("!", "'Exclamation")
      , ("#", "'Hash")
      , ("$", "'Dollar")
      , ("%", "'Percent")
      , ("&", "'Ampersand")
      , ("'", "'Quote")
      , ("(", "'Left_Parenthesis")
      , (")", "'Right_Parenthesis")
      , ("*", "'Star")
      , ("+", "'Plus")
      , (",", "'Comma")
      , ("-", "'Dash")
      , (".", "'Period")
      , ("/", "'Slash")
      , (":", "'Colon")
      , ("{", "'Left_Curly_Bracket")
      , ("|", "'Pipe")
      , ("<", "'LessThan")
      , ("!=", "'Not_Equal")
      , ("=", "'Equal")
      , ("}", "'Right_Curly_Bracket")
      , (">", "'GreaterThan")
      , ("~", "'Tilde")
      , ("?", "'Question_Mark")
      , (">=", "'Greater_Than_Or_Equal_To")
      ]
    mkCharReplacement (replaceStr, searchStr) = T.unpack . replacer (T.pack searchStr) (T.pack replaceStr) . T.pack
    replacer =
      if forParsing
        then flip T.replace
        else T.replace
