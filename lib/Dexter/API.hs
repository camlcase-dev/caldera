{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC
-fno-warn-unused-binds -fno-warn-unused-imports -fcontext-stack=328 #-}

module Dexter.API
  -- * Client and Server
  ( ServerConfig(..)
  , DexterBackend
  , createDexterClient
  , runDexterServer
  , runDexterClient
  , runDexterClientWithManager
  , DexterClient
  -- ** Servant
  , DexterAPI
  ) where

import Dexter.Types

import Control.Monad.Except (ExceptT)
import Control.Monad.IO.Class
import Data.Aeson (Value)
import Data.Coerce (coerce)
import Data.Function ((&))
import qualified Data.Map as Map
import Data.Monoid ((<>))
import Data.Proxy (Proxy(..))
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Exts (IsString(..))
import GHC.Generics (Generic)
import Network.HTTP.Client (Manager, defaultManagerSettings, newManager)
import Network.HTTP.Types.Method (methodOptions)
import qualified Network.Wai.Handler.Warp as Warp
import Servant (ServantErr, serve)
import Servant.API
import Servant.API.Verbs (StdMethod(..), Verb)
import Servant.Client (Scheme(Http), ServantError, client)
import Servant.Common.BaseUrl (BaseUrl(..))
import Web.HttpApiData




-- For the form data code generation.
lookupEither :: FromHttpApiData b => Text -> [(Text, Text)] -> Either String b
lookupEither key assocs =
  case lookup key assocs of
    Nothing -> Left $ "Could not find parameter " <> (T.unpack key) <> " in form data"
    Just value ->
      case parseQueryParam value of
        Left result -> Left $ T.unpack result
        Right result -> Right $ result

-- | Servant type-level API, generated from the Swagger spec for Dexter.
type DexterAPI
    =    "contracts" :> QueryParam "block" Text :> QueryParam "cycle" Int :> Verb 'GET 200 '[JSON] [Contract] -- 'contracts' route
    :<|> "contracts" :> Capture "contractID" Text :> QueryParam "block" Text :> QueryParam "cycle" Int :> Verb 'GET 200 '[JSON] Contract -- 'contractsContractIDGet' route
    :<|> "liquidity" :> Capture "contractID" Text :> QueryParam "block" Text :> QueryParam "cycle" Int :> Verb 'GET 200 '[JSON] [Liquidity] -- 'liquidityContractIDGet' route
    :<|> "liquidity" :> QueryParam "block" Text :> QueryParam "cycle" Int :> Verb 'GET 200 '[JSON] [Liquidity] -- 'liquidityGet' route
    :<|> "liquidity" :> "historic" :> Capture "contractID" Text :> QueryParam "interval" Text :> QueryParam "currency" Text :> QueryParam "from" Text :> QueryParam "to" Text :> Verb 'GET 200 '[JSON] [HistoricLiquidity] -- 'liquidityHistoricContractIDGet' route
    :<|> "prices" :> Capture "fa12ID" Text :> Verb 'GET 200 '[JSON] Prices -- 'pricesFa12IDGet' route
    :<|> "prices" :> Verb 'GET 200 '[JSON] [Prices] -- 'pricesGet' route
    :<|> "prices" :> "historic" :> Capture "fa12ID" Text :> QueryParam "interval" Text :> QueryParam "currency" Text :> QueryParam "from" Text :> QueryParam "to" Text :> Verb 'GET 200 '[JSON] [HistoricPrice] -- 'pricesHistoricFa12IDGet' route
    :<|> "volume" :> Capture "contractID" Text :> QueryParam "block" Text :> QueryParam "cycle" Int :> Verb 'GET 200 '[JSON] Volume -- 'volumeContractIDGet' route
    :<|> "volume" :> QueryParam "block" Text :> QueryParam "cycle" Int :> Verb 'GET 200 '[JSON] [Volume] -- 'volumeGet' route
    :<|> "volume" :> "historic" :> Capture "contractID" Text :> QueryParam "interval" Text :> QueryParam "currency" Text :> Verb 'GET 200 '[JSON] Volume -- 'volumeHistoricContractIDGet' route

-- | Server or client configuration, specifying the host and port to query or serve on.
data ServerConfig = ServerConfig
  { configHost :: String  -- ^ Hostname to serve on, e.g. "127.0.0.1"
  , configPort :: Int      -- ^ Port to serve on, e.g. 8080
  } deriving (Eq, Ord, Show, Read)

-- | List of elements parsed from a query.
newtype QueryList (p :: CollectionFormat) a = QueryList
  { fromQueryList :: [a]
  } deriving (Functor, Applicative, Monad, Foldable, Traversable)

-- | Formats in which a list can be encoded into a HTTP path.
data CollectionFormat
  = CommaSeparated -- ^ CSV format for multiple parameters.
  | SpaceSeparated -- ^ Also called "SSV"
  | TabSeparated -- ^ Also called "TSV"
  | PipeSeparated -- ^ `value1|value2|value2`
  | MultiParamArray -- ^ Using multiple GET parameters, e.g. `foo=bar&foo=baz`. Only for GET params.

instance FromHttpApiData a => FromHttpApiData (QueryList 'CommaSeparated a) where
  parseQueryParam = parseSeparatedQueryList ','

instance FromHttpApiData a => FromHttpApiData (QueryList 'TabSeparated a) where
  parseQueryParam = parseSeparatedQueryList '\t'

instance FromHttpApiData a => FromHttpApiData (QueryList 'SpaceSeparated a) where
  parseQueryParam = parseSeparatedQueryList ' '

instance FromHttpApiData a => FromHttpApiData (QueryList 'PipeSeparated a) where
  parseQueryParam = parseSeparatedQueryList '|'

instance FromHttpApiData a => FromHttpApiData (QueryList 'MultiParamArray a) where
  parseQueryParam = error "unimplemented FromHttpApiData for MultiParamArray collection format"

parseSeparatedQueryList :: FromHttpApiData a => Char -> Text -> Either Text (QueryList p a)
parseSeparatedQueryList char = fmap QueryList . mapM parseQueryParam . T.split (== char)

instance ToHttpApiData a => ToHttpApiData (QueryList 'CommaSeparated a) where
  toQueryParam = formatSeparatedQueryList ','

instance ToHttpApiData a => ToHttpApiData (QueryList 'TabSeparated a) where
  toQueryParam = formatSeparatedQueryList '\t'

instance ToHttpApiData a => ToHttpApiData (QueryList 'SpaceSeparated a) where
  toQueryParam = formatSeparatedQueryList ' '

instance ToHttpApiData a => ToHttpApiData (QueryList 'PipeSeparated a) where
  toQueryParam = formatSeparatedQueryList '|'

instance ToHttpApiData a => ToHttpApiData (QueryList 'MultiParamArray a) where
  toQueryParam = error "unimplemented ToHttpApiData for MultiParamArray collection format"

formatSeparatedQueryList :: ToHttpApiData a => Char ->  QueryList p a -> Text
formatSeparatedQueryList char = T.intercalate (T.singleton char) . map toQueryParam . fromQueryList


-- | Backend for Dexter.
-- The backend can be used both for the client and the server. The client generated from the Dexter Swagger spec
-- is a backend that executes actions by sending HTTP requests (see @createDexterClient@). Alternatively, provided
-- a backend, the API can be served using @runDexterServer@.
data DexterBackend m = DexterBackend
  { contracts :: Maybe Text -> Maybe Int -> m [Contract]{- ^  -}
  , contractsContractIDGet :: Text -> Maybe Text -> Maybe Int -> m Contract{- ^  -}
  , liquidityContractIDGet :: Text -> Maybe Text -> Maybe Int -> m [Liquidity]{- ^  -}
  , liquidityGet :: Maybe Text -> Maybe Int -> m [Liquidity]{- ^  -}
  , liquidityHistoricContractIDGet :: Text -> Maybe Text -> Maybe Text -> Maybe Text -> Maybe Text -> m [HistoricLiquidity]{- ^  -}
  , pricesFa12IDGet :: Text -> m Prices{- ^  -}
  , pricesGet :: m [Prices]{- ^  -}
  , pricesHistoricFa12IDGet :: Text -> Maybe Text -> Maybe Text -> Maybe Text -> Maybe Text -> m [HistoricPrice]{- ^  -}
  , volumeContractIDGet :: Text -> Maybe Text -> Maybe Int -> m Volume{- ^  -}
  , volumeGet :: Maybe Text -> Maybe Int -> m [Volume]{- ^  -}
  , volumeHistoricContractIDGet :: Text -> Maybe Text -> Maybe Text -> m Volume{- ^  -}
  }

newtype DexterClient a = DexterClient
  { runClient :: Manager -> BaseUrl -> ExceptT ServantError IO a
  } deriving Functor

instance Applicative DexterClient where
  pure x = DexterClient (\_ _ -> pure x)
  (DexterClient f) <*> (DexterClient x) =
    DexterClient (\manager url -> f manager url <*> x manager url)

instance Monad DexterClient where
  (DexterClient a) >>= f =
    DexterClient (\manager url -> do
      value <- a manager url
      runClient (f value) manager url)

instance MonadIO DexterClient where
  liftIO io = DexterClient (\_ _ -> liftIO io)

createDexterClient :: DexterBackend DexterClient
createDexterClient = DexterBackend{..}
  where
    ((coerce -> contracts) :<|>
     (coerce -> contractsContractIDGet) :<|>
     (coerce -> liquidityContractIDGet) :<|>
     (coerce -> liquidityGet) :<|>
     (coerce -> liquidityHistoricContractIDGet) :<|>
     (coerce -> pricesFa12IDGet) :<|>
     (coerce -> pricesGet) :<|>
     (coerce -> pricesHistoricFa12IDGet) :<|>
     (coerce -> volumeContractIDGet) :<|>
     (coerce -> volumeGet) :<|>
     (coerce -> volumeHistoricContractIDGet)) = client (Proxy :: Proxy DexterAPI)

-- | Run requests in the DexterClient monad.
runDexterClient :: ServerConfig -> DexterClient a -> ExceptT ServantError IO a
runDexterClient clientConfig cl = do
  manager <- liftIO $ newManager defaultManagerSettings
  runDexterClientWithManager manager clientConfig cl

-- | Run requests in the DexterClient monad using a custom manager.
runDexterClientWithManager :: Manager -> ServerConfig -> DexterClient a -> ExceptT ServantError IO a
runDexterClientWithManager manager clientConfig cl =
  runClient cl manager $ BaseUrl Http (configHost clientConfig) (configPort clientConfig) ""

-- | Run the Dexter server at the provided host and port.
runDexterServer :: MonadIO m => ServerConfig -> DexterBackend (ExceptT ServantErr IO)  -> m ()
runDexterServer ServerConfig{..} backend =
  liftIO $ Warp.runSettings warpSettings $ serve (Proxy :: Proxy DexterAPI) (serverFromBackend backend)
  where
    warpSettings = Warp.defaultSettings & Warp.setPort configPort & Warp.setHost (fromString configHost)
    serverFromBackend DexterBackend{..} =
      (coerce contracts :<|>
       coerce contractsContractIDGet :<|>
       coerce liquidityContractIDGet :<|>
       coerce liquidityGet :<|>
       coerce liquidityHistoricContractIDGet :<|>
       coerce pricesFa12IDGet :<|>
       coerce pricesGet :<|>
       coerce pricesHistoricFa12IDGet :<|>
       coerce volumeContractIDGet :<|>
       coerce volumeGet :<|>
       coerce volumeHistoricContractIDGet)