module Main where

import Caldera.API.Routes (writeSwaggerJSON)

main :: IO ()
main = writeSwaggerJSON
