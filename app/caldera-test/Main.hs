{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
module Main where

import Data.Proxy
import Data.Text
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant.API
import Servant.Client
import Network.HTTP.Client (Manager)
import Network.HTTP.Client.TLS    (tlsManagerSettings)
import Servant.Client (BaseUrl(BaseUrl), Scheme, ClientM, ClientError, mkClientEnv, runClientM)
import Caldera.Clients.Generic ( runClient, ClientConfig )
-- Tezos Servant Client
import Tezos.Client.Types(ChainId(ChainIdMain), BlockId(BlockIdHead), Block)
import Tezos.Client.Protocol.Alpha (GetBlock)

--tezosHost = "mainnet.camlcase.io"
tezosHost = "mainnet-tezos.giganode.io"
tezosPort = 443

myCall = getBlock ChainIdMain BlockIdHead

getBlock :: ChainId -> BlockId -> ClientM Block
getBlock  = client (Proxy :: Proxy GetBlock)

main :: IO ()
main = do
  manager' <- newManager tlsManagerSettings
  res <- runClientM myCall
       $ mkClientEnv manager'
       $ BaseUrl Https tezosHost tezosPort ""
  case res of
    Left err -> putStrLn $ "Error: " ++ show err
    Right books -> print books


--getBlock :: ChainId -> BlockId -> ClientM Block
--getBlock = client (Proxy :: Proxy GetBlock)

--requestBlock :: ClientConfig -> BlockId -> IO (Either ClientError Block)
--requestBlock config blockID = runClient config



