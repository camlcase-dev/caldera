module Main where

import CalderaSync (start)
import Logging.Logger (linfo)

main :: IO ()
main = do
  start
  linfo "Started Caldera Indexing."
