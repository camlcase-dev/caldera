#!/bin/bash


build() {
(cd ..;
graphmod -i src/                         \
         -i app                          \
         -i calderaWorker                \
         --remove-module=Caldera.Config  \
         --collapse-module=Caldera.Database     \
         --collapse-module=Clients.Tezos \
         --collapse-module=Clients.Gecko \
         --remove-module=Logging.Logger > docs/calderaModules.dot ) && \
dot -Tpdf calderaModules.dot -o calderaModules.pdf
pandoc \
       --filter pandoc-filter-graphviz \
       --filter pandoc-plantuml-diagrams \
       \
       architecture.md -o architecture.pdf
}

build;

while inotifywait architecture.md ; do build; done
