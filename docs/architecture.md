---
title: Caldera architecture documentation
author: Michał J. Gajda
date: |
  ```{=latex}
  \today
  ```
---

This document documents the current Caldera as-is to ensure
easy onboarding, and a single place for architectural discussion.

This is status as of `caldera` repository commit `92fef4a0148f49e9f0629d9078a27b37f173e8e1`.

# Data flow

Here is a diagram of data flow in the Caldera:

```{=latex}
\begin{figure}
```

```dot
digraph Caldera {
  rankdir=TD;
  node [shape=plain];

  database [label="PostgresSQL\ndatabase"];


  subgraph data_sources {
    label="Data sources";
    dexter [label="Dexter exchange API"];
    tezosNode [label="Tezos\nnode"];
    coinGecko [label="CoinGecko"];
    rank=same;
  }

  subgraph cluster_worker {
    label="Worker process";
    blockCache [label="Block cache"];
    subgraph workers {
      contractStorageWorker [label="Contract storage worker"];
      operationsWorker [label="Operations worker"];
      blockWorker [label="Block worker"];
      priceWorker [label="Fiat prices worker"];
      liquidityWorker [label="Liquidity worker",shape=box,style=dashed];
      rank=same;
    }
  }

  subgraph cluster_apiServer {
    label="API server process";
    shape=box;
    subgraph cluster_queryCache {
      label="Database query cache";
      cache [label="Query\ncache"];
    }
    stats [label="Compute statistics"];
    endpoints [label="API\nendpoints"];
    subgraph cluster_responseCache {
      label="API response cache";
      liquidityCache [label="Liquidity cache"];
    }
  }
  frontend [label="Web frontend"];
  
  tezosNode -> blockCache -> operationsWorker;
  blockCache -> contractStorageWorker;
  dexter -> liquidityWorker -> database;
  dexter -> operationsWorker -> database;
  dexter -> contractStorageWorker -> database;
  blockCache -> blockWorker -> database;
  coinGecko -> priceWorker -> database;
  database -> cache [label="query data"];
  cache -> stats -> liquidityCache -> endpoints;
  endpoints -> frontend;
  liquidityRewards [label="Liquidity\nrewards\n", shape=box, style=dashed];
  liquidityRewards -> frontend [style=dashed];
  jsonBenchmarks [label="JSON\nbenchmarks", shape=box, style=dashed];
  jsonBenchmarks -> cache [style=invis];
}
```

```{=latex}
\caption{Overview of data flow in Caldera}
\end{figure}
```


**Note:** Performance was bad without query cache, since the same block is requested multiple times by the server.
Possibly multiple times when for each request.

We start by running `calderaWorker/Main.hs` that queries Tezos node and inserts blocks into the SQL database for storage and fast querying.

Whenever API request arrives, we pull all the necessary blocks and summary data,
summarize them and present a result to the user.

Frontend is developed in [caldera-frontend repository](https://gitlab.com/camlcase-dev/caldera-frontend).

## Rationale

Querying Tezos blockchain would take a long time, and Dexter should not be overloaded,
so we fill the database with information for computing statistics.
We also use a fixed database schema, since we can re-query the data
and insert it into the database for each new version.

Filling database from scratch took about two days.
Most of the time was spent on XTZ to FA1.2 contract storage.
CoinGecko takes about 10 minutes.

# Code placement

```{=latex}
\begin{figure}
```

```{.uml}
@startsalt
{
{T
+
++ app
+++ Main.hs - <i> API server
++ caldera-liquidity - <i>liquidity API <b>will be merged into main server
+++ Main.hs
++ calderaWorker
+++ Main.hs - <i>database workers <b>move to app/worker
++ caldera-worker-benchmark
+++ Main.hs - benchmark <b>move to bench/serialization
++ db - <b>To be removed
+++ price.sql
++ deployment - <i>Kubernetes definitions for dev and prod environments
++ docs
+++ architecture.md - <i>this document
++ lib - Dexter contract storage parsing
+++ Dexter
++++ API.hs
++++ Types.hs
++ migrations
+++ 1_initailize_schema.up.sql -- database schema
++ src
+++ Caldera
++++ Config.hs - <i>Config shared between API server and workers
++++ Database - <i>Shared database connection handling
++++ API - <i>API server
+++++ Cache -- <i>Query caches
+++++ ContractStorage.hs
+++++ Liquidity.hs
+++++ Prices.hs
+++++ Routes.hs - <i>Server API routes
+++++ Server.hs
+++++ Types.hs
+++++ Volume.hs
++++ Clients
+++++ Gecko - <i>calls to CoinGecko API
+++++ Tezos - <i>calls to Tezos node API
+++++ AddLiquidityTable - Liquidity table construction
+++++ Connection.hs - <i>DB connection management
+++++  "*/SQL.hs" - <i>queries
++++ Synchronizers - <i>worker threads
+++ CalderaAPI.hs
+++ CalderaSync.hs - <i>worker main
+++ Dexter - <i>connecting to Dexter contract storage
++++ Contract
+++++ Storage.hs
++++ Exchange
+++++ Transaction
++++++ Parameters.hs
+++++ Transaction.hs
++++ Exchange.hs
+++ LiquidityRewards.hs
+++ Logging - <i> logging utilities
++ test - <i> test suite
+++ Aeson
++++ Hspec.hs - <i>serialization tests
+++ "*.json" - <i>serialized test inputs
+++ Spec
++++ Dexter
+++++ Contract
++++++ Storage.hs - <b>Tezos node deserialization benchmarks
+++++ LiquidityRewards.hs - <i> unit tests for Liquity rewards stats
}
}
@endsalt
```

```{=latex}
\caption{Directory structure}
\end{figure}
```

Dexter API
  : Used for exchange rates between XTZ and other coins?

    Is in `lib/Dexter`.

Caldera worker
  : Pulls information from all APIs and stores it into PG SQL database.

    - Main function is in `calderaWorker/Main.hs`.
    - Most of the source code is in `src/Caldera/Synchronizers`.

      It refers to the API code wrappers in `src/Caldera/Clients`:
      
        - [Tezos](https://gitlab.com/camlcase-dev/hs-tezos-client)
        - [Dexter](https://gitlab.com/camlcase-dev/dexter)
        - For fiat currency values: [CoinGecko](https://www.coingecko.com/en/api)

    - Kubernetes pod configuration is in:
      
      `deployment/{dev|prod}/worker-deployment.yml`.

Caldera API server
  : Web API server that responds to frontend requests:

    - Main function is in `app/Main.hs`.
    - Most of the source code is in `src/Caldera/API`.
    - Kubernetes pod configuration is in:
      
      `deployment/{dev|prod}/api-deployment.yml`

Shared libraries
  : `Logging.Logger` contains shared logging abstractions.

Database code
  : Is placed under:

    - SQL Database schema is currently in:
    
      `migrations/1_initialize_schema.up.sql`

    To be replaced by `beam-core` schema.
    The database binding functions are in `src/Caldera/Database`.

Majority of data about Dexter goes from filtered Tezos node query.
Initial idea was to use `indexter`, but they tend to flatten out too much _parent-child_
relationships at the moment.

![Module dependencies](calderaModules.pdf)

### Database schema

Master table is `dexter_operation`. Index `block_hash` is just hash of the block header.

Contract information is indexed by triple:
- `block_level`
- `operation_x`
- `operation_y`

TODO: put the DB schema diagram here.

### Benchmarks

Serialization benchmarks are in `caldera-worker-benchmarks`, but serialization does not seem to be critical for performance.


Liquidity rewards endpoint
  : Temporarily in `LiquidityRewards.hs`, liquidity for any particular account, will be API for institutional providers
# Worker threads


Most workers are divided into two threads:
* _start block_ worker -- periodically queries for the new start block
* _missing block worker_ -- that periodically re-queries all blocks that are marked as _missing_ due to network errors

Worker threads live in `src/Caldera/Synchronizers`:

Operations in `Operations.Worker`
  : Synchronizes operations from Dexter API.

    Queries operations data through Dexter.


Contract in `ContractStorage.Worker`
  : Synchronizes contract storage from Dexter API using two sub-threads:

    - `startContract` -- synchronizes contract storage
    - `startMissing'` -- synchronizes missing and erroneous contract store data (**Q**)

    Both threads use the same processing.
    There is a pair of these threads **for each contract**.

Block cache in `Block.Cache`
  : Loads data from Tezos blockchain using three subthreads:
  
    - `purge` -- purge blocks that are below the last known block
    - `processMissingBlocks` -- pull missing blocks
    - `processHeadBlocks` -- grabs head block and puts it in cache

Price worker in `DailyPrices.Worker`
  : Loads fiat prices from CoinGecko in a single thread `startPriceWorker`. Takes <10 minutes.

# Caches

## Worker cache

Workers only use block cache.

## Query caches

Query caches for database queries:

- Block
- Price
- Volume
- LiquidityCache

# Frontend and API endpoints usage

The following endpoints are used in [caldera-frontend](https://gitlab.com/camlcase-dev/caldera-frontend)
^[as of commit `886ae6dfbcb4325aaff16e8181b5f8f95ada6d89`]:

```{.uml}
@startsalt
{
{T
+
++ src
+++ types
++++ Contract.re - /contracts
++++ Currency
+++++ HistoricalPrice.re - /prices/:currency?interval=all&timeframe=all
+++ services
++++ Api
+++++ GraphData.re - prices/:currency?timeframe=all
+++++ Data.re - /:contract/current
+++ components
++++ Content
+++++ ContentStats
++++++ Content
+++++++ ContentStatsPool
++++++++ ContentStatsOverallLiquidity.re - /liquidity/:contract?/current
++++++++ ContentStatsOverallVolume.re - /volume/:contract?/current
++++ Subcontent
+++++ GraphVolume.re - /volume/:contract?timeframe=[1w,1M,1y,all] also (start,end)
+++++ GraphLiquidity.re - /liquidity/:contract?timeframe=[1w,1M,1y,all] also (start,end)
++++ Navbar
+++++ Navbar.re - /liquidity/current and /volume/current
}
}
@endsalt
```


# Plans for the improvement

## Already prioritized

1. Switch to `beam`, so we avoid generating SQL schema
2. Test everything we can with `QuickCheck`, and mock the endpoints:

    - Property testing of all type laws.
    - provide serialization/deserialization instance checks using `quickcheck-classes`
    - provide test scenarios for each function
    - provide error handling examples in the tests
    - mock a monad (this may require abstracting most of the standard operations into a `Caldera` and `CalderaAPI` monads first)

## Low-hanging fruits to improve the code

Things that can be implemented without tests:

- Use `app/` subdirectories for all deployed executables.
- Use `bench/` subdirectories for all benchmarks.
- Add export lists to every module.
- Add thread names with debug function `labelThread`.
- Remove unused:
  * `caldera-worker-benchmarks`
  * `price.sql`
  * `caldera-stats` repository?
  * `price.sql`
- Add `hlint`, `homplexity`, and a style checker on CI.
- Use `COMMENT '...'` instead of `/* ... */` to attach comments in SQL to a particular column or table.

## After test coverage goes above 80%

- Change all huge numbers from `String` to:
  ```{.haskell}
  newtype HugeInt = HugeInt Integer
  ```
  That is serialized into an SQL `VARCHAR(100)` type. 
  Reverse the order of digits upon serialization, and and them with `'.'`.
  This will preserve ordering for SQL queries.
  The huge integers represent large numbers of variable length.
  We may use `VARCHAR(100)` to store them in PG SQL database,
  but we need to reverse order of digits, and and them with `'.'` in order to preserve
  ordering for SQL queries.

  On the SQL side we [define new type](https://www.postgresql.org/docs/9.5/sql-createdomain.html) as:
  ```{.sql}
  CREATE DOMAIN HugeInt AS VARCHAR(100);
  ```
- Refactor logging, configuration into a single monad that:
  * holds name of current thread
  * holds current stage of computation
  * allows to log just the message
- Provide a monad interface for:
  - logging abstractions
  - common state:
      * logger,
      * current processing stage
      * thread name
  - automatically providing error message from `beam` query
- Update to latest `Stackage` for the compiler version
  (_pursue this only if no code changes required_.)
- Split configuration into worker and server part.

## Improvements to consider 

A bit more work, but to great rewards:

- Use foreign keys to document relations 
- Convert SQL queries to typed query library like `esqueleto`
- Benchmark performance-sensitive queries, and make self-contained `beam` queries
  (using typed query library like `esqueleto` or similar)
- Cleanup big records and change them to `Data.Map`
- Generate `swagger.yaml` from `servant` endpoint types to keep them in sync.
- make indexter more generic to hold all _parent-child_ relationships between blocks
- replace returning code and error message with a documented exception datatype
- add check that each exception and message is only thrown in a single site within code

## Already done:

- GitLabCI
- Architecture documentation
