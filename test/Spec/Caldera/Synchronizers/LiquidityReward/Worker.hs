{-|
Module      : Spec.Caldera.Synchronizers.LiquidityReward.Worker
Description : Unit tests for getting Liquidity Positions and Rewards from the database
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Spec.Caldera.Synchronizers.LiquidityReward.Worker where

-- base
import Prelude hiding (readFile)
import Data.Either (rights)

-- serialization
import Data.Aeson

-- testing
import Test.Hspec

-- tezos
import qualified Tezos.Client.Types.Block as Block

-- string
import Data.ByteString.Lazy (readFile)
import Text.Pretty.Simple (pPrint)

-- caldera
import Spec.Dexter.Exchange (tzBTC)
import Caldera.Database.Table.DexterOperation (DexterOperation)
import Caldera.Database.Table.LiquidityPosition
import Caldera.Synchronizers.DexterOperation.Worker
import Caldera.Synchronizers.LiquidityReward.Worker (getLiquidityPosition)

prettyFail :: Show a => a -> IO b
prettyFail a = do pPrint a; fail ""

dexterOperationsFromBlock :: FilePath -> IO [DexterOperation]
dexterOperationsFromBlock fp = do
  file <- readFile fp
  block <- either prettyFail pure $ eitherDecode' file :: IO Block.Block
  pure $ rights $ getDexterOperationForBlock [tzBTC] block

spec :: Spec
spec = do
  describe "getLiquidityPosition" $ do    
    it "addLiquidity" $ do
      -- a user adds liquidity for the first time
      dexterOperations <- dexterOperationsFromBlock "golden/block_1280862.json"
      let liquidityPositions = concat $ rights $ fmap getLiquidityPosition dexterOperations
      liquidityPositions `shouldBe`
        [LiquidityPosition
          { lpLiquidityPositionId = 0
          , lpAccountId           = "tz1W4soNxmZCzHiNdawbcKJ6UVfLuR5R9dww"
          , lpLqt                 = "3277087282"
          , lpDexterOperationId   = 0
          }]

    it "removeLiquidity" $ do
      -- a user removes all liquidity from dexter
      dexterOperations <- dexterOperationsFromBlock "golden/block_1337625.json"
      let liquidityPositions = concat $ rights $ fmap getLiquidityPosition dexterOperations
      liquidityPositions `shouldBe`
        [LiquidityPosition
          { lpLiquidityPositionId = 0
          , lpAccountId           = "tz1SRqikWtR2KNpugCApreA9QeZuqh8swQMa"
          , lpLqt                 = "0"
          , lpDexterOperationId   = 0
          }]
