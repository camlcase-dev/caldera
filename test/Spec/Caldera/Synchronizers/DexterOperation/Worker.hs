{-|
Module      : Spec.Caldera.Synchronizers.DexterOperation.Worker
Description : Unit tests for parsing DexterOperation from the database
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Spec.Caldera.Synchronizers.DexterOperation.Worker where

-- base
import Prelude hiding (readFile)
import Data.Either (lefts, rights)

-- serialization
import Data.Aeson

-- testing
import Test.Hspec

-- tezos
import qualified Tezos.Client.Types.Block as Block

-- string
import Data.ByteString.Lazy (readFile)
import Text.Pretty.Simple (pPrint)

-- caldera
import Spec.Dexter.Exchange (tzBTC, usdTz)
import Caldera.Synchronizers.DexterOperation.Worker

prettyFail :: Show a => a -> IO b
prettyFail a = do pPrint a; fail ""

dexterOperationFromBlock :: FilePath -> IO ()
dexterOperationFromBlock fp = do
  file <- readFile fp
  block <- either prettyFail pure $ eitherDecode' file :: IO Block.Block
  let operations = getDexterOperationForBlock [tzBTC, usdTz] block
  (length $ rights operations) `shouldBe` 1

-- | Run this to inspect failing values
dexterOperationFromBlockDebug :: FilePath -> IO ()
dexterOperationFromBlockDebug fp = do
  file <- readFile fp
  block <- either prettyFail pure $ eitherDecode' file :: IO Block.Block
  let operations = getDexterOperationForBlock [tzBTC, usdTz] block
  pPrint (lefts operations)
  (length $ rights operations) `shouldBe` 1

spec :: Spec
spec = do
  describe "getDexterOperationForBlock tzBTC (delphi)" $ do
    it "addLiquidity" $
      dexterOperationFromBlock "golden/block_1253664.json"

    it "xtzToToken" $
      dexterOperationFromBlock "golden/block_1257788.json"

    it "tokenToXtz" $
      dexterOperationFromBlock "golden/block_1256652.json"

    it "removeLiquidity" $
      dexterOperationFromBlock "golden/block_1255568.json"

    it "tokenToToken" $
      dexterOperationFromBlock "golden/block_1315901.json"

  describe "getDexterOperationForBlock tzBTC (edo)" $ do
    it "addLiquidity" $
      dexterOperationFromBlock "golden/block_1348898.json"      

    it "xtzToToken" $
      dexterOperationFromBlock "golden/block_1348912.json"

    it "xtzToToken" $
      dexterOperationFromBlock "golden/block_1349948.json"

    it "tokenToXtz" $
      dexterOperationFromBlock "golden/block_1348892.json"

    it "removeLiquidity" $
      dexterOperationFromBlock "golden/block_1348353.json"

    it "tokenToToken" $
      dexterOperationFromBlock "golden/block_1348939.json"      
