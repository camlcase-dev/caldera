{-|
Module      : Spec.Caldera.Database.Query.DexterOperation
Description : Unit tests for parsing DexterOperation from the database
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Spec.Caldera.Database.Query.DexterOperation where

-- base
import Prelude hiding (readFile)
import Data.Either (isRight, lefts, rights)
import Safe (headMay)

-- serialization
import Data.Aeson

-- testing
import Test.Hspec

-- tezos
import qualified Tezos.Client.Types.Block as Block

-- string
import Data.ByteString.Lazy (readFile)
import Text.Pretty.Simple (pPrint)

-- caldera
import Spec.Dexter.Exchange (tzBTC, usdTz)
import Caldera.Synchronizers.DexterOperation.Worker
import Caldera.Database.Query.DexterOperation (parseDexterTransaction)

prettyFail :: Show a => a -> IO b
prettyFail a = do pPrint a; fail ""

-- | Read a block json file from the Tezos node. Parse an DexterOperation
-- out of it, then parse a DexterTransaction from it.
dexterOperationToTransaction :: FilePath -> IO ()
dexterOperationToTransaction fp = do
  file <- readFile fp
  block <- either prettyFail pure $ eitherDecode' file :: IO Block.Block
  let operations = getDexterOperationForBlock [tzBTC, usdTz] block
  operation <- maybe (fail "no transaction") pure $ headMay $ rights operations
  isRight (parseDexterTransaction operation) `shouldBe` True

-- | Run this to inspect failing values
dexterOperationToTransactionDebug :: FilePath -> IO ()
dexterOperationToTransactionDebug fp = do
  file <- readFile fp
  block <- either prettyFail pure $ eitherDecode' file :: IO Block.Block
  let operations = getDexterOperationForBlock [tzBTC, usdTz] block
  pPrint $ lefts operations
  operation <- maybe (fail "no transaction") pure $ headMay $ rights operations
  pPrint $ parseDexterTransaction operation
  isRight (parseDexterTransaction operation) `shouldBe` True

spec :: Spec
spec = do
  describe "parseDexterTransaction tzBTC (delphi)" $ do    
    it "addLiquidity DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1253664.json"

    it "xtzToToken DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1257788.json"

    it "tokenToXtz DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1256652.json"

    it "removeLiquidity DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1255568.json"

    it "tokenToToken DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1315901.json"

  describe "parseDexterTransaction tzBTC (edo)" $ do    
    it "addLiquidity DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1348898.json"      

    it "xtzToToken DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1348912.json"

    it "tokenToXtz DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1348892.json"

    it "removeLiquidity DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1348353.json"

    it "tokenToToken DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1348939.json"      

  describe "parseDexterTransaction USDtz (edo)" $ do
    it "addLiquidity DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1350069.json"

    it "xtzToToken DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1350105.json"

    it "xtzToToken" $
      dexterOperationToTransaction "golden/block_1349948.json"      

    it "tokenToXtz DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1349897.json"

    it "removeLiquidity DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1350069.json"

    it "tokenToToken DexterOperation to DexterTransction" $
      dexterOperationToTransaction "golden/block_1348781.json"
