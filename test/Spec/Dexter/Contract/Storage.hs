{-|
Module      : Spec.Dexter.Contract.Storage
Description : Unit tests for parsing Dexter data from Tezos Blocks
Copyright   : (c) camlCase, 2020-2021
Maintainer  : james@camlcase.io

We have collected some examples of Dexter data in the Tezos mainnet blocks.
These tests make sure we can collect the expected Dexter data.

-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Spec.Dexter.Contract.Storage where

-- base
import Prelude hiding (readFile)
import Data.Maybe (isJust)

-- serialization
import Aeson.Hspec

-- testing
import Test.Hspec

-- tezos
import Tezos.Client.Types.Core (mkMutez)

-- caldera
import qualified Tezos.Client.Types.Core as CoreTypes
import Dexter.Contract.Storage 


spec :: Spec
spec = do
  describe "parseBytes" $ do
    it "parseBytes for Tezos bytes encoding" $ do
      isJust (CoreTypes.parseBytes "01813a19aa1cb96fe5b039c0bfc1633e61daf0ae3f00") `shouldBe` True
      isJust (CoreTypes.parseBytes "01a3d0f58d8964bd1b37fb0a0c197b38cf46608d4900") `shouldBe` True
      isJust (CoreTypes.parseBytes "01a819ed5da3593505f8085b2cb11a85fe5be237c400") `shouldBe` True
      isJust (CoreTypes.parseBytes "0139c8ade2617663981fa2b87592c9ad92714d14c200") `shouldBe` True
      isJust (CoreTypes.parseBytes "0000a119f3bc66d759392a31fb233b42239d9785f260") `shouldBe` True
      isJust (CoreTypes.parseBytes "0000ee9a456171bb3b5d89894679880f7c3e3a436c53") `shouldBe` True
      isJust (CoreTypes.parseBytes "0000a119f3bc66d759392a31fb233b42239d9785f260") `shouldBe` True

  describe "expressionToStorage" $ do
    it "read json from dexter_contract_storage.json" $ do
      expression <- readGoldenFile "golden/dexter_contract_storage.json"
      expressionToStorage expression `shouldBe` (Storage 46456288880 791997888 <$> (mkMutez 56414761028))

    it "read json from dexter_contract_storage_edo.json" $ do
      expression <- readGoldenFile "golden/dexter_contract_storage_edo.json"
      expressionToStorage expression `shouldBe` (Storage 71265032904 1010355129 <$> (mkMutez 110310262580))
