{-|
Module      : Spec.Dexter.Exchange.LiquidityReward
Description : Unit tests for liquidity rewards from Dexter
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Spec.Dexter.Exchange.LiquidityReward where

-- base
import Prelude hiding (readFile)

-- testing
import Test.Hspec
  
-- caldera
import Dexter.Exchange.LiquidityReward

spec :: Spec
spec = do
  describe "Liquidity Reward calculations" $ do
    it "calculatePoolRewardForXtzSold" $ do
      calculatePoolReward 1000000 `shouldBe` 3000
