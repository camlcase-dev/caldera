{-# LANGUAGE OverloadedStrings #-}

module Spec.Dexter.Exchange where

import Dexter.Exchange (DexterExchange(..))
import Tezos.Client.Types (ContractId(..), Unistring(..))

-- | hard coded values for tzBTC
tzBTC :: DexterExchange
tzBTC =
  DexterExchange
  "tzBTC"
  "tzBTC"
  8
  (ContractId $ UnistringText "KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf")
  (ContractId $ UnistringText "KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
  1149666
  123

-- | hard coded values for usdTz
usdTz :: DexterExchange
usdTz =
  DexterExchange
  "USD Tez"
  "USDtz"
  6
  (ContractId $ UnistringText "KT1Puc9St8wdNoGtLiD2WXaHbWU7styaxYhD")
  (ContractId $ UnistringText "KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")
  1149672
  124


