{-|
Module      : Main
Description : Unit tests for caldera
Copyright   : (c) camlCase, 2020-2021
Maintainer  : james@camlcase.io

-}

module Main where

import qualified Spec.Dexter.Contract.Storage
import qualified Spec.Dexter.Exchange.LiquidityReward
import qualified Spec.Caldera.Database.Query.DexterOperation
import qualified Spec.Caldera.Synchronizers.DexterOperation.Worker
import qualified Spec.Caldera.Synchronizers.LiquidityReward.Worker
import Test.Hspec

main :: IO ()
main =
  hspec $ parallel $ do
    Spec.Dexter.Contract.Storage.spec
    Spec.Dexter.Exchange.LiquidityReward.spec
    Spec.Caldera.Database.Query.DexterOperation.spec
    Spec.Caldera.Synchronizers.DexterOperation.Worker.spec
    Spec.Caldera.Synchronizers.LiquidityReward.Worker.spec
