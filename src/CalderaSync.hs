{-# LANGUAGE OverloadedStrings     #-}
module CalderaSync(
    start    
  ) where

-- caldera
import qualified Caldera.Config as Caldera
import Caldera.Clients.Generic
    ( clientManager,
      clientScheme,
      clientHost,
      clientPort,
      ClientConfig(ClientConfig) )

import Data.Time.Calendar (fromGregorian)

-- network
import Servant.Client (Scheme(Https))
import Network.HTTP.Client.TLS (newTlsManager)

-- Database
import Caldera.Database.Connection ( connectToDb )

-- Tezos Types 
import Tezos.Client.Types.Core (unContractId)
import Caldera.Synchronizers.Block.Parsing (textToContractId)

-- Workers
import qualified Caldera.Synchronizers.DailyPrices.Worker as DailyPrices

-- Concurrency
import Control.Concurrent (forkIO)
import qualified Control.Concurrent.STM.TChan as Tchan

import qualified Caldera.Synchronizers.DexterOperation.Worker as DexterOperationWorker

-- Logger
import qualified Logging.Logger as L

import qualified Dexter.Exchange as DexterExchange

import Control.Monad (forM_, void)
import Caldera.Database (catchSqlErrors)
import Caldera.Database.Table.DexterContract
import qualified Caldera.Database.Query.DexterContract as DC
import qualified Caldera.Synchronizers.LiquidityReward.Worker as LR

start :: IO ()
start = do
  eitherConfig <- Caldera.getCalderaSyncConfig

  case eitherConfig of
    Left pe      -> fail $ show pe
    Right config -> do
      let tezosConfig = Caldera.csTezosConfig config
          postgresConfig = Caldera.csPostgresConfig config
          workerIntervals = Caldera.csWorkerIntervals config
          syncConfig = Caldera.csSync config
      
      manager <- newTlsManager
      conn <- connectToDb postgresConfig
      logchannel <- Tchan.newTChanIO :: IO (Tchan.TChan L.LogMsg)

      let tezosClient = ClientConfig
                            { clientManager = manager
                            , clientScheme = Https
                            , clientHost = Caldera.tezosNodeHost tezosConfig
                            , clientPort = Caldera.tezosNodePort tezosConfig
                            }
          geckoClient = ClientConfig
                            { clientManager = manager
                            , clientScheme = Https
                            , clientHost = "api.coingecko.com"
                            , clientPort = 443
                            }
          priceDailyConfig = DailyPrices.PricesWorkerConfig
                             { DailyPrices.workerInterval = Caldera.dailyPricesInterval workerIntervals
                             , DailyPrices.clientConfig = geckoClient
                             , DailyPrices.databaseConnection = conn
                             }
          operationsConfig = DexterOperationWorker.OperationsWorkerConfig
                             { DexterOperationWorker.logChan = logchannel
                             , DexterOperationWorker.client = tezosClient
                             , DexterOperationWorker.db = conn
                             , DexterOperationWorker.startLevel = 1365556
                             , DexterOperationWorker.dexters = map toDexter (Caldera.contractConfigs syncConfig)
                             , DexterOperationWorker.workerInterval = Caldera.blockWorkerInterval workerIntervals
                             }

      -- insert dexter exchanges into the database
      forM_ (DexterOperationWorker.dexters operationsConfig) $ \d -> do
        void $ catchSqlErrors $ DC.insert conn $
          DexterContract
            { dcTokenName        = DexterExchange.tokenName d
            , dcTokenSymbol      = DexterExchange.tokenSymbol d
            , dcDexterContractId = DexterOperationWorker.toText . unContractId $ DexterExchange.dexterContract d
            , dcTokenContractId  = DexterOperationWorker.toText . unContractId $ DexterExchange.tokenContract d
            , dcOrigination      = DexterExchange.dexterContractOrigination d
            , dcBigMapId         = DexterExchange.dexterBigMapId d
            , dcDecimals         = DexterExchange.decimals d
            }
            
      L.start logchannel L.INFO 
      DexterOperationWorker.run operationsConfig

      -- | This is the date that the first dexter contract was originated
      let beginningOfDexter = fromGregorian 2021 2 20
      
      _ <- forkIO $ DailyPrices.startPricesWorker logchannel priceDailyConfig beginningOfDexter

      -- run every minute, only updates when there are new dexter operations
      LR.start logchannel conn (1000000 * 60)

toDexter :: Caldera.ContractConfig -> DexterExchange.DexterExchange
toDexter config 
  = DexterExchange.DexterExchange
    { DexterExchange.tokenName = Caldera.dexterTokenName config
    , DexterExchange.tokenSymbol = Caldera.dexterTokenSymbol config
    , DexterExchange.decimals = Caldera.decimals config
    , DexterExchange.dexterContract = textToContractId $ Caldera.dexterContract config
    , DexterExchange.tokenContract = textToContractId $ Caldera.tokenContract config
    , DexterExchange.dexterContractOrigination = Caldera.dexterContractOrigination config
    , DexterExchange.dexterBigMapId = Caldera.dexterBigMapId config    
    }
