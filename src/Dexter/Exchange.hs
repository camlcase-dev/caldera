{- |
Module      :  Dexter.Exchange
Description :  The invariant values of a Dexter Exchange
Copyright   :  (c) 2021, camlCase
Maintainer  :  james@camlcase.io

A dexter exchange is defined by its contract ids, big map id, number of decimals
the corresponding token has and the block level at which it was originated at.
-}

{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Dexter.Exchange(
    DexterExchange(..)
  , getDexterExchangeByContractId
  ) where

import Data.Aeson   (FromJSON)
import Data.Int     (Int64)
import Data.Text    (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types (ContractId(..))
import Safe (headMay)

-- | hard coded information about a dexter exchange
data DexterExchange =
  DexterExchange
    { tokenName                 :: Text -- ^ the name of the token the exchange is paired with
    , tokenSymbol               :: Text -- ^ the symbol of the token the exchange is paired with
    , decimals                  :: Integer -- ^ the number of decimals a token contract has, must be zero or greater
    , dexterContract            :: ContractId -- ^ the KT1 address of a dexter contract
    , tokenContract             :: ContractId -- ^ the KT1 address of a token contract that dexterContract interfaces
    , dexterContractOrigination :: Int64 -- ^ the block level at which the dexter contract was originated
    , dexterBigMapId            :: Integer -- ^ dexter's big map id
    } deriving (Eq, Generic, Show)

instance FromJSON DexterExchange

getDexterExchangeByContractId :: ContractId -> [DexterExchange] -> Maybe DexterExchange
getDexterExchangeByContractId c =
  headMay . filter (\d -> dexterContract d == c)
