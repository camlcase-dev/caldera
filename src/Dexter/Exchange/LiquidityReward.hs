{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}

module Dexter.Exchange.LiquidityReward where

import Dexter.Exchange.Transaction hiding (contract)
import GHC.Natural (Natural)
import Tezos.Client.Types.Core (Mutez, unMutez, unBignum)

mutezToNatural :: Mutez -> Natural
mutezToNatural = fromIntegral . unMutez

mutezToDouble :: Mutez -> Double
mutezToDouble = fromInteger . toInteger . unMutez

naturalToDouble :: Natural -> Double
naturalToDouble = fromInteger . toInteger

-- | Calculate the liquidity reward for a single transaction that the liquidity
-- pool earns (XTZ, FA1.2 or FA2 token).
calculatePoolReward
  :: Natural
  -> Natural
calculatePoolReward sold = sold * 3 `div` 1000

-- | Convert the token amount to XTZ based on the exchange rate of the two pools
-- at a block
tokenToMutez :: Natural -> Natural -> Natural -> Natural
tokenToMutez tokenAmount xtzPool' tokenPool' =
  -- xtzPool' `div` tokenPool * tokenAmount
  floor $ (naturalToDouble xtzPool') / (naturalToDouble tokenPool') * (naturalToDouble tokenAmount)

-- | For a give transaction type, get the liquidity rewards for the entire pool.
getSingleTransactionLiquidityRewards :: DexterTransaction -> (Natural, Natural)
getSingleTransactionLiquidityRewards =
  \case
    XtzToToken t ->
      ( calculatePoolReward $ mutezToNatural $ xtzSold t
      , 0
      )
    TokenToXtz t ->
      ( 0
      , calculatePoolReward (fromInteger $ unBignum $ tokensSold (t :: TokenToXtzTransaction))
      )
    TokenToToken t ->
      ( calculatePoolReward (mutezToNatural $ internalXtzSold t)
      , calculatePoolReward (fromInteger $ unBignum $ tokensSold (t :: TokenToTokenTransaction))
      )
    AddLiquidity    _ -> (0,0)
    RemoveLiquidity _ -> (0,0)
