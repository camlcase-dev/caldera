{- |
Module      :  Dexter.Exchange.Transaction
Description :  Dexter Exchange transactions from the Tezos Blockchain
Copyright   :  (c) 2021, camlCase
Maintainer  :  james@camlcase.io

Caldera tracks five Dexter Exchange transactions from the Tezos Blockchain

-}

{-# LANGUAGE DuplicateRecordFields #-}

module Dexter.Exchange.Transaction where

import qualified Data.Text as T
import GHC.Natural (Natural)
import Tezos.Client.Types (Bignum, ContractId, Mutez)

-- | Transaction status
data TransactionStatus
  = Applied
  | Failed
  | Skipped
  | Backtracked
  deriving (Eq, Show)

-- | The five Dexter transaction entry points for exchanging
data DexterTransaction
  = XtzToToken XtzToTokenTransaction
  | TokenToXtz TokenToXtzTransaction
  | TokenToToken TokenToTokenTransaction
  | AddLiquidity AddLiquidityTransaction
  | RemoveLiquidity RemoveLiquidityTransaction
  deriving (Eq, Show)


-- | Exchange XTZ for token.
data XtzToTokenTransaction =
  XtzToTokenTransaction
    { contract          :: T.Text -- ^ the dexter contract id that was called
    , source            :: ContractId -- ^ address that initiated the operation. May be different from the owner and to addresses.
    , to_               :: T.Text -- ^ the address which will receive the purchased token
    , xtzSold           :: Mutez  -- ^ the amount of XTZ the sender sold to Dexter
    , minTokensBought   :: Bignum -- ^ the minimum amount of token the spender wishes to purchase. the total is equal or greater.
    , deadline          :: T.Text -- ^ the latest time at which the transaction must complete or it fail
    , totalTokensBought :: Bignum -- ^ the total number of tokens purchased. This comes from an internal operation.
    , totalLqt          :: Natural -- ^ the total amount of liquidity tokens after the transaction
    , tokenPool         :: Natural -- ^ the total amount of tokens in dexter after the transaction
    , xtzPool           :: Mutez -- ^ the total amount of XTZ in dexter after the transaction
    } deriving (Eq, Show)


-- | Exchangee token for XTZ.
data TokenToXtzTransaction =
  TokenToXtzTransaction
    { contract       :: T.Text -- ^ the dexter contract id that was called
    , source         :: ContractId -- ^ address that initiated the operation. May be different from the owner and to addresses.
    , owner          :: T.Text -- ^ the tokens spent belong to this account (dexter needs permission)
    , to_            :: T.Text -- ^ the address which will receive the purchased XTZ
    , tokensSold     :: Bignum -- ^ the amount of owner's tokens sold
    , minXtzBought   :: Bignum -- ^ minimum amount the sender wishes to purchase. the total is equal or greater.
    , deadline       :: T.Text -- ^ the latest time at which the transaction must complete or it fail   
    , totalXtzBought :: Mutez -- ^ the total number of XTZ purchased. This comes from an internal operation.
    , totalLqt       :: Natural -- ^ the total amount of liquidity tokens after the transaction
    , tokenPool      :: Natural -- ^ the total amount of tokens in dexter after the transaction
    , xtzPool        :: Mutez -- ^ the total amount of XTZ in dexter after the transaction    
    } deriving (Eq, Show)


-- | Exchange one token for another token. This involves two Dexter Exchange contracts.
data TokenToTokenTransaction =
  TokenToTokenTransaction
    { contract             :: T.Text -- ^ the dexter contract id that was called
    , source               :: ContractId -- ^ address that initiated the operation. May be different from the owner and to addresses.
    , outputDexterContract :: T.Text -- ^ the dexter contract of the target token the sender wishes to purchase
    , minTokensBought      :: Bignum -- ^ minimum amount the sender wishes to purchase. the total is equal or greater
    , owner                :: T.Text -- ^ the tokens spent belong to this account (dexter needs permission)
    , to_                  :: T.Text -- ^ the address which will receive the purchased token
    , tokensSold           :: Bignum -- ^ the amount of owner's tokens sold
    , deadline             :: T.Text -- ^ the latest time at which the transaction must complete or it fail
    , internalXtzSold      :: Mutez -- ^ amount of xtz transferred from dexter to outputDexterContract. This comes from an internal operation.
    , totalTokensBought    :: Bignum -- ^ the total number of tokens purchased. This comes from an internal operation.
    , totalLqt             :: Natural -- ^ the total amount of liquidity tokens after the transaction
    , tokenPool            :: Natural -- ^ the total amount of tokens in dexter after the transaction
    , xtzPool              :: Mutez -- ^ the total amount of XTZ in dexter after the transaction
    } deriving (Eq, Show)


-- | A sender provides equal values of XTZ and token to a dexter exchange in
-- exchange for liquidity tokens.
data AddLiquidityTransaction =
  AddLiquidityTransaction
    { contract             :: T.Text -- ^ the dexter contract id that was called
    , source               :: ContractId -- ^ address that initiated the operation. May be different from the owner and to addresses.
    , owner                :: T.Text -- ^ the tokens spent belong to this account (dexter needs permission)
    , minLqtMinted         :: Bignum -- ^ minimum amount of liquidity the sender wishes to receive.
    , maxTokensDeposited   :: Bignum -- ^ the maximum amount of token the sender is willing to deposit
    , xtzDeposited         :: Mutez -- ^ the amount of xtz the sender has deposited
    , deadline             :: T.Text -- ^ the latest time at which the transaction must complete or it fail
    , totalTokensDeposited :: Bignum -- ^ the total number of tokens purchased. This comes from an internal operation.
    , totalLqt             :: Natural -- ^ the total amount of liquidity tokens after the transaction
    , tokenPool            :: Natural -- ^ the total amount of tokens in dexter after the transaction
    , xtzPool              :: Mutez -- ^ the total amount of XTZ in dexter after the transaction    
    } deriving (Eq, Show)

-- | The source owns or has permission to burn liquidity. An equivalent amount
-- of XTZ and token is sent to the to_ address.
data RemoveLiquidityTransaction =
  RemoveLiquidityTransaction
    { contract             :: T.Text -- ^ the dexter contract id that was called
    , source               :: ContractId -- ^ address that initiated the operation. May be different from the owner and to addresses.
    , owner                :: T.Text -- ^ the tokens spent belong to this account (dexter needs permission)
    , to_                  :: T.Text -- ^ the address which will receive the withdrawn XTZ and token.
    , lqtBurned            :: Bignum -- ^ amount of lqt burned. Must be equal to or less than the amount owned by owner
    , minXtzWithdrawn      :: Bignum -- ^ the minimum amount of XTZ the to address is willing to receive
    , minTokensWithdrawn   :: Bignum -- ^ the minimum amount of token the to address is willing to receive    
    , deadline             :: T.Text -- ^ the latest time at which the transaction must complete or it fail
    , totalXtzWithdrawn    :: Mutez -- ^ the total number of XTZ withdrawn. This comes from an internal operation.
    , totalTokensWithdrawn :: Bignum -- ^ the total number of tokens withdrawn. This comes from an internal operation.
    , totalLqt             :: Natural -- ^ the total amount of liquidity tokens after the transaction
    , tokenPool            :: Natural -- ^ the total amount of tokens in dexter after the transaction
    , xtzPool              :: Mutez -- ^ the total amount of XTZ in dexter after the transaction    
    } deriving (Eq, Show)
