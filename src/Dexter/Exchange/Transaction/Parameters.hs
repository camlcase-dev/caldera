{- |
Module      :  Dexter.Exchange.Transaction.Parameters
Description :  Dexter Exchange transactions from the Tezos Blockchain
Copyright   :  (c) 2021, camlCase
Maintainer  :  james@camlcase.io

Parse the expected Micheline values out of a Dexter Operation
-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Dexter.Exchange.Transaction.Parameters(
    ParseError                          (..)
  , RemoveLiquidityTransactionParameters(..)
  , AddLiquidityTransactionParameters   (..)
  , TokenToTokenTransactionParameters   (..)
  , TokenToXtzTransactionParameters     (..)
  , XtzToTokenTransactionParameters     (..)
  , DexterTransactionParameters         (..)
  , parseDexterEntrypointParameters
  , toText
  ) where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Time (secondsToNominalDiffTime, utc, utcToZonedTime)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Data.Time.RFC3339 (formatTimeRFC3339)
import qualified Tezos.Client.Types.Micheline as Micheline
import Tezos.Client.Types (Bignum(..), Unistring(..), Entrypoint(..))

-- | A time value may be in two possible formats
readTime :: Micheline.Expression -> T.Text
readTime e =
  case e of
    Micheline.StringExpression t -> toText t
    Micheline.IntExpression i ->
      formatTimeRFC3339 . utcToZonedTime utc . posixSecondsToUTCTime . secondsToNominalDiffTime . fromInteger . unBignum $ i
    _ -> ""

-- | Get the Text of a UnistringText value
toText :: Unistring -> T.Text
toText =
  \case
    UnistringText t -> t
    _ -> ""

-- | Errors encountered when reading the Micheline parameters
data ParseError
  = UnexpectedEntrypoint Text
  | IgnoredDexterEntrypoint Text
  | UnexpectedMichelineParameter Text Micheline.Expression
  deriving (Eq, Show)

-- | The five Dexter transaction entry points for exchanging
data DexterTransactionParameters
  = XtzToToken XtzToTokenTransactionParameters
  | TokenToXtz TokenToXtzTransactionParameters
  | TokenToToken TokenToTokenTransactionParameters
  | AddLiquidity AddLiquidityTransactionParameters
  | RemoveLiquidity RemoveLiquidityTransactionParameters
  deriving (Eq, Show)


-- | Exchange XTZ for token.
data XtzToTokenTransactionParameters =
  XtzToTokenTransactionParameters
    { toAddress      :: Text
    , minTokenBought :: Bignum
    , deadline       :: Text
    } deriving (Eq, Show)


-- | Exchangee token for XTZ.
data TokenToXtzTransactionParameters =
  TokenToXtzTransactionParameters
    { ownerAddress   :: Text
    , toAddress      :: Text
    , tokensSold     :: Bignum    
    , minXtzBought   :: Bignum
    , deadline       :: Text
    } deriving (Eq, Show)

-- | Exchange one token for another token. This involves two Dexter Exchange contracts.
data TokenToTokenTransactionParameters =
  TokenToTokenTransactionParameters
    { tokensBoughtDexterContract :: Text
    , ownerAddress               :: Text
    , toAddress                  :: Text
    , tokensSold                 :: Bignum
    , minTokensBought            :: Bignum
    , deadline                   :: Text
    } deriving (Eq, Show)

-- | A sender provides equal values of XTZ and token to a dexter exchange in
-- exchange for liquidity tokens.
data AddLiquidityTransactionParameters =
  AddLiquidityTransactionParameters
    { ownerAddress               :: Text
    , minLqtMinted               :: Bignum
    , maxTokensDeposited         :: Bignum
    , deadline                   :: Text
    } deriving (Eq, Show)

-- | The source owns or has permission to burn liquidity. An equivalent amount
-- of XTZ and token is sent to the to_ address.
data RemoveLiquidityTransactionParameters =
  RemoveLiquidityTransactionParameters
    { ownerAddress               :: Text
    , toAddress                  :: Text
    , lqtBurned                  :: Bignum
    , minXtzWithdrawn            :: Bignum    
    , minTokensWithdrawn         :: Bignum
    , deadline                   :: Text
    } deriving (Eq, Show)

-- | Get the expected dexter transaction parameters
parseDexterEntrypointParameters :: Micheline.Parameters -> Either ParseError DexterTransactionParameters
parseDexterEntrypointParameters parameters =
  case Micheline.entrypoint parameters of
    EUnistring (UnistringText "xtzToToken") -> parseXtzToToken (Micheline.value parameters)
    EUnistring (UnistringText "tokenToXtz") -> parseTokenToXtz (Micheline.value parameters)
    EUnistring (UnistringText "tokenToToken") -> parseTokenToToken (Micheline.value parameters)
    EUnistring (UnistringText "addLiquidity") ->  parseAddLiquidity (Micheline.value parameters)
    EUnistring (UnistringText "removeLiquidity") -> parseRemoveLiquidity (Micheline.value parameters)
    EUnistring (UnistringText x) -> Left $ IgnoredDexterEntrypoint x
    EDefault -> Left $ IgnoredDexterEntrypoint "default"
    x -> Left $ UnexpectedEntrypoint $ T.pack $ show x
              
parseXtzToToken :: Micheline.Expression -> Either ParseError DexterTransactionParameters
parseXtzToToken =
  \case
    Micheline.Expression
      (Micheline.PrimitiveData Micheline.PDPair)
      (Just
        [ Micheline.StringExpression toAddress'
        , Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.IntExpression minTokenBought'
            , deadline'
            ]
          )
          _
        ]
      )
      _ -> Right $ XtzToToken $ XtzToTokenTransactionParameters (toText toAddress') minTokenBought' (readTime deadline')
    -- edo
    Micheline.Expression
      (Micheline.PrimitiveData Micheline.PDPair)
      (Just
        [ Micheline.StringExpression toAddress'
        , Micheline.IntExpression minTokenBought'
        , deadline'
        ]
      )
      _ -> Right $ XtzToToken $ XtzToTokenTransactionParameters (toText toAddress') minTokenBought' (readTime deadline')

    e -> Left $ UnexpectedMichelineParameter "xtzToToken" e
  

parseTokenToXtz :: Micheline.Expression -> Either ParseError DexterTransactionParameters
parseTokenToXtz =
  \case
    Micheline.Expression
      (Micheline.PrimitiveData Micheline.PDPair)
      (Just
        [ Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.StringExpression ownerAddress'
            , Micheline.StringExpression toAddress'
            ]
          )
          _
        , Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.IntExpression tokenSold'
            , Micheline.Expression
              (Micheline.PrimitiveData Micheline.PDPair)
              (Just
                [ Micheline.IntExpression minXtzBought'
                , deadline'
                ]
              )
              _
            ]
          )
          _
        ]
      )
      _ -> Right $ TokenToXtz $ TokenToXtzTransactionParameters (toText ownerAddress') (toText toAddress') tokenSold' minXtzBought' (readTime deadline')
    Micheline.Expression
      (Micheline.PrimitiveData Micheline.PDPair)
      (Just
        [ Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.StringExpression ownerAddress'
            , Micheline.StringExpression toAddress'
            ]
          )
          _
        , Micheline.IntExpression tokenSold'
        , Micheline.IntExpression minXtzBought'
        , deadline'
        ]
      )
      _ -> Right $ TokenToXtz $ TokenToXtzTransactionParameters (toText ownerAddress') (toText toAddress') tokenSold' minXtzBought' (readTime deadline')      
    e -> Left $ UnexpectedMichelineParameter "tokenToXtz" e



parseTokenToToken :: Micheline.Expression -> Either ParseError DexterTransactionParameters
parseTokenToToken =
  \case
    Micheline.Expression
      (Micheline.PrimitiveData Micheline.PDPair)
      (Just
        [ Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.StringExpression tokensBoughtDexterContract'
            , Micheline.Expression
              (Micheline.PrimitiveData Micheline.PDPair)
              (Just
                [ Micheline.IntExpression minTokensBought'
                , Micheline.StringExpression ownerAddress'
                ]
              )
              _
            ]
          )
          _
        , Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.StringExpression toAddress'
            , Micheline.Expression
              (Micheline.PrimitiveData Micheline.PDPair)
              (Just
                [ Micheline.IntExpression tokensSold'
                , deadline'
                ]
              )
              _
            ]
          )
          _
        ]
      )
      _ -> Right $ TokenToToken $
      TokenToTokenTransactionParameters
      (toText tokensBoughtDexterContract')
      (toText ownerAddress')
      (toText toAddress')
      tokensSold'
      minTokensBought'
      (readTime deadline')

    e -> Left $ UnexpectedMichelineParameter "tokenToToken" e


parseAddLiquidity :: Micheline.Expression -> Either ParseError DexterTransactionParameters
parseAddLiquidity =
  \case
    Micheline.Expression
      (Micheline.PrimitiveData Micheline.PDPair)
      (Just
        [ Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.StringExpression ownerAddress'
            , Micheline.IntExpression minLqtMinted'
            ]
          )
          _
        , Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.IntExpression maxTokensDeposited'
            , deadline'
            ]
          )
          _
        ]
      )
      _ -> Right $ AddLiquidity $
      AddLiquidityTransactionParameters
      (toText ownerAddress')
      minLqtMinted'
      maxTokensDeposited'
      (readTime deadline')
      
    e -> Left $ UnexpectedMichelineParameter "addLiquidity" e
  

parseRemoveLiquidity :: Micheline.Expression -> Either ParseError DexterTransactionParameters
parseRemoveLiquidity =
  \case
    Micheline.Expression
      (Micheline.PrimitiveData Micheline.PDPair)
      (Just
        [ Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.StringExpression ownerAddress'
            , Micheline.Expression
              (Micheline.PrimitiveData Micheline.PDPair)
              (Just
                [ Micheline.StringExpression toAddress'
                , Micheline.IntExpression lqtBurned'
                ]
              )
              _
            ]
          )
          _
        , Micheline.Expression
          (Micheline.PrimitiveData Micheline.PDPair)
          (Just
            [ Micheline.IntExpression minXtzWithdrawn'
            , Micheline.Expression
              (Micheline.PrimitiveData Micheline.PDPair)
              (Just
                [ Micheline.IntExpression minTokensWithdrawn'
                , deadline'
                ]
              )
              _
            ]
          )
          _
        ]
      )
      _ ->
      Right $ RemoveLiquidity $
      RemoveLiquidityTransactionParameters
      (toText ownerAddress')
      (toText toAddress')
      lqtBurned'
      minXtzWithdrawn'
      minTokensWithdrawn'      
      (readTime deadline')
      
    e -> Left $ UnexpectedMichelineParameter "removeLiquidity" e
