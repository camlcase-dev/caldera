{-|
Module      : Dexter.Contract.Storage
Description : Parse Dexter contract storage
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

-}

module Dexter.Contract.Storage(
    Storage(..)
  , expressionToStorage
  ) where

import GHC.Natural (Natural)
import Tezos.Client.Types.Core (Mutez, mkMutez, unBignum)
import Tezos.Client.Types.Micheline (Expression(..), Primitives(..), PrimitiveData(..))

-- | storage from the dexter contract
data Storage =
  Storage
    { lqtTotal       :: Natural
    , tokenPool      :: Natural
    , xtzPool        :: Mutez
    } deriving (Eq, Show)

-- | parse the storage from carthagenet, delphinet or edonet
expressionToStorage :: Expression -> Maybe Storage
expressionToStorage expression =
  case expression of
    -- this is for delphinet and earlier
    (Expression
      (PrimitiveData PDPair)
      (Just
       [IntExpression _bigMapId,
        (Expression
         (PrimitiveData PDPair)
         (Just
          [(Expression
            (PrimitiveData PDPair)
            (Just
              [(Expression (PrimitiveData _calledByDexter) _ _)
              ,(Expression
                (PrimitiveData PDPair)
                (Just
                 [(Expression (PrimitiveData _freezeBaker) _ _),
                  (IntExpression lqtTotal')])
                 _)
              ])
             _
           ),
           (Expression
            (PrimitiveData PDPair)
            (Just
             [(Expression
               (PrimitiveData PDPair)
               (Just
                [ _manager -- bytes or string
                , _tokenAddress -- bytes or string
                ])
               _
              ),
              (Expression
               (PrimitiveData PDPair)
               (Just
                [ (IntExpression tokenPool')
                , (IntExpression xtzPool')
                ])
               _
              )      
                
             ])
            _
           )            
          ])
        _
       )
       ])
      _) ->
      Storage
        (fromIntegral . unBignum $ lqtTotal')
        (fromIntegral . unBignum $ tokenPool') <$> mkMutez (fromIntegral . unBignum $ xtzPool')
    -- this is for edonet
    (Expression
      (PrimitiveData PDPair)
      (Just
       [ IntExpression _bigMapId
       , Expression
           (PrimitiveData PDPair)
           (Just
            [ _calledByDexter
            , _freezeBaker
            , (IntExpression lqtTotal')
            ]
           )
           _
       , Expression
           (PrimitiveData PDPair)
           (Just
            [ _manager
            , _tokenAddress
            ]
           )
           _
       , (IntExpression tokenPool')
       , (IntExpression xtzPool')
       ]
      )
      _) ->
      Storage
        (fromIntegral . unBignum $ lqtTotal')
        (fromIntegral . unBignum $ tokenPool') <$> mkMutez (fromIntegral . unBignum $ xtzPool')

    -- this is for edonet
    Expressions
      [ IntExpression _bigMapId
      , Expression
        (PrimitiveData PDPair)
        (Just
          [ _calledByDexter
          , (Expression
             (PrimitiveData PDPair)
             (Just
              [ _freezeBaker -- bytes or string
              , (IntExpression lqtTotal')
              ])
             _
            )
          ]
        )
        _
      , Expression
        (PrimitiveData PDPair)
        (Just
          [ _manager
          , _tokenAddress
          ]
        )
        _
      , (IntExpression tokenPool')
      , (IntExpression xtzPool')
      ] ->
      Storage
        (fromIntegral . unBignum $ lqtTotal')
        (fromIntegral . unBignum $ tokenPool') <$> mkMutez (fromIntegral . unBignum $ xtzPool')      
        
    _ -> Nothing
