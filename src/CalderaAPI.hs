{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module CalderaAPI(
    startAPI
  ) where

-- base
import qualified Control.Concurrent.STM.TChan as Tchan
import qualified Data.Map as M
import qualified System.Clock as Clock

import Control.Monad.STM (atomically)
import Data.Maybe (isJust)
import Data.Version (showVersion)
import Paths_caldera (version)
import System.Environment (lookupEnv)

-- caldera
import qualified Caldera.API.Routes   as Caldera
import qualified Caldera.API.Server as Caldera
import qualified Caldera.API.Types as Types
import qualified Caldera.Config as Caldera

-- text and encoding 
import           Data.Text             (Text)
import qualified Data.Text             as T
import qualified Data.Text.IO          as T(putStrLn)

-- networking
import           Network.Wai
import           Network.Wai.Handler.Warp     (run)
import           Servant

-- database
import Caldera.Database.Connection ( connectToDb )
import Database.PostgreSQL.Simple ( Connection )

-- cache
import qualified Data.Cache as Cache
import qualified Caldera.API.Cache.Liquidity as LiquidityCache
import qualified Caldera.API.Cache.Volume as VolumeCache

-- logging
import qualified Logging.Logger as L

-- network
import Network.Wai.Middleware.Cors
import Network.HTTP.Types.Method   (renderStdMethod)


-- | application to be run
app
  :: Tchan.TChan L.LogMsg
  -> Connection
  -> Cache.Cache (T.Text, T.Text) Types.Liquidity
  -> Cache.Cache (T.Text, T.Text) Types.Volume
  -> Application
app logchan conn liquidityCache volumeCache =
  serve (Proxy :: Proxy Caldera.API) $
  Caldera.server logchan conn liquidityCache volumeCache

-- | caldera version from the cabal file
calderaVersion :: Text
calderaVersion = T.pack $ showVersion version

-- | read configs then run the server
startAPI :: IO ()
startAPI = do
  T.putStrLn $ "Caldera " <> calderaVersion
  eitherConfig <- Caldera.getCalderaAPIConfig
  case eitherConfig of 
    Left err -> fail err
    Right config -> do
      -- connection per thread
      conn1 <- connectToDb $ Caldera.ccPostgresConfig config
      conn2 <- connectToDb $ Caldera.ccPostgresConfig config
      conn3 <- connectToDb $ Caldera.ccPostgresConfig config
      liquidityCache <- Cache.newCache (Just $ Clock.TimeSpec 86400 0) :: IO (Cache.Cache (T.Text, T.Text) Types.Liquidity)
      volumeCache <- Cache.newCache (Just $ Clock.TimeSpec 86400 0) :: IO (Cache.Cache (T.Text, T.Text) Types.Volume)
      logChan <- Tchan.newTChanIO :: IO (Tchan.TChan L.LogMsg)

      let port = read $ T.unpack $ Caldera.serverPort (Caldera.ccServerConfig config)

          cacheInterval = Caldera.cacheWorkerInterval (Caldera.ccCacheConfig config)

          liquidityCacheConfig = LiquidityCache.LiquidityCacheConfig
                                     { LiquidityCache.logChan = logChan
                                     , LiquidityCache.db = conn1
                                     , LiquidityCache.cache = liquidityCache
                                     , LiquidityCache.workerInterval = cacheInterval
                                     }

          volumeCacheConfig = VolumeCache.VolumeCacheConfig
                                     { VolumeCache.logChan = logChan
                                     , VolumeCache.db = conn2
                                     , VolumeCache.cache = volumeCache
                                     , VolumeCache.workerInterval = cacheInterval
                                     }

      L.start logChan L.DEBUG 
      LiquidityCache.start liquidityCacheConfig
      VolumeCache.start volumeCacheConfig

      let logMap = Just $ M.fromList [("port", T.pack (show port))]
      atomically $ Tchan.writeTChan logChan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Starting caldera-api"}

      isDevServer <- isJust <$> lookupEnv "CALDERA_DEV"      
      
      run port
        $ (if isDevServer then devCors else id)
        $ app logChan conn3 liquidityCache volumeCache

devCors :: Middleware
devCors =
  let allowedOrigins = [ "http://127.0.0.1:8011"
                       , "http://localhost:8011"
                       , "https://127.0.0.1:8011"
                       , "https://localhost:8011"
                       , "127.0.0.1:8011"
                       , "localhost:8011"                       
                       ]
      allowedHeaders = [ "Accept"
                       , "Content-Language"
                       , "Content-Type"
                       , "Access-Control-Allow-Origin"
                       , "Access-Control-Request-Method"                       
                       , "Accept-Encoding"
                       , "Accept-Language"
                       , "Authorization"
                       , "Cache-Control"
                       , "Connection"
                       , "Cookie"
                       , "DNT"
                       , "Expires"
                       , "Host"
                       , "Origin"
                       , "Pragma"
                       , "Referer"
                       , "Sec-WebSocket-Extensions"
                       , "Sec-WebSocket-Key"
                       , "Sec-WebSocket-Version"                       
                       , "Upgrade"
                       , "User-Agent"
                       ]
   
   in cors $ const $ Just simpleCorsResourcePolicy
    { corsRequestHeaders = allowedHeaders
    , corsOrigins        = Just (allowedOrigins, True)
    -- allow all methods
    , corsMethods        = fmap renderStdMethod [minBound..maxBound]
    }
