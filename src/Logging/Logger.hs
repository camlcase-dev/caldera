{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module Logging.Logger(
    LogMsg(..)
  , Level (..)
  , start
  , lerror
  , linfo
  , ldebug
  , lwarning
  , logInfo
  , logDebug
  , logError
  , logWarning
  , logEitherErrors
  ) where

import qualified Colog as C
import qualified Control.Concurrent.STM.TChan as Tchan
import qualified Data.Text as T
import qualified Data.Map as M

import Control.Concurrent (forkIO)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.STM (atomically)
import Data.Either (lefts)
import Data.Text (Text)

data Level
  = INFO
  | WARNING
  | ERROR
  | DEBUG

data LogMsg
  = LogMsg
    { level :: Level
    , vals :: Maybe (M.Map T.Text T.Text)
    , msg :: T.Text
    }

start :: Tchan.TChan LogMsg -> Level -> IO ()
start logMsgChannel wantLevel = do
  _ <- forkIO $ log' logMsgChannel wantLevel
  pure ()

log' :: Tchan.TChan LogMsg -> Level -> IO ()
log' logMsgChannel wantLevel = do
  logMsg <- atomically $ Tchan.readTChan logMsgChannel
  let logLevel = level logMsg
  logFunc wantLevel logLevel $ constructLogMsg (vals logMsg) (T.unpack $ msg logMsg)
  log' logMsgChannel wantLevel

constructLogMsg :: Show a => Maybe a -> [Char] -> [Char]
constructLogMsg maybeMap logMsg = do
  case maybeMap of
    Nothing -> logMsg
    Just x -> logMsg ++ ": " ++ show x

logFunc :: MonadIO m => Level -> (Level -> String -> m ())
logFunc logLevel =
  case logLevel of
    INFO -> logLevelInfoFunc
    WARNING -> logLevelWarningFunc 
    ERROR -> logLevelErrorFunc 
    DEBUG -> logLevelDebugFunc 

logLevelInfoFunc :: MonadIO m => Level -> String -> m ()
logLevelInfoFunc logLevel logMsg =
  case logLevel of
    INFO -> linfo logMsg
    ERROR -> lerror logMsg          
    WARNING -> pure ()
    DEBUG -> pure ()

logLevelWarningFunc :: MonadIO m => Level -> String -> m ()
logLevelWarningFunc logLevel logMsg =
  case logLevel of
    INFO -> linfo logMsg
    WARNING -> lwarning logMsg
    ERROR -> lerror logMsg
    DEBUG -> pure ()

logLevelDebugFunc :: MonadIO m => Level -> String -> m ()
logLevelDebugFunc logLevel logMsg =
  case logLevel of
    INFO -> linfo logMsg
    WARNING -> lwarning logMsg
    ERROR -> lerror logMsg
    DEBUG -> ldebug logMsg

logLevelErrorFunc :: MonadIO m => Level -> String -> m ()
logLevelErrorFunc logLevel logMsg =
  case logLevel of
    INFO    -> pure ()
    WARNING -> pure ()
    ERROR   -> lerror logMsg
    DEBUG   -> pure ()

linfo :: MonadIO m => String -> m ()
linfo logMsg = do 
  let action = C.cmap C.fmtMessage C.logTextStdout
  C.usingLoggerT action $ C.logInfo $ T.pack logMsg

lwarning :: MonadIO m => String -> m ()
lwarning logMsg = do 
  let action = C.cmap C.fmtMessage C.logTextStdout
  C.usingLoggerT action $ C.logWarning $ T.pack logMsg

ldebug :: MonadIO m => String -> m ()
ldebug logMsg = do 
  let action = C.cmap C.fmtMessage C.logTextStdout
  C.usingLoggerT action $ C.logDebug $ T.pack logMsg

lerror :: MonadIO m => String -> m ()
lerror logMsg = do 
  let action = C.cmap C.fmtMessage C.logTextStdout
  C.usingLoggerT action $ C.logError $ T.pack logMsg

logInfo
  :: Text
  -> Tchan.TChan LogMsg
  -> Text
  -> [(Text,Text)]
  -> IO ()
logInfo source logchan msg vals =
  atomically $
  Tchan.writeTChan
  logchan
  LogMsg
    { level = INFO
    , vals = if length vals > 0 then Just $ M.fromList vals else Nothing
    , msg = source <> ": " <> msg
    }

logDebug
  :: Text
  -> Tchan.TChan LogMsg
  -> Text
  -> [(Text,Text)]
  -> IO ()
logDebug source logchan msg vals =
  atomically $
  Tchan.writeTChan
  logchan
  LogMsg
    { level = DEBUG
    , vals = if length vals > 0 then Just $ M.fromList vals else Nothing
    , msg = source <> ": " <> msg
    }

logError
  :: Text
  -> Tchan.TChan LogMsg
  -> Text
  -> [(Text,Text)]
  -> IO ()
logError source logchan msg vals =
  atomically $
  Tchan.writeTChan
  logchan
  LogMsg
    { level = ERROR
    , vals = if length vals > 0 then Just $ M.fromList vals else Nothing
    , msg = source <> ": " <> msg
    }

logWarning
  :: Text
  -> Tchan.TChan LogMsg
  -> Text
  -> [(Text,Text)]
  -> IO ()
logWarning source logchan msg vals =
  atomically $
  Tchan.writeTChan
  logchan
  LogMsg
    { level = WARNING
    , vals = if length vals > 0 then Just $ M.fromList vals else Nothing
    , msg = source <> ": " <> msg
    }

logEitherErrors
  :: (Show a)
  => Text
  -> Tchan.TChan LogMsg
  -> Text
  -> [Either a b]
  -> IO ()
logEitherErrors source logchan msg vals =
  if length (lefts vals) > 0
  then
    logError
    source
    logchan
    (msg <> (T.pack . show . length $ lefts vals) <> " times")
    ((\(err, idx) -> (T.pack . show $ idx, T.pack . show $ err) ) <$> zip (lefts vals) ([0..] :: [Int]))
  else pure ()
