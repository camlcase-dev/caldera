{-# LANGUAGE OverloadedStrings   #-}

module Caldera.Database.Query.LiquidityRewardAndOperation where

import Caldera.Database.Table.LiquidityRewardAndOperation
import Database.PostgreSQL.Simple
import Data.Text (Text)

selectForAccount
  :: Connection
  -> Text
  -> IO [LiquidityRewardAndOperation]
selectForAccount conn accountId =
  query conn
  "SELECT lr.liquidity_reward_id,lr.account_id,lr.xtz_reward,lr.token_reward,lr.dexter_operation_id,dop.dexter_contract_id,dop.block_level,dop.operation_x,dop.operation_y,dop.content_z,dop.lqt_total,dop.token_pool,dop.xtz_pool,dop.timestamp FROM liquidity_reward AS lr INNER JOIN dexter_operation AS dop ON dop.dexter_operation_id = lr.dexter_operation_id WHERE lr.account_id = ?"
  (Only accountId)
