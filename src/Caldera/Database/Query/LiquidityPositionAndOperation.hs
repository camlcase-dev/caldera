{-# LANGUAGE OverloadedStrings   #-}

module Caldera.Database.Query.LiquidityPositionAndOperation where

import Caldera.Database.Table.LiquidityPositionAndOperation
import Database.PostgreSQL.Simple
import Data.Maybe (listToMaybe)
import Data.Text (Text)

selectForAccount
  :: Connection
  -> Text
  -> IO [LiquidityPositionAndOperation]
selectForAccount conn accountId =
  query conn
  "SELECT lp.liquidity_position_id,lp.account_id,lp.lqt,lp.dexter_operation_id,dop.dexter_contract_id,dop.block_level,dop.operation_x,dop.operation_y,dop.content_z,dop.lqt_total,dop.token_pool,dop.xtz_pool,dop.timestamp FROM liquidity_position AS lp INNER JOIN dexter_operation AS dop ON dop.dexter_operation_id = lp.dexter_operation_id WHERE lp.account_id = ?"
  (Only accountId)

selectMostRecentPositionForAccount
  :: Connection
  -> Text
  -> Text
  -> IO (Maybe LiquidityPositionAndOperation)
selectMostRecentPositionForAccount conn accountId contractId =
  fmap listToMaybe $
  query conn
  "SELECT lp.liquidity_position_id,lp.account_id,lp.lqt,lp.dexter_operation_id,dop.dexter_contract_id,dop.block_level,dop.operation_x,dop.operation_y,dop.content_z,dop.lqt_total,dop.token_pool,dop.xtz_pool,dop.timestamp FROM liquidity_position AS lp INNER JOIN dexter_operation AS dop ON dop.dexter_operation_id = lp.dexter_operation_id WHERE lp.account_id = ? AND dop.dexter_contract_id = ? ORDER BY dop.block_level DESC LIMIT 1"
  (accountId, contractId)
