{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE Strict #-} 

module Caldera.Database.Query.DexterContractAndOperation(
    selectOperationsForDay
  , selectLatest  
  , selectLatestForContract
  , selectClosestBlockLevel
  , selectClosestBlockLevelForContract
  , toContract
  ) where

import Data.Either (partitionEithers)
import Data.Int (Int64)
import Data.List (intercalate)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import qualified Data.Text as T
import Caldera.API.Types (Contract(..))
import Caldera.Database.Table.DexterContractAndOperation
import Database.PostgreSQL.Simple (Connection, Only(..), query)
import Safe (headMay)
import Data.Time (Day)
import Caldera.Database (catchSqlErrors)
import Caldera.Database.Table.DexterContract
import qualified Caldera.Database.Query.DexterContract as DC

-- | For each contract, get the latest operation
selectLatest :: Connection -> IO (Either String [DexterContractAndOperation])
selectLatest conn = do
  eContracts <- catchSqlErrors $ DC.selectAll conn
  case eContracts of
    Left err -> pure $ Left err
    Right contracts -> do
      eResults <- mapM (catchSqlErrors . selectLatestForContract conn) (fmap dcDexterContractId contracts)
      let (errors, results) = partitionEithers eResults
      if length errors > 0
        then pure . Left $ intercalate ", " errors
        else pure . Right . catMaybes $ results

selectLatestForContract :: Connection -> Text -> IO (Maybe DexterContractAndOperation)
selectLatestForContract conn contractId = do
  headMay <$> query conn "SELECT dop.dexter_operation_id, dop.dexter_contract_id, dop.block_level, dop.block_hash, dop.timestamp, dop.protocol, dop.operation_x, dop.operation_y, dop.content_z, dop.source, dop.entrypoint, dop.lqt_total, dop.token_pool, dop.xtz_pool, dop.payload, dc.token_name, dc.token_symbol, dc.token_contract_id, dc.origination, dc.big_map_id, dc.decimals FROM dexter_operation AS dop INNER JOIN dexter_contract AS dc ON dop.dexter_contract_id = dc.dexter_contract_id WHERE dc.dexter_contract_id = ? ORDER BY ARRAY[block_level, operation_x, operation_y, content_z] DESC LIMIT 1" (Only contractId)

-- -- | For each contract, get the latest operation relative to the given to the target block level
selectClosestBlockLevel :: Connection -> Int64 -> IO (Either String [DexterContractAndOperation])
selectClosestBlockLevel conn blockLevel_ = do
  eContracts <- catchSqlErrors $ DC.selectAll conn
  case eContracts of
    Left err -> pure $ Left err
    Right contracts -> do
      eResults <- mapM (catchSqlErrors . selectClosestBlockLevelForContract conn blockLevel_) (fmap dcDexterContractId contracts)
      let (errors, results) = partitionEithers eResults
      if length errors > 0
        then pure . Left $ intercalate ", " errors
        else pure . Right . catMaybes $ results

selectClosestBlockLevelForContract :: Connection -> Int64 -> Text -> IO (Maybe DexterContractAndOperation)
selectClosestBlockLevelForContract conn blockLevel_ contractId = do
  headMay <$> query conn "SELECT dop.dexter_operation_id, dop.dexter_contract_id, dop.block_level, dop.block_hash, dop.timestamp, dop.protocol, dop.operation_x, dop.operation_y, dop.content_z, dop.source, dop.entrypoint, dop.lqt_total, dop.token_pool, dop.xtz_pool, dop.payload, dc.token_name, dc.token_symbol, dc.token_contract_id, dc.origination, dc.big_map_id, dc.decimals FROM dexter_operation AS dop INNER JOIN dexter_contract AS dc ON dop.dexter_contract_id = dc.dexter_contract_id WHERE block_level <= ? AND dc.dexter_contract_id = ? ORDER BY ARRAY[block_level, operation_x, operation_y, content_z] DESC LIMIT 1" (blockLevel_, contractId)
    
-- | this is only reliable if the database is up to date
selectOperationsForDay :: Connection -> Day -> IO (Either String [DexterContractAndOperation])
selectOperationsForDay conn day = do
  eContracts <- catchSqlErrors $ DC.selectAll conn
  case eContracts of
    Left err -> pure $ Left err
    Right contracts -> do
      eResults <- mapM (catchSqlErrors . selectOperationsForDayForContract conn day) (fmap dcDexterContractId contracts)
      let (errors, results) = partitionEithers eResults
      if length errors > 0
        then pure . Left $ intercalate ", " errors
        else pure . Right . catMaybes $ results

-- | this is only reliable if the database is up to date
selectOperationsForDayForContract :: Connection -> Day -> Text -> IO (Maybe DexterContractAndOperation)
selectOperationsForDayForContract conn day contractId = do
  headMay <$> query conn "SELECT dop.dexter_operation_id, dop.dexter_contract_id, dop.block_level, dop.block_hash, dop.timestamp, dop.protocol, dop.operation_x, dop.operation_y, dop.content_z, dop.source, dop.entrypoint, dop.lqt_total, dop.token_pool, dop.xtz_pool, dop.payload, dc.token_name, dc.token_symbol, dc.token_contract_id, dc.origination, dc.big_map_id, dc.decimals FROM dexter_operation AS dop INNER JOIN dexter_contract AS dc ON dop.dexter_contract_id = dc.dexter_contract_id WHERE dc.dexter_contract_id = ? AND timestamp <= ? ORDER BY timestamp DESC LIMIT 1" (contractId, day)

-- =============================================================================
-- Interpret the table into a Contract type
-- =============================================================================

toContract :: DexterContractAndOperation -> Contract
toContract dco =
  Contract
    { contract          = dcoDexterContractId dco
    , contractName      = "XTZ/" <> dcoTokenSymbol dco
    , contractDecimals  = fromInteger $ dcoDecimals dco
    , blockLevel        = dcoBlockLevel dco
    , blockHash         = dcoBlockHash dco
    , contractLqtTotal  = dcoLqtTotal dco
    , contractTokenPool = dcoTokenPool dco
    , contractXtzPool   = dcoXtzPool dco
    , contractTimestamp = T.pack . show $ dcoTimestamp dco
    }
