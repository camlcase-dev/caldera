{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Query.LiquidityPosition where

import Caldera.Database
import Caldera.Database.Table.LiquidityPosition
import Control.Monad (void)
import Data.Int (Int32, Int64)
import Data.Text (Text)
import qualified Database.Beam as B
import Database.Beam hiding (insert, timestamp)
import Database.Beam.Postgres (runBeamPostgres)
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField (toField)
import Safe (headMay)

-- | use insertExpressions in order to use auto increment
insert :: Connection -> LiquidityPosition -> IO ()
insert conn value = do  
  runBeamPostgres conn $ runInsert $
    B.insert (calderaLiquidityPosition calderaDb) $
    insertExpressions
      [LiquidityPosition
         { lpLiquidityPositionId = default_
         , lpAccountId           = val_ (lpAccountId value)
         , lpLqt                 = val_ (lpLqt value)
         , lpDexterOperationId   = val_ (lpDexterOperationId value)
         }]

-- | For a given account account id, contract id, block level, operation x, operation y 
-- find the closest 
selectClosest
  :: Connection
  -> Text
  -> Text
  -> Int64
  -> Int32
  -> Int32
  -> Int32
  -> IO (Maybe LiquidityPosition)
selectClosest conn accountId contractId blockLevel operationX operationY contentZ =
  fmap headMay $
    query conn
    "SELECT lp.liquidity_position_id,lp.account_id,lp.lqt,lp.dexter_operation_id FROM liquidity_position AS lp INNER JOIN dexter_operation AS dop ON dop.dexter_operation_id = lp.dexter_operation_id WHERE lp.account_id = ? AND dop.dexter_contract_id = ? AND ARRAY[dop.block_level, dop.operation_x, dop.operation_y, dop.content_z] <= ARRAY[?,?,?,?] ORDER BY ARRAY[dop.block_level, dop.operation_x, dop.operation_y, dop.content_z] DESC LIMIT 1"
    (accountId, contractId, blockLevel, operationX, operationY, contentZ)

upsert :: Connection -> LiquidityPosition -> IO ()
upsert conn value =
  void $ 
  execute
  conn
  "INSERT INTO liquidity_position (account_id, lqt, dexter_operation_id) VALUES (?,?,?) ON CONFLICT (account_id, dexter_operation_id) DO UPDATE SET lqt = ?"
  [ toField $ lpAccountId value
  , toField $ lpLqt value
  , toField $ lpDexterOperationId value
  , toField $ lpLqt value  
  ]
