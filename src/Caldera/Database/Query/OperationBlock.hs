{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Query.OperationBlock(
    selectMissing
  , selectLatest
  , insert
  , update
  ) where

import Caldera.Database
import Caldera.Database.Table.OperationBlock
import qualified Database.Beam as B
import Database.Beam hiding (insert, update)
import Database.PostgreSQL.Simple
import Database.Beam.Postgres (runBeamPostgres)

-- | Insert a new OperationBlock, will fail if blockLevel already exists
insert :: Connection -> OperationBlock -> IO ()
insert conn value =
  runBeamPostgres conn $ runInsert $
    B.insert (calderaOperationBlock calderaDb) $
    insertValues [value]

-- | Update an existing OperationBlock, changes the missing status
update :: Connection -> OperationBlock -> IO ()
update conn value =
  runBeamPostgres conn $
    runUpdate $
      save (calderaOperationBlock calderaDb) value
  
selectMissing :: Connection -> IO [OperationBlock]
selectMissing conn =
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      operationBlocks <- all_ (calderaOperationBlock calderaDb)
      guard_ (obMissing operationBlocks ==. val_ False) 
      pure operationBlocks
  
-- =============================================================================
-- Raw PostgreSQL
-- =============================================================================

selectLatest :: Connection -> IO [OperationBlock]
selectLatest conn =
  query_ conn
  "SELECT block_level, missing, timestamp FROM operation_block WHERE block_level = (SELECT MAX(block_level) FROM operation_block)"
