{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Query.LiquidityReward where

import Caldera.Database
import Caldera.Database.Table.LiquidityReward
import Control.Monad (void)
import qualified Database.Beam as B
import Database.Beam hiding (insert, timestamp)
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField (toField)
import Database.Beam.Postgres (runBeamPostgres)

-- | use insertExpressions in order to use auto increment
insert :: Connection -> LiquidityReward -> IO ()
insert conn value = do  
  runBeamPostgres conn $ runInsert $
    B.insert (calderaLiquidityReward calderaDb) $
    insertExpressions
      [LiquidityReward
         { lrLiquidityRewardId  = default_
         , lrAccountId          = val_ (lrAccountId value)
         , lrXtzReward          = val_ (lrXtzReward value)
         , lrTokenReward        = val_ (lrTokenReward value)
         , lrDexterOperationId  = val_ (lrDexterOperationId value)         
         }]

upsert :: Connection -> LiquidityReward -> IO ()
upsert conn value =
  void $ 
  execute
  conn
  "INSERT INTO liquidity_reward (account_id, xtz_reward, token_reward, dexter_operation_id) VALUES (?,?,?,?) ON CONFLICT (account_id, dexter_operation_id) DO UPDATE SET xtz_reward = ?, token_reward = ?"
  [ toField $ lrAccountId value
  , toField $ lrXtzReward value
  , toField $ lrTokenReward value
  , toField $ lrDexterOperationId value
  , toField $ lrXtzReward value
  , toField $ lrTokenReward value
  ]
