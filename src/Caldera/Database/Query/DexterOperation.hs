{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE Strict #-} 


module Caldera.Database.Query.DexterOperation(
    insert
  , selectOperationsForDay
  , parseDexterTransaction
  , selectAllLiquidityOperations
  , selectAllTrades
  , selectAllTradesByContractId
  , selectContractTradesLast24Hours
  , selectLast24Hours
  , selectLatestByContractId  
  , selectLessThanOrEqualToBlockLevel
  , selectSourceTransactions
  , selectSourceTransactionsLast24Hours
  , selectTradesFor24HourPeriod
  , safeParseBytes
  , count
  , DexterTransactionError(..)
  ) where

import Caldera.Database
import Caldera.Database.Table.DexterOperation
import qualified Database.Beam as B
import Database.Beam hiding (insert, timestamp)
import Database.PostgreSQL.Simple
import Database.Beam.Postgres (runBeamPostgres)

import qualified Control.Arrow as Arrow
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Types as Aeson
import Data.Text (Text)
import qualified Data.Text as T
import qualified Dexter.Exchange.Transaction.Parameters as Parameters
import qualified Tezos.Client.Types.Block     as Block
import Tezos.Client.Types.Core
import qualified Tezos.Client.Types.Micheline as Micheline
import qualified Tezos.Client.Types.Core      as CoreTypes
import Safe (headMay, readMay)
import Dexter.Exchange.Transaction
import Data.Maybe (catMaybes, fromMaybe)
import Data.Time
  (Day, LocalTime, addDays, addUTCTime, getCurrentTime, localTimeToUTC
  , utc, utcToLocalTime)
import Data.Int (Int64)

-- | use insertExpressions in order to use auto increment
insert :: Connection -> DexterOperation -> IO ()
insert conn value = do  
  runBeamPostgres conn $ runInsert $
    B.insert (calderaDexterOperation calderaDb) $
    insertExpressions
      [DexterOperation
         { doDexterOperationId = default_
         , doDexterContractId  = val_ (doDexterContractId value)
         , doTokenContractId   = val_ (doTokenContractId value)
         , doBlockLevel        = val_ (doBlockLevel value)
         , doBlockHash         = val_ (doBlockHash value)
         , doTimestamp         = val_ (doTimestamp value)
         , doProtocol          = val_ (doProtocol value)         
         , doOperationX        = val_ (doOperationX value)
         , doOperationY        = val_ (doOperationY value)
         , doContentZ          = val_ (doContentZ value)
         , doSource            = val_ (doSource value)
         , doEntrypoint        = val_ (doEntrypoint value)
         , doLqtTotal          = val_ (doLqtTotal value)
         , doTokenPool         = val_ (doTokenPool value)
         , doXtzPool           = val_ (doXtzPool value)
         , doPayload           = val_ (doPayload value)
         }]

selectSourceTransactions :: Connection -> Text -> IO [DexterOperation]
selectSourceTransactions conn accountId =
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      do_ <- all_ (calderaDexterOperation calderaDb)
      guard_ (doSource do_ ==. val_ accountId)
      pure do_

selectSourceTransactionsLast24Hours  :: Connection -> Text -> IO [DexterOperation]
selectSourceTransactionsLast24Hours conn accountId = do
  now <- getCurrentTime
  let _24hoursAgo = utcToLocalTime utc $ addUTCTime (- 86400) now
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      do_ <- all_ (calderaDexterOperation calderaDb)
      guard_ (doSource do_ ==. val_ accountId
             &&. doTimestamp do_ >=. val_ _24hoursAgo
             &&. doTimestamp do_ <=. val_ (utcToLocalTime utc $ now))
      pure do_
  
-- | Get the DexterOperation with the highest blockLevel for a contractId
selectLatestByContractId :: Connection -> Text -> IO (Maybe DexterOperation)
selectLatestByContractId conn contractId =
  fmap headMay $
    runBeamPostgres conn $
      runSelectReturningList $ select $ do
       do_ <- limit_ 1 $ orderBy_ (desc_ . doBlockLevel) $ all_ (calderaDexterOperation calderaDb)
       guard_ (doDexterContractId do_ ==. val_ contractId)
       pure do_

-- | Get all dexter operations for all contracts and entrypoints for the last 24 hours
selectLast24Hours  :: Connection -> IO [DexterOperation]
selectLast24Hours conn = do
  now <- getCurrentTime
  let _24hoursAgo = utcToLocalTime utc $ addUTCTime (- 86400) now
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      ops <- all_ (calderaDexterOperation calderaDb)
      guard_ (doTimestamp ops >=. val_ _24hoursAgo
               &&. doTimestamp ops <=. val_ (utcToLocalTime utc $ now)
             )
      pure ops

-- | Get all dexter trades (tokenToToken, tokenToXtz or xtzToToken) for all
-- contracts for the last 24 hours
selectContractTradesLast24Hours  :: Connection -> Text -> IO [DexterOperation]
selectContractTradesLast24Hours conn contractId = do
  now <- getCurrentTime
  let _24hoursAgo = utcToLocalTime utc $ addUTCTime (- 86400) now
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      ops <- all_ (calderaDexterOperation calderaDb)
      guard_ (doDexterContractId ops ==. val_ contractId
              &&. doTimestamp ops >=. val_ _24hoursAgo
              &&. doTimestamp ops <=. val_ (utcToLocalTime utc $ now)
              &&.
              (doEntrypoint ops ==. val_ "xtzToToken"
                ||. doEntrypoint ops ==. val_ "tokenToToken"
                ||. doEntrypoint ops ==. val_ "tokenToXtz"
              )               
             )
      pure ops

-- | Get the DexterOperation with the highest blockLevel for a contractId
selectLessThanOrEqualToBlockLevel :: Connection -> Int64 -> Text -> IO (Maybe DexterOperation)
selectLessThanOrEqualToBlockLevel conn blockLevel_ contractId =
  fmap headMay $
    runBeamPostgres conn $
      runSelectReturningList $ select $ do
       do_ <- limit_ 1 $ orderBy_ (desc_ . doBlockLevel) $ all_ (calderaDexterOperation calderaDb)
       guard_ (doDexterContractId do_ ==. val_ contractId &&. doBlockLevel do_ <=. val_ blockLevel_)
       pure do_

-- | select all liquidity operations (addLiquidity or removeLiquidity) for all contracts
selectAllLiquidityOperations :: Connection -> IO [DexterOperation]
selectAllLiquidityOperations conn =
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      ops <- all_ (calderaDexterOperation calderaDb)
      guard_ (doEntrypoint ops ==. val_ "addLiquidity"
               ||. doEntrypoint ops ==. val_ "removeLiquidity"
             )
      pure ops

-- | select all trades (tokenToToken, tokenToXtz or xtzToToken) for all contracts
selectAllTrades :: Connection -> IO [DexterOperation]
selectAllTrades conn =
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      ops <- all_ (calderaDexterOperation calderaDb)
      guard_ (doEntrypoint ops ==. val_ "xtzToToken"
               ||. doEntrypoint ops ==. val_ "tokenToToken"
               ||. doEntrypoint ops ==. val_ "tokenToXtz"
             )
      pure ops

-- | select all trades (tokenToToken, tokenToXtz or xtzToToken) for all contracts
selectAllTradesByContractId :: Connection -> Text -> IO [DexterOperation]
selectAllTradesByContractId conn contractId =
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      ops <- all_ (calderaDexterOperation calderaDb)
      guard_ (doDexterContractId ops ==. val_ contractId &&.
              (doEntrypoint ops ==. val_ "xtzToToken"
               ||. doEntrypoint ops ==. val_ "tokenToToken"
               ||. doEntrypoint ops ==. val_ "tokenToXtz"
              )
             )
      pure ops

-- | tokenToToken, tokenToXtz or xtzToToken
selectTradesBetween :: Connection -> Text -> LocalTime -> LocalTime -> IO [DexterOperation]
selectTradesBetween conn contractId start finish =
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
      ops <- all_ (calderaDexterOperation calderaDb)
      guard_ (doDexterContractId ops ==. val_ contractId
             &&. between_ (doTimestamp ops) (val_ start) (val_ finish)
             &&. (doEntrypoint ops ==. val_ "xtzToToken"
                  ||. doEntrypoint ops ==. val_ "tokenToToken"
                  ||. doEntrypoint ops ==. val_ "tokenToXtz"
                 )
             ) 
      pure ops

selectTradesFor24HourPeriod :: Connection -> Text -> LocalTime -> IO [DexterOperation]
selectTradesFor24HourPeriod conn contractId ts =
  let utcTime = localTimeToUTC utc ts
      hours24 = - (60 * 60 * 24)
      fromUTCTIme = addUTCTime hours24 utcTime
      fromLocalTime = utcToLocalTime utc fromUTCTIme
  in selectTradesBetween conn contractId fromLocalTime ts

-- | tokenToToken, tokenToXtz or xtzToToken
selectOperationsForDay :: Connection -> Text -> Day -> IO [DexterOperation]
selectOperationsForDay conn contractId day = 
  query conn "SELECT dexter_operation_id,dexter_contract_id,token_contract_id,block_level,block_hash,timestamp,protocol,operation_x,operation_y,content_z,source,entrypoint,lqt_total,token_pool,xtz_pool,payload FROM dexter_operation WHERE dexter_contract_id = ? AND timestamp BETWEEN ? AND ? AND (entrypoint = 'tokenToToken' OR entrypoint = 'tokenToXtz' OR entrypoint = 'xtzToToken')" (contractId, day, addDays 1 day)
  

-- | count the number of operations
count :: Connection -> IO (Maybe Int)
count conn = do
  mOnly <- fmap headMay $ query_ conn "SELECT count(dexter_operation_id) FROM dexter_operation"
  case mOnly of
    Just (Only c) -> pure (Just c)
    Nothing -> pure Nothing

-- =============================================================================
-- Interpret the table and payload into a specific dexter transaction
-- =============================================================================

-- | The details of a call to the FA1.2 entrypoint transfer
data FA1_2Transfer =
  FA1_2Transfer
    { ownerAddress     :: ContractId -- ^ the address of the FA1.2 owner
    , toAddress        :: ContractId -- ^ the address of whom will receive the FA1.2 in the transaction
    , valueTransferred :: Bignum -- ^ the amount of FA1.2 given from the owner address to the to address
    } deriving (Eq, Show)

-- | safely parse a contract id from michelson
safeParseBytes :: Micheline.Expression -> Maybe ContractId
safeParseBytes =
  \case
    Micheline.BytesExpression bytes -> CoreTypes.parseBytes bytes
    Micheline.StringExpression str -> Just . ContractId $ str
    _ -> Nothing

-- | Read the parameters of a call to an FA1.2 token, return result if it is
-- a call to the transfer entrypoint
getFA1_2Transfer :: Micheline.Expression -> Maybe FA1_2Transfer
getFA1_2Transfer expression =
  case expression of
    Micheline.Expression
      (Micheline.PrimitiveData Micheline.PDPair)
      (Just
       [ ownerAddressBytes
       , Micheline.Expression
           (Micheline.PrimitiveData Micheline.PDPair)
           (Just
             [ toAddressBytes
             , Micheline.IntExpression value
             ]
           )
           _
       ])
      _ -> FA1_2Transfer <$> safeParseBytes ownerAddressBytes <*> safeParseBytes toAddressBytes <*> pure value
    _ -> Nothing


toContractId :: Text -> ContractId
toContractId = ContractId . UnistringText

-- | Get the FA1.2 transfer from a Dexter transaction.
getTokenTransferTransaction
  :: Text
  -> Text
  -> Block.InternalOperationResult
  -> Maybe FA1_2Transfer
getTokenTransferTransaction src destination op =
  case op of
    Block.InternalOperationResultTransaction source' _nonce _amount destination' parameters _resultTransaction ->
      case toContractId src == source' && toContractId destination == destination' of
        False -> Nothing
        True -> do
          ps <- parameters
          if Micheline.entrypoint ps == EUnistring (UnistringText "transfer") then getFA1_2Transfer $ Micheline.value ps else Nothing
    _ -> Nothing

-- | Get the FA1.2 transfer from a Dexter transaction for TokenToToken's internal query
getTokenTransferTransactionForTokenToToken
  :: Text
  -> Block.InternalOperationResult
  -> Maybe FA1_2Transfer
getTokenTransferTransactionForTokenToToken src op =
  case op of
    Block.InternalOperationResultTransaction source' _nonce _amount _destination parameters _resultTransaction ->
      case toContractId src == source' of
        False -> Nothing
        True -> do
          ps <- parameters
          if Micheline.entrypoint ps == EUnistring (UnistringText "transfer") then getFA1_2Transfer $ Micheline.value ps else Nothing
    _ -> Nothing

-- | Get the XTZ transfer from a Dexter transaction.
getXtzTransfer
  :: Text
  -> Text
  -> Block.InternalOperationResult
  -> Maybe Mutez
getXtzTransfer src destination op =
  case op of
    Block.InternalOperationResultTransaction source' _nonce amount destination' parameters _resultTransaction ->
      if toContractId src == source' && toContractId destination == destination'
      then
        case parameters of
          Nothing -> Just amount
          _ -> Nothing
      else Nothing
    _ -> Nothing

-- | Get the XTZ transfer for tokenToToken's internal xtzToToken
getXtzTransferForTokenToToken
  :: Text
  -> Text
  -> Block.InternalOperationResult
  -> Maybe Mutez
getXtzTransferForTokenToToken src destination op =
  case op of
    Block.InternalOperationResultTransaction source' _nonce amount destination' parameters _resultTransaction ->
      if toContractId src == source' && toContractId destination == destination'
      then
        case parameters of
          Just ps ->
            if Micheline.entrypoint ps == EUnistring (UnistringText "xtzToToken") then Just amount else Nothing
          Nothing -> Nothing

      else Nothing
    _ -> Nothing

-- | Errors that can occur when trying to interpret the payload field of dexter_operation
data DexterTransactionError
  = ParseDexterOperationPayloadFailed Text
  | MissingParameters
  | MissingInternalOperations
  | ParameterParseError Parameters.ParseError -- ^ an unexpected parse error in the parameters
  | MissingTokenTransactions Parameters.DexterTransactionParameters -- ^ did not find expected token transaction (FA1.2 or FA2) in inter
  | MissingXtzTransactions Parameters.DexterTransactionParameters -- ^ did not find expected token transaction (FA1.2 or FA2) in inte
  | MissingBoth Parameters.DexterTransactionParameters -- ^ did not find expected token transaction (FA1.2 or FA2) in inte  
  deriving (Eq, Show)

parseDexterTransaction :: DexterOperation -> Either DexterTransactionError DexterTransaction
parseDexterTransaction dexterOperation = do
  -- get the stored OperationContentsResult
  (transaction, metadata) <-
    either
    (Left . ParseDexterOperationPayloadFailed . T.pack)
    Right
    ((Aeson.parseEither Aeson.parseJSON $ doPayload dexterOperation)
     :: Either String (Block.OperationTransaction, (Block.InternalOperationResultsMetadata Block.Transaction)))

  -- get the transaction parameters
  transactionParameters <-
    maybe
    (Left MissingParameters)
    Right
    (Block.parameters (transaction :: Block.OperationTransaction))

  -- parse the dexter entry point
  dexterParameters <-
    Arrow.left
    ParameterParseError
    (Parameters.parseDexterEntrypointParameters transactionParameters)

  -- all dexter exchanges and liquidity actions we are interested in have internal operations
  internalOps <-
    maybe
    (Left MissingInternalOperations)
    Right
    (Block.internalOperationResults metadata)

  case dexterParameters of
    Parameters.XtzToToken ps ->
      -- get FA1.2 transfers from the internal operations, check that the source
      -- is Dexter and the destination is its corresponding FA1.2 call
      let toAddress_ =
            (Parameters.toAddress (ps :: Parameters.XtzToTokenTransactionParameters))
          transfers =
            filter
            (\t -> toAddress t == (toContractId toAddress_) && (ownerAddress t) == (toContractId $ doDexterContractId dexterOperation))
            $ catMaybes
            $ getTokenTransferTransaction (doDexterContractId dexterOperation) (doTokenContractId dexterOperation) <$> internalOps

      in case headMay transfers of
        Nothing -> Left $ MissingTokenTransactions dexterParameters
        Just transfer ->
          Right $ Dexter.Exchange.Transaction.XtzToToken $ XtzToTokenTransaction
            (doDexterContractId dexterOperation)
            (ContractId . UnistringText $ doSource dexterOperation)
            toAddress_
            (Block.amount (transaction :: Block.OperationTransaction))
            (Parameters.minTokenBought (ps :: Parameters.XtzToTokenTransactionParameters))
            (Parameters.deadline (ps :: Parameters.XtzToTokenTransactionParameters))
            (valueTransferred transfer)
            (fromMaybe 0 . readMay . T.unpack . doLqtTotal $ dexterOperation)
            (fromMaybe 0 . readMay . T.unpack . doTokenPool $ dexterOperation)
            (maybe zeroMutez unsafeMkMutez . readMay . T.unpack . doXtzPool $ dexterOperation)

    Parameters.TokenToXtz ps ->
      -- there should be an xtz transfer from dexter to the to address
      let toAddress_ =
            (Parameters.toAddress (ps :: Parameters.TokenToXtzTransactionParameters))
          xtzTransfers =
            (catMaybes $
              getXtzTransfer
              (doDexterContractId dexterOperation)
              (toAddress_) <$> internalOps)
      in case headMay xtzTransfers of
        Nothing -> Left $ MissingXtzTransactions dexterParameters
        Just totalXtzAmount ->
          Right $ Dexter.Exchange.Transaction.TokenToXtz $ TokenToXtzTransaction
            (doDexterContractId dexterOperation)
            (ContractId . UnistringText $ doSource dexterOperation)
            (Parameters.ownerAddress (ps :: Parameters.TokenToXtzTransactionParameters))
            toAddress_
            (Parameters.tokensSold (ps :: Parameters.TokenToXtzTransactionParameters))
            (Parameters.minXtzBought (ps :: Parameters.TokenToXtzTransactionParameters))
            (Parameters.deadline (ps :: Parameters.TokenToXtzTransactionParameters))
            totalXtzAmount
            (fromMaybe 0 . readMay . T.unpack . doLqtTotal $ dexterOperation)
            (fromMaybe 0 . readMay . T.unpack . doTokenPool $ dexterOperation)
            (maybe zeroMutez unsafeMkMutez . readMay . T.unpack . doXtzPool $ dexterOperation)

    Parameters.TokenToToken ps ->
      -- there should be an XTZ transfer from dexter to the outputDexterContract address
      let tokensBoughtDexterContract_ =
            Parameters.tokensBoughtDexterContract (ps :: Parameters.TokenToTokenTransactionParameters)
          toAddress_ =
            Parameters.toAddress (ps :: Parameters.TokenToTokenTransactionParameters)
          xtzTransfers =
            (catMaybes $
              getXtzTransferForTokenToToken
              (doDexterContractId dexterOperation)
              (tokensBoughtDexterContract_) <$> internalOps)
          -- get the FA1.2 transfer from dexter to the to address
          transfers =
            filter (\t -> toAddress t == (ContractId $ UnistringText toAddress_))
            $ catMaybes
            $ getTokenTransferTransactionForTokenToToken tokensBoughtDexterContract_ <$> internalOps

      in case (headMay xtzTransfers, headMay transfers) of
        (Just internalXtzSold_, Just transfer) ->
          Right $ Dexter.Exchange.Transaction.TokenToToken $ TokenToTokenTransaction
          (doDexterContractId dexterOperation)
          (ContractId . UnistringText $ doSource dexterOperation)
          tokensBoughtDexterContract_
          (Parameters.minTokensBought (ps :: Parameters.TokenToTokenTransactionParameters))
          (Parameters.ownerAddress (ps :: Parameters.TokenToTokenTransactionParameters))
          toAddress_
          (Parameters.tokensSold (ps :: Parameters.TokenToTokenTransactionParameters))
          (Parameters.deadline (ps :: Parameters.TokenToTokenTransactionParameters))
          internalXtzSold_
          (valueTransferred transfer)
          (fromMaybe 0 . readMay . T.unpack . doLqtTotal $ dexterOperation)
          (fromMaybe 0 . readMay . T.unpack . doTokenPool $ dexterOperation)
          (maybe zeroMutez unsafeMkMutez . readMay . T.unpack . doXtzPool $ dexterOperation)          
        (Just _, Nothing) -> Left $ MissingTokenTransactions dexterParameters
        (Nothing, Just _) -> Left $ MissingXtzTransactions dexterParameters
        _ -> Left $ MissingBoth dexterParameters

    Parameters.AddLiquidity ps ->
      let transfers =
            filter (\t -> toAddress t == (toContractId $ doDexterContractId dexterOperation))
            $ catMaybes
            $ getTokenTransferTransaction (doDexterContractId dexterOperation) (doTokenContractId dexterOperation) <$> internalOps
            
      in case headMay transfers of
        Nothing -> Left $ MissingTokenTransactions dexterParameters
        Just transfer ->
          Right $ Dexter.Exchange.Transaction.AddLiquidity $ AddLiquidityTransaction
          (doDexterContractId dexterOperation)
          (ContractId . UnistringText $ doSource dexterOperation)
          (Parameters.ownerAddress (ps :: Parameters.AddLiquidityTransactionParameters))
          (Parameters.minLqtMinted (ps :: Parameters.AddLiquidityTransactionParameters))
          (Parameters.maxTokensDeposited (ps :: Parameters.AddLiquidityTransactionParameters))
          (Block.amount (transaction :: Block.OperationTransaction))
          (Parameters.deadline (ps :: Parameters.AddLiquidityTransactionParameters))
          (valueTransferred transfer)
          (fromMaybe 0 . readMay . T.unpack . doLqtTotal $ dexterOperation)
          (fromMaybe 0 . readMay . T.unpack . doTokenPool $ dexterOperation)
          (maybe zeroMutez unsafeMkMutez . readMay . T.unpack . doXtzPool $ dexterOperation)
          
    Parameters.RemoveLiquidity ps ->
      -- there should be an XTZ transfer from dexter to the outputDexterContract address
      let toAddress_ = Parameters.toAddress (ps :: Parameters.RemoveLiquidityTransactionParameters)
          xtzTransfers =
            (catMaybes $
              getXtzTransfer
              (doDexterContractId dexterOperation)
              toAddress_ <$> internalOps)            
          -- get the FA1.2 transfer from dexter to the to address
          transfers =
            filter (\t -> toAddress t == (ContractId $ UnistringText toAddress_))
            $ catMaybes
            $ getTokenTransferTransaction (doDexterContractId dexterOperation) (doTokenContractId dexterOperation) <$> internalOps
            
      in case (headMay xtzTransfers, headMay transfers) of
        (Just xtzWithdrawn, Just transfer) ->
          Right $ Dexter.Exchange.Transaction.RemoveLiquidity $ RemoveLiquidityTransaction
          (doDexterContractId dexterOperation)
          (ContractId . UnistringText $ doSource dexterOperation)
          (Parameters.ownerAddress (ps :: Parameters.RemoveLiquidityTransactionParameters))
          toAddress_
          (Parameters.lqtBurned (ps :: Parameters.RemoveLiquidityTransactionParameters))
          (Parameters.minXtzWithdrawn (ps :: Parameters.RemoveLiquidityTransactionParameters))
          (Parameters.minTokensWithdrawn (ps :: Parameters.RemoveLiquidityTransactionParameters))
          (Parameters.deadline (ps :: Parameters.RemoveLiquidityTransactionParameters))
          xtzWithdrawn
          (valueTransferred transfer)
          (fromMaybe 0 . readMay . T.unpack . doLqtTotal $ dexterOperation)
          (fromMaybe 0 . readMay . T.unpack . doTokenPool $ dexterOperation)
          (maybe zeroMutez unsafeMkMutez . readMay . T.unpack . doXtzPool $ dexterOperation)
          
        (Just _, Nothing) -> Left $ MissingTokenTransactions dexterParameters
        (Nothing, Just _) -> Left $ MissingXtzTransactions dexterParameters
        _ -> Left $ MissingBoth dexterParameters
