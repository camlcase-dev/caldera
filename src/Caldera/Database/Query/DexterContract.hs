{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Query.DexterContract(
    insert
  , selectAll
  , getContractToSymbolMap
  , getContractByDexterContractId
  ) where

import qualified Data.Map as Map
import Data.Text (Text)
import qualified Database.Beam as B
import Caldera.Database
import Caldera.Database.Table.DexterContract
import Database.Beam hiding (insert, timestamp)
import Database.Beam.Postgres (runBeamPostgres)
import Database.PostgreSQL.Simple (Connection, query_)
import Safe (headMay)

-- | insert a DexterContract
insert :: Connection -> DexterContract -> IO ()
insert conn value = do  
  runBeamPostgres conn $ runInsert $
    B.insert (calderaDexterContract calderaDb) $
    insertValues [value]

-- | Get the DexterOperation with the highest blockLevel for a contractId
selectAll :: Connection -> IO [DexterContract]
selectAll conn =
  runBeamPostgres conn $
    runSelectReturningList $ select $
     all_ (calderaDexterContract calderaDb)

-- | Get the dexter contract id to symbol for lookup
getContractToSymbolMap :: Connection -> IO (Map.Map Text Text)
getContractToSymbolMap conn =
  fmap Map.fromList
  (query_
   conn
   "SELECT dexter_contract_id, token_symbol FROM dexter_contract" :: IO [(Text,Text)]
  )

-- | so far all of the symbols are unique, but if they are not in the future
-- we will need to take a different approach
getContractByDexterContractId :: Connection -> Text -> IO (Maybe DexterContract)
getContractByDexterContractId conn dexterContractId =
  fmap headMay $
  runBeamPostgres conn $
    runSelectReturningList $ select $ do
     cs <- all_ (calderaDexterContract calderaDb)
     guard_ (dcDexterContractId cs ==. val_ dexterContractId)
     pure cs
