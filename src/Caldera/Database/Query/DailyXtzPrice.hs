{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Caldera.Database.Query.DailyXtzPrice(
    update
  , insert
  , upsert
  , getDatesWithData
  , selectPriceForDay
  , selectLastKnownPrice
  , selectAllCurrencyPrices
  , selectCurrencyPricesForRange
  ) where

import qualified Database.Beam as B

import Control.Monad (void)
import Caldera.Database
import Caldera.Database.Table.DailyXtzPrice
import Data.Int (Int64)
import Data.Maybe (listToMaybe)
import Data.Time (Day)
import Data.Text (Text)
import Database.Beam hiding (insert, date, update)
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField (toField)
import Database.Beam.Postgres (runBeamPostgres)

insert :: Connection -> DailyXtzPrice -> IO ()
insert conn value =
  runBeamPostgres conn $ runInsert $
    B.insert (calderaDailyXtzPrice calderaDb) $
    insertExpressions
      [DailyXtzPrice
        { dxpDailyXtzPriceId = default_
        , dxpVsCurrency = val_ (dxpVsCurrency value)
        , dxpPrice = val_ (dxpPrice value)
        , dxpDate = val_ (dxpDate value)
        }
      ]

-- | daily_xtz_price has a UNIQUE constraint on (vs_currency, price)
upsert :: Connection -> DailyXtzPrice -> IO ()
upsert conn value =
  void $ 
  execute
  conn
  "INSERT INTO daily_xtz_price (vs_currency, price, date) VALUES (?,?,?) ON CONFLICT (vs_currency, date) DO UPDATE SET price = ?"
  [ toField $ dxpVsCurrency value
  , toField $ dxpPrice value
  , toField $ dxpDate value
  , toField $ dxpPrice value
  ]

  
-- | Query the dates that are known for a particular currency
getDatesWithData :: Connection -> Text -> IO [Day]
getDatesWithData conn currency = do
  prices <-
    query
    conn
    "SELECT daily_xtz_price_id,vs_currency,price,date FROM daily_xtz_price WHERE vs_currency = ? ORDER BY date ASC"
    (Only currency) :: IO [DailyXtzPrice]
  pure $ fmap dxpDate prices

-- =============================================================================
-- Raw PostgreSQL
-- =============================================================================

-- | This violates beams model of using the primary key to update a table
update :: Connection -> DailyXtzPrice -> IO Int64
update conn table =
  execute
  conn
  "UPDATE daily_xtz_price SET price = ? WHERE date = ?"
  (dxpPrice table, dxpDate table)

-- | For 
selectLastKnownPrice :: Connection -> Text -> IO (Maybe DailyXtzPrice)
selectLastKnownPrice conn currency =
  fmap listToMaybe $
  query
  conn
  "SELECT daily_xtz_price_id,vs_currency,price,date FROM daily_xtz_price WHERE vs_currency = ? ORDER BY date DESC"
  (Only currency)

selectPriceForDay :: Connection -> Day -> IO [DailyXtzPrice]
selectPriceForDay conn day =
  query
  conn
  "SELECT DISTINCT daily_xtz_price_id,vs_currency,price,date FROM daily_xtz_price WHERE date = ?"
  (Only day)

selectCurrencyPricesForRange :: Connection -> Text -> Day -> Day -> IO [DailyXtzPrice]
selectCurrencyPricesForRange conn vsCurrency start end =
  query
  conn
  "SELECT daily_xtz_price_id,vs_currency,price,date FROM daily_xtz_price WHERE vs_currency = ? AND date >= ? AND date <= ? ORDER BY date DESC"
  (vsCurrency, start, end)

selectAllCurrencyPrices  :: Connection -> Text -> IO [DailyXtzPrice]
selectAllCurrencyPrices conn vsCurrency =
  query
  conn
  "SELECT daily_xtz_price_id,vs_currency,price,date FROM daily_xtz_price WHERE vs_currency = ? ORDER BY date DESC"
  (Only vsCurrency)
