module Caldera.Database.Connection(
    connectToDb
  ) where

-- Config
import qualified Caldera.Config as Caldera

-- Core
import qualified Data.Text as T

-- Database
import Database.PostgreSQL.Simple
  (Connection, ConnectInfo(connectHost, connectDatabase, connectUser, connectPassword), defaultConnectInfo, connect )

connectToDb :: Caldera.PostgresConfig -> IO Connection
connectToDb config = do
  let dbconf =
        defaultConnectInfo
        { connectHost = T.unpack $ Caldera.psqlHost config
        , connectDatabase = T.unpack $ Caldera.psqlDb config
        , connectUser = T.unpack $ Caldera.psqlUser config
        , connectPassword = T.unpack $ Caldera.psqlPassword config
        }
  connect dbconf
