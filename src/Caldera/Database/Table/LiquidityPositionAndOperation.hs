{-# LANGUAGE DeriveGeneric       #-}

module Caldera.Database.Table.LiquidityPositionAndOperation where

import Data.Int (Int32, Int64)
import Data.Text (Text)
import Data.Time (LocalTime)
import Database.PostgreSQL.Simple (FromRow)
import GHC.Generics (Generic)

data LiquidityPositionAndOperation
    = LiquidityPositionAndOperation
    { lpoLiquidityPositionId :: Int64 -- ^ an incrementing, unique idea, comes from the database
    , lpoAccountId           :: Text -- ^ the tz or KT address who earned this reward
    , lpoLqt                 :: Text -- ^ the amount of liquidity the account holds after the operations
    , lpoDexterOperationId   :: Int64 -- ^ the dexter operation id that this reward corresponds to
    , lpoDexterContractId    :: Text -- ^ KT1 address of corresponding dexter contract
    , lpoBlockLevel          :: Int64 -- ^ the block level that this was found at
    , lpoOperationX          :: Int32 -- ^ the block has a structure of operations: [[[OperationContentAndResult]]], this is the x index
    , lpoOperationY          :: Int32 -- ^ the y index
    , lpoContentZ            :: Int32 -- ^ the z index
    , lpoLqtTotal            :: Text -- ^ storage total liquidity after this operation
    , lpoTokenPool           :: Text -- ^ storage token pool after this operation
    , lpoXtzPool             :: Text -- ^ storage xtz pool after this operation
    , lpoTimestamp           :: LocalTime -- ^ time of the block
    } deriving (Eq, Show, Generic)

instance FromRow LiquidityPositionAndOperation
