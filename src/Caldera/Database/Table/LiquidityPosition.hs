{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Table.LiquidityPosition where

import Data.Int (Int64)
import Data.Text (Text)
import Database.Beam
import Database.PostgreSQL.Simple (FromRow)

-- | The amount of liquidity that an address (tz or KT) holds at a particular
-- block. This position is true for the corresponding block up until the block
-- that has a new position for this particular account id.
data LiquidityPositionT f
    = LiquidityPosition
    { lpLiquidityPositionId :: C f Int64 -- ^ an incrementing, unique idea, comes from the database
    , lpAccountId           :: C f Text -- ^ the tz or KT address that owns liquidity
    , lpLqt                 :: C f Text -- ^ the amount of liquidity the account holds after the operations
    , lpDexterOperationId   :: C f Int64 -- ^ the dexter operation id that this position corresponds to
    } deriving Generic

instance Beamable LiquidityPositionT
instance Table LiquidityPositionT where
   data PrimaryKey LiquidityPositionT f = LiquidityPositionId (Columnar f Int64) deriving (Generic, Beamable)
   primaryKey = LiquidityPositionId . lpLiquidityPositionId
   
type LiquidityPosition = LiquidityPositionT Identity
type LiquidityPositionId = PrimaryKey LiquidityPositionT Identity

deriving instance Show (PrimaryKey LiquidityPositionT Identity)
deriving instance Show LiquidityPosition
deriving instance Eq (PrimaryKey LiquidityPositionT Identity)
deriving instance Eq LiquidityPosition

instance FromRow LiquidityPosition
