{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Table.DailyXtzPrice(
    DailyXtzPriceT(..)
  , DailyXtzPrice
  , DailyXtzPriceId
  ) where

import Data.Int (Int64)
import Data.Text (Text)
import Data.Time (Day)
import Database.Beam hiding (date)
import Database.PostgreSQL.Simple

data DailyXtzPriceT f =
  DailyXtzPrice
    { dxpDailyXtzPriceId :: C f Int64
    , dxpVsCurrency :: Columnar f Text
    , dxpPrice :: Columnar f Double
    , dxpDate :: Columnar f Day
    } deriving (Generic)

-- UNIQUE (vs_currency, date)

instance Beamable DailyXtzPriceT
instance Table DailyXtzPriceT where
   data PrimaryKey DailyXtzPriceT f = DailyXtzPriceId (Columnar f Int64) deriving (Generic, Beamable)
   primaryKey = DailyXtzPriceId . dxpDailyXtzPriceId
   
type DailyXtzPrice = DailyXtzPriceT Identity
type DailyXtzPriceId = PrimaryKey DailyXtzPriceT Identity

deriving instance Show (PrimaryKey DailyXtzPriceT Identity)
deriving instance Show DailyXtzPrice
deriving instance Eq DailyXtzPrice

-- for raw queries
instance FromRow DailyXtzPrice
