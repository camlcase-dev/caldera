{-# LANGUAGE DeriveGeneric       #-}

module Caldera.Database.Table.LiquidityRewardAndOperation where

import Data.Int (Int32, Int64)
import Data.Text (Text)
import Data.Time (LocalTime)
import Database.PostgreSQL.Simple (FromRow)
import GHC.Generics (Generic)

data LiquidityRewardAndOperation
    = LiquidityRewardAndOperation
    { lroLiquidityRewardId :: Int64 -- ^ an incrementing, unique idea, comes from the database
    , lroAccountId         :: Text -- ^ the tz or KT address who earned this reward
    , lroXtzReward         :: Text -- ^ the amount of XTZ earned from the transaction in this operation
    , lroTokenReward       :: Text -- ^ the amount of token earned from the transaction in this operation
    , lroDexterOperationId :: Int64 -- ^ the dexter operation id that this reward corresponds to
    , lroDexterContractId  :: Text -- ^ the dexter contract that the reward corresponds to
    , lroBlockLevel        :: Int64 -- ^ the block level that this was found at
    , lroOperationX        :: Int32 -- ^ the block has a structure of operations: [[[OperationContentAndResult]]], this is the x index
    , lroOperationY        :: Int32 -- ^ the y index
    , lroContentZ          :: Int32 -- ^ the z index
    , lroLqtTotal          :: Text -- ^ storage total liquidity after this operation
    , lroTokenPool         :: Text -- ^ storage token pool after this operation
    , lroXtzPool           :: Text -- ^ storage xtz pool after this operation
    , lroTimestamp         :: LocalTime -- ^ time of the block
    } deriving (Eq, Show, Generic)

instance FromRow LiquidityRewardAndOperation
