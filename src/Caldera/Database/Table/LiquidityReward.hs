{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Table.LiquidityReward where

import Data.Int (Int64)
import Data.Text (Text)
import Database.Beam
import Database.PostgreSQL.Simple (FromRow)

-- | Individual liquity rewards. This are slow to calculate so it is better to store them in the database.
data LiquidityRewardT f
    = LiquidityReward
    { lrLiquidityRewardId :: C f Int64 -- ^ an incrementing, unique idea, comes from the database
    , lrAccountId         :: C f Text -- ^ the tz or KT address who earned this reward
    , lrXtzReward         :: C f Text -- ^ the amount of XTZ earned from the transaction in this operation
    , lrTokenReward       :: C f Text -- ^ the amount of token earned from the transaction in this operation
    , lrDexterOperationId :: C f Int64 -- ^ the dexter operation id that this reward corresponds to    
    } deriving Generic

instance Beamable LiquidityRewardT
instance Table LiquidityRewardT where
   data PrimaryKey LiquidityRewardT f = LiquidityRewardId (Columnar f Int64) deriving (Generic, Beamable)
   primaryKey = LiquidityRewardId . lrLiquidityRewardId
   
type LiquidityReward = LiquidityRewardT Identity
type LiquidityRewardId = PrimaryKey LiquidityRewardT Identity

deriving instance Show (PrimaryKey LiquidityRewardT Identity)
deriving instance Show LiquidityReward
deriving instance Eq (PrimaryKey LiquidityRewardT Identity)
deriving instance Eq LiquidityReward

instance FromRow LiquidityReward
