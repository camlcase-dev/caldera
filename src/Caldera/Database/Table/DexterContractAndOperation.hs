{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Table.DexterContractAndOperation(
    DexterContractAndOperation(..)
  ) where

import qualified Data.Aeson as Aeson
import Data.Int (Int32, Int64)
import Data.Text (Text)
import Data.Time (LocalTime)
import Database.Beam
import Database.PostgreSQL.Simple (FromRow)

-- | A combination of DexterContract and DexterOperation
data DexterContractAndOperation
    = DexterContractAndOperation
    { dcoDexterOperationId :: Int64 -- ^ an incrementing, unique idea, comes from the database
    , dcoDexterContractId  :: Text -- ^ the dexter contract id that this operation corresponds to
    , dcoBlockLevel        :: Int64 -- ^ the block level that this was found at
    , dcoBlockHash         :: Text -- ^ hash of the block
    , dcoTimestamp         :: LocalTime  -- ^ time of the block
    , dcoProtocol          :: Text -- ^ the protocol at which this was queried, this determines how to parse the json payload    
    , dcoOperationX        :: Int32 -- ^ the block has a structure of operations: [[[OperationContentAndResult]]], this is the x index
    , dcoOperationY        :: Int32 -- ^ the y index
    , dcoContentZ          :: Int32 -- ^ the z index
    , dcoSource            :: Text -- ^ the address that initiated this operation    
    , dcoEntrypoint        :: Text -- ^ the dexter entrypoint that was called
    , dcoLqtTotal          :: Text -- ^ storage total liquidity after this operation
    , dcoTokenPool         :: Text -- ^ storage token pool after this operation
    , dcoXtzPool           :: Text -- ^ storage xtz pool after this operation
    , dcoPayload           :: Aeson.Value -- ^ the OperationContentAndResult payload as json
    , dcoTokenName         :: Text -- ^ the name of the FA1.2/FA2 token associated with this contract
    , dcoTokenSymbol       :: Text -- ^ the symbol of the FA1.2/FA2 token associated with this contract
    , dcoTokenContractId   :: Text -- ^ the token contract id dexter is paired with
    , dcoOrigination       :: Int64 -- ^ the block at which this dexter contract was originated at
    , dcoBigMapId          :: Integer -- ^ the dexter contract's big map id
    , dcoDecimals          :: Integer -- ^ the number of decimals the token has
    } deriving (Eq, Generic, Show)

instance FromRow DexterContractAndOperation
