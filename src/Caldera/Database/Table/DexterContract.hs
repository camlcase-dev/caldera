{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Table.DexterContract(
    DexterContract
  , DexterContractT(..)
  , DexterContractId
  ) where

import Data.Int (Int64)
import Data.Text (Text)
import Database.Beam
import Database.PostgreSQL.Simple (FromRow)

-- | Static information about a Dexter contract
data DexterContractT f
    = DexterContract
    { dcTokenName        :: C f Text -- ^ the name of the FA1.2/FA2 token associated with this contract
    , dcTokenSymbol      :: C f Text -- ^ the symbol of the FA1.2/FA2 token associated with this contract
    , dcDexterContractId :: C f Text -- ^ the dexter contract id
    , dcTokenContractId  :: C f Text -- ^ the token contract id dexter is paired with
    , dcOrigination      :: C f Int64 -- ^ the block at which this dexter contract was originated at
    , dcBigMapId         :: C f Integer -- ^ the dexter contract's big map id
    , dcDecimals         :: C f Integer -- ^ the number of decimals the token has
    } deriving Generic

instance Beamable DexterContractT
instance Table DexterContractT where
   data PrimaryKey DexterContractT f = DexterContractId (Columnar f Text) deriving (Generic, Beamable)
   primaryKey = DexterContractId . dcDexterContractId
   
type DexterContract = DexterContractT Identity
type DexterContractId = PrimaryKey DexterContractT Identity

deriving instance Show (PrimaryKey DexterContractT Identity)
deriving instance Show DexterContract
deriving instance Eq (PrimaryKey DexterContractT Identity)
deriving instance Eq DexterContract

instance FromRow DexterContract