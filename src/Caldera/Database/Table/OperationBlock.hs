{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Table.OperationBlock(
    OperationBlockT(..)
  , OperationBlock
  , OperationBlockId
  ) where

import Data.Int (Int64)
import Data.Time (LocalTime)
import Database.Beam
import Database.PostgreSQL.Simple

data OperationBlockT f =
  OperationBlock
    { obBlockLevel :: C f Int64
    , obMissing    :: C f Bool
    , obTimestamp  :: C f LocalTime
    } deriving (Generic)

instance Beamable OperationBlockT
instance Table OperationBlockT where
   data PrimaryKey OperationBlockT f = OperationBlockId (Columnar f Int64) deriving (Generic, Beamable)
   primaryKey = OperationBlockId . obBlockLevel
   
type OperationBlock = OperationBlockT Identity
type OperationBlockId = PrimaryKey OperationBlockT Identity

deriving instance Show (PrimaryKey OperationBlockT Identity)
deriving instance Show OperationBlock
deriving instance Eq OperationBlock

-- for raw queries
instance FromRow OperationBlock
