{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database.Table.DexterOperation(
    DexterOperation
  , DexterOperationT(..)
  , DexterOperationId
  ) where

import qualified Data.Aeson as Aeson
import Data.Int (Int32, Int64)
import Data.Text (Text)
import Data.Time (LocalTime)
import Database.Beam
import Database.PostgreSQL.Simple (FromRow)

-- | An operation can be uniquely identified by a four value tuple: (block level, index x, index y, index z)
data DexterOperationT f
    = DexterOperation
    { doDexterOperationId :: C f Int64 -- ^ an incrementing, unique idea, comes from the database
    , doDexterContractId  :: C f Text -- ^ the dexter contract id that this operation corresponds to
    , doTokenContractId   :: C f Text -- ^ the token contract id that this operation corresponds to    
    , doBlockLevel        :: C f Int64 -- ^ the block level that this was found at
    , doBlockHash         :: C f Text -- ^ hash of the block
    , doTimestamp         :: C f LocalTime  -- ^ time of the block
    , doProtocol          :: C f Text -- ^ the protocol at which this was queried, this determines how to parse the json payload    
    , doOperationX        :: C f Int32 -- ^ the block has a structure of operations: [[[OperationContentAndResult]]], this is the x index
    , doOperationY        :: C f Int32 -- ^ the y index
    , doContentZ          :: C f Int32 -- ^ the z index
    , doSource            :: C f Text -- ^ the address that initiated this operation    
    , doEntrypoint        :: C f Text -- ^ the dexter entrypoint that was called
    , doLqtTotal          :: C f Text -- ^ storage total liquidity after this operation
    , doTokenPool         :: C f Text -- ^ storage token pool after this operation
    , doXtzPool           :: C f Text -- ^ storage xtz pool after this operation
    , doPayload           :: C f Aeson.Value -- ^ the OperationContentAndResult payload as json
    } deriving (Generic)

instance Beamable DexterOperationT
instance Table DexterOperationT where
   data PrimaryKey DexterOperationT f = DexterOperationId (Columnar f Int64) deriving (Generic, Beamable)
   primaryKey = DexterOperationId . doDexterOperationId
   
type DexterOperation = DexterOperationT Identity 
type DexterOperationId = PrimaryKey DexterOperationT Identity

deriving instance Show (PrimaryKey DexterOperationT Identity)
deriving instance Show DexterOperation
deriving instance Eq (PrimaryKey DexterOperationT Identity)
deriving instance Eq DexterOperation

instance FromRow DexterOperation
