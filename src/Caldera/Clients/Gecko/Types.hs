{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Caldera.Clients.Gecko.Types(
    TimeValuePair(..)
  , MarketChart  (..)
  , History      (..)
  , currencyToFunc
  , currentPrices
  ) where

import Data.Aeson   (ToJSON(toJSON), FromJSON(parseJSON), withArray, withObject, object, (.:), (.=))
import Data.Vector  ((!?))
import GHC.Generics (Generic)
import Data.Text as T ( Text )

data TimeValuePair =
  TimeValuePair
    { time :: Integer
    , value :: Float
    } deriving (Eq,Read,Show,Generic)

instance ToJSON TimeValuePair where
  toJSON (TimeValuePair time' value') = toJSON [toJSON time', toJSON value']

instance FromJSON TimeValuePair where
  parseJSON = withArray "TimeValuePair" $ \a ->
    case (,) <$> (a !? 0) <*> (a !? 1) of
      Just (time', value') -> TimeValuePair <$> parseJSON time' <*> parseJSON value'
      _ -> fail "Unexpected value"

data MarketChart =
  MarketChart
    { prices      :: [TimeValuePair]
    , marketCaps  :: [TimeValuePair]
    , totalVolumes :: [TimeValuePair]
    } deriving (Eq,Read,Show,Generic)
  

instance ToJSON MarketChart where
  toJSON (MarketChart prices' marketCaps' totalValues') =
    object
      [ "prices"       .= prices'
      , "market_caps"  .= marketCaps'
      , "total_volumes" .= totalValues'
      ]
    

instance FromJSON MarketChart where
  parseJSON = withObject "MarketChart" $ \o ->
    MarketChart <$> o .: "prices"
                <*> o .: "market_caps"
                <*> o .: "total_volumes"

data Localization = Localization
  { en :: T.Text
  , de :: T.Text
  , es :: T.Text
  , fr :: T.Text
  , it :: T.Text
  , pl :: T.Text
  , ro :: T.Text
  , hu :: T.Text
  , nl :: T.Text
  , pt :: T.Text
  , sv :: T.Text
  , vi :: T.Text
  , tr :: T.Text
  , ru :: T.Text
  , ja :: T.Text
  , zh :: T.Text
  , zhtw :: T.Text
  , ko :: T.Text
  , ar :: T.Text
  , th :: T.Text
  , id :: T.Text
  } deriving (Eq,Read,Show,Generic)

instance ToJSON Localization where
  toJSON (Localization en' de' es' fr' it' pl' ro' hu' nl' pt' sv' vi' tr' ru' ja' zh' zhtw' ko' ar' th' id') =
    object
      [ "en" .= en'
      , "de" .= de'
      , "es" .= es'
      , "fr" .= fr'
      , "it" .= it'
      , "pl" .= pl'
      , "ro" .= ro'
      , "hu" .= hu'
      , "nl" .= nl'
      , "pt" .= pt'
      , "sv" .= sv'
      , "vi" .= vi'
      , "tr" .= tr'
      , "ru" .= ru'
      , "ja" .= ja'
      , "zh" .= zh'
      , "zhtw" .= zhtw'
      , "ko" .= ko'
      , "ar" .= ar'
      , "th" .= th'
      , "id" .= id'
      ]

instance FromJSON Localization where
  parseJSON = withObject "Localization" $ \o ->
    Localization <$> o .: "en"
                <*> o .: "de"
                <*> o .: "es"
                <*> o .: "fr"
                <*> o .: "it"
                <*> o .: "pl"
                <*> o .: "ro"
                <*> o .: "hu"
                <*> o .: "nl"
                <*> o .: "pt"
                <*> o .: "sv"
                <*> o .: "vi"
                <*> o .: "tr"
                <*> o .: "ru"
                <*> o .: "ja"
                <*> o .: "zh"
                <*> o .: "zh-tw"
                <*> o .: "ko"
                <*> o .: "ar"
                <*> o .: "th"
                <*> o .: "id"

data Image = Image
  { thumb :: T.Text
  , small :: T.Text
  } deriving (Eq,Read,Show,Generic)

instance ToJSON Image 
instance FromJSON Image

data MarketPair = MarketPair
  { aed :: Double
  , ars :: Double
  , aud :: Double
  , bch :: Double
  , bdt :: Double
  , bhd :: Double
  , bmd :: Double
  , bnb :: Double
  , brl :: Double
  , btc :: Double
  , cad :: Double
  , chf :: Double
  , clp :: Double
  , cny :: Double
  , czk :: Double
  , dkk :: Double
  , dot :: Double
  , eos :: Double
  , eth :: Double
  , eur :: Double
  , gbp :: Double
  , hkd :: Double
  , huf :: Double
  , idr :: Double
  , ils :: Double
  , inr :: Double
  , jpy :: Double
  , krw :: Double
  , kwd :: Double
  , lkr :: Double
  , ltc :: Double
  , mmk :: Double
  , mxn :: Double
  , myr :: Double
  , ngn :: Double
  , nok :: Double
  , nzd :: Double
  , php :: Double
  , pkr :: Double
  , pln :: Double
  , rub :: Double
  , sar :: Double
  , sek :: Double
  , sgd :: Double
  , thb :: Double
  , try :: Double
  , twd :: Double
  , uah :: Double
  , usd :: Double
  , vef :: Double
  , vnd :: Double
  , xag :: Double
  , xau :: Double
  , xdr :: Double
  , xlm :: Double
  , xrp :: Double
  , yfi :: Double
  , zar :: Double
  , link :: Double
  } deriving (Eq,Read,Show,Generic)

currencyToFunc :: T.Text -> MarketPair -> Maybe Double
currencyToFunc currency 
  | currency == "aed" = Just . aed
  | currency == "ars" = Just . ars
  | currency == "aud" = Just . aud
  | currency == "bch" = Just . bch
  | currency == "bdt" = Just . bdt
  | currency == "bhd" = Just . bhd
  | currency == "bmd" = Just . bmd
  | currency == "bnb" = Just . bnb
  | currency == "brl" = Just . brl
  | currency == "btc" = Just . btc
  | currency == "cad" = Just . cad
  | currency == "chf" = Just . chf
  | currency == "clp" = Just . clp
  | currency == "cny" = Just . cny
  | currency == "czk" = Just . czk
  | currency == "dkk" = Just . dkk
  | currency == "dot" = Just . dot
  | currency == "eos" = Just . eos
  | currency == "eth" = Just . eth
  | currency == "eur" = Just . eur
  | currency == "gbp" = Just . gbp
  | currency == "hkd" = Just . hkd
  | currency == "huf" = Just . huf
  | currency == "idr" = Just . idr
  | currency == "ils" = Just . ils
  | currency == "inr" = Just . inr
  | currency == "jpy" = Just . jpy
  | currency == "krw" = Just . krw
  | currency == "kwd" = Just . kwd
  | currency == "lkr" = Just . lkr
  | currency == "ltc" = Just . ltc
  | currency == "mmk" = Just . mmk
  | currency == "mxn" = Just . mxn
  | currency == "myr" = Just . myr
  | currency == "ngn" = Just . ngn
  | currency == "nok" = Just . nok
  | currency == "nzd" = Just . nzd
  | currency == "php" = Just . php
  | currency == "pkr" = Just . pkr
  | currency == "pln" = Just . pln
  | currency == "rub" = Just . rub
  | currency == "sar" = Just . sar
  | currency == "sek" = Just . sek
  | currency == "sgd" = Just . sgd
  | currency == "thb" = Just . thb
  | currency == "try" = Just . try
  | currency == "twd" = Just . twd
  | currency == "uah" = Just . uah
  | currency == "usd" = Just . usd
  | currency == "vef" = Just . vef
  | currency == "vnd" = Just . vnd
  | currency == "xag" = Just . xag
  | currency == "xau" = Just . xau
  | currency == "xdr" = Just . xdr
  | currency == "xlm" = Just . xlm
  | currency == "xrp" = Just . xrp
  | currency == "yfi" = Just . yfi
  | currency == "zar" = Just . zar
  | currency == "link" = Just . link
  | otherwise = \_ -> Nothing

instance ToJSON MarketPair 
instance FromJSON MarketPair

data MarketData = MarketData
  { currentPrices :: MarketPair
  , marketCap     :: MarketPair
  , totalVolume   :: MarketPair
  } deriving (Eq,Read,Show,Generic)

instance ToJSON MarketData where
  toJSON m =
    object
      [ "current_price" .= currentPrices m
      , "market_cap"    .= marketCap m
      , "total_volume"  .= totalVolume m
      ]
instance FromJSON MarketData where
  parseJSON = withObject "MarketData" $ \o ->
    MarketData <$> o .: "current_price"
               <*> o .: "market_cap"
               <*> o .: "total_volume"
    

data CommunityData = CommunityData
  { facebookLikes :: Maybe Int
  , twitterFollowers :: Maybe Int
  , redditAveragePosts48h :: Maybe Double
  , redditAverageComments48h :: Maybe Double
  , redditSubscribers :: Maybe Int
  , redditAccountsActive48h :: T.Text
  } deriving (Eq,Read,Show,Generic)

instance ToJSON CommunityData where
  toJSON (CommunityData facebookLikes' twitterFollowers' redditAveragePosts48h' redditAverageComments48h' redditSubscribers' redditAccountsActive48h') =
    object
      [ "facebook_likes" .= facebookLikes'
      , "twitter_followers" .= twitterFollowers'
      , "reddit_average_posts_48h" .= redditAveragePosts48h'
      , "reddit_average_comments_48h" .= redditAverageComments48h'
      , "reddit_subscribers" .= redditSubscribers'
      , "reddit_accounts_active_48h" .= redditAccountsActive48h'
      ]


instance FromJSON CommunityData where
  parseJSON = withObject "community_data" $ \o ->
    CommunityData <$> o .: "facebook_likes"
                <*> o .: "twitter_followers"
                <*> o .: "reddit_average_posts_48h"
                <*> o .: "reddit_average_comments_48h"
                <*> o .: "reddit_subscribers"
                <*> o .: "reddit_accounts_active_48h"

data DeveloperData = DeveloperData
  { forks :: Maybe Int
  , stars :: Maybe Int
  , subscribers :: Maybe Int
  , totalIssues :: Maybe Int
  , closedIssues :: Maybe Int
  , pullRequestsMerged :: Maybe Int
  , pullRequestContributors :: Maybe Int
  , codeAdditionsDeletions4Weeks :: CodeAdditionsDeletions4Weeks
  , commitCount4Weeks :: Maybe Int
  } deriving (Eq, Show, Generic)

instance ToJSON DeveloperData where
  toJSON (DeveloperData forks' stars' subscribers' totalIssues' closedIssues' pullRequestsMerged' pullRequestContributors' codeAdditionsDeletions4Weeks' commitCount4Weeks') =
    object
      [ "forks" .= forks'
      , "stars" .= stars'
      , "subscribers" .= subscribers'
      , "total_issues" .= totalIssues'
      , "closed_issues" .= closedIssues'
      , "pull_requests_merged" .= pullRequestsMerged'
      , "pull_request_contributors" .= pullRequestContributors'
      , "code_additions_deletions_4_weeks" .= codeAdditionsDeletions4Weeks'
      , "commit_count_4_weeks" .= commitCount4Weeks'
      ]


instance FromJSON DeveloperData where
  parseJSON = withObject "developer_data" $ \o ->
    DeveloperData <$> o .: "forks"
                <*> o .: "stars"
                <*> o .: "subscribers"
                <*> o .: "total_issues"
                <*> o .: "closed_issues"
                <*> o .: "pull_requests_merged"
                <*> o .: "pull_request_contributors"
                <*> o .: "code_additions_deletions_4_weeks"
                <*> o .: "commit_count_4_weeks"

data CodeAdditionsDeletions4Weeks = CodeAdditionsDeletions4Weeks
  { additions :: Maybe Int
  , deletions :: Maybe Int
  } deriving (Eq, Show, Generic)

instance ToJSON CodeAdditionsDeletions4Weeks where
  toJSON (CodeAdditionsDeletions4Weeks additions' deletions') =
    object
      [ "additions" .= additions'
      , "deletions" .= deletions'
      ]


instance FromJSON CodeAdditionsDeletions4Weeks where
  parseJSON = withObject "code_additions_deletions_4_weeks" $ \o ->
    CodeAdditionsDeletions4Weeks <$> o .: "additions"
                                 <*> o .: "deletions"

data PublicInterestStats = PublicInterestStats
  { alexaRank :: Maybe Int
  , bingMatches :: Maybe Int
  } deriving (Eq, Show)

instance ToJSON PublicInterestStats where
  toJSON (PublicInterestStats alexaRank' bingMatches') =
    object
      [ "alexa_rank" .= alexaRank'
      , "bing_matches" .= bingMatches'
      ]


instance FromJSON PublicInterestStats where
  parseJSON = withObject "public_interest_stats" $ \o ->
    PublicInterestStats <$> o .: "alexa_rank"
                        <*> o .: "bing_matches"

data History = History 
  { historyId :: T.Text
  , symbol :: T.Text
  , name :: T.Text
  , localization :: Localization
  , image :: Image
  , marketData :: MarketData
  , communityData :: CommunityData
  , developerData :: DeveloperData
  , publicInterestStats :: PublicInterestStats
  } deriving (Eq, Show)

instance ToJSON History where
  toJSON (History historyId' symbol' name' localization' image' marketData' communityData' developerData' publicInterestStats') =
    object
      [ "id" .= historyId'
      , "symbol" .= symbol'
      , "name" .= name'
      , "localization" .= localization'
      , "image" .= image'
      , "market_data" .= marketData'
      , "community_data" .= communityData'
      , "developer_data" .= developerData'
      , "public_interest_stats" .= publicInterestStats'
      ]


instance FromJSON History where
  parseJSON = withObject "History" $ \o ->
    History <$> o .: "id"
                <*> o .: "symbol"
                <*> o .: "name"
                <*> o .: "localization"
                <*> o .: "image"
                <*> o .: "market_data"
                <*> o .: "community_data"
                <*> o .: "developer_data"
                <*> o .: "public_interest_stats"
