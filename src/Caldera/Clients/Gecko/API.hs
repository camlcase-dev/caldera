{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Caldera.Clients.Gecko.API(
    requestHistoricPrice
  , requestTezosPriceHistory
  , requestVsCurrencies
  ) where

-- Base
import Data.Proxy (Proxy(Proxy))
import Data.Text (Text)

-- Types
import Caldera.Clients.Gecko.Types (MarketChart, History)

-- Network
import Servant.API ( Capture, JSON, QueryParam, type (:>), Get )
import Servant.Client (ClientM, ClientError, client)
import Caldera.Clients.Generic ( runClient, ClientConfig ) 

-- | Query the value of the x passed days since now
-- https://api.coingecko.com/api/v3/coins/tezos/market_chart?vs_currency=usd&days=1
type GetMarketChart
  =  "api"
  :> "v3"
  :> "coins"
  :> Capture "id" Text
  :> "market_chart"
  :> QueryParam "vs_currency" Text -- required
  :> QueryParam "days" Text -- required
  :> Get '[JSON] MarketChart

getMarketChart :: Text -> Text -> Text -> ClientM MarketChart
getMarketChart id_ vsCurrency days = client (Proxy :: Proxy GetMarketChart) id_ (Just vsCurrency) (Just days)

-- | Query the value for a particular date
-- https://api.coingecko.com/api/v3/coins/tezos/history?date=25-11-2020
type GetHistory
  =  "api"
  :> "v3"
  :> "coins"
  :> Capture "id" Text
  :> "history"
  :> QueryParam "date" Text -- required
  :> Get '[JSON] History

getHistory :: Text -> Text -> ClientM History
getHistory id_ date_ = client (Proxy :: Proxy GetHistory) id_ (Just date_)

-- | Query for a list of supported vs currencies
-- https://api.coingecko.com/api/v3/simple/supported_vs_currencies
type GetSupportedVsCurrencies
  =  "api"
  :> "v3"
  :> "simple"
  :> "supported_vs_currencies"
  :> Get '[JSON] [Text]

getSupportedVsCurrencies :: ClientM [Text]
getSupportedVsCurrencies = client (Proxy :: Proxy GetSupportedVsCurrencies) 

requestVsCurrencies :: ClientConfig -> IO (Either ClientError [Text])
requestVsCurrencies config = do runClient config getSupportedVsCurrencies

requestTezosPriceHistory :: ClientConfig -> Text -> IO (Either ClientError MarketChart)
requestTezosPriceHistory config vsCurrency = do runClient config (getMarketChart "tezos" vsCurrency "max")

requestHistoricPrice :: ClientConfig -> Text -> IO (Either ClientError History)
requestHistoricPrice config date = do runClient config (getHistory "tezos"  date)
