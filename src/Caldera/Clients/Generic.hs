module Caldera.Clients.Generic(
    ClientConfig(..)
  , runClient
  ) where

import Network.HTTP.Client (Manager)
import Servant.Client (BaseUrl(BaseUrl), Scheme, ClientM, ClientError, mkClientEnv, runClientM)


data ClientConfig = ClientConfig 
    { clientManager :: Manager
    , clientScheme :: Scheme
    , clientHost :: String
    , clientPort :: Int
    }

runClient :: ClientConfig -> ClientM a -> IO (Either ClientError a)
runClient config cl = do
  let clientEnv = mkClientEnv (clientManager config) (BaseUrl (clientScheme config) (clientHost config) (clientPort config) "")
  runClientM cl clientEnv  