{-# LANGUAGE DuplicateRecordFields #-}

module Caldera.Clients.Tezos.API(
    requestBlock
  ) where

-- Base
import Data.Proxy (Proxy(Proxy))

-- Network
import Servant.Client (ClientM, ClientError, client)
import Caldera.Clients.Generic ( runClient, ClientConfig )

-- Tezos Servant Client
import Tezos.Client.Types(ChainId(ChainIdMain), BlockId, Block)
import Tezos.Client.Protocol.Alpha (GetBlock)

getBlock :: ChainId -> BlockId -> ClientM Block
getBlock = client (Proxy :: Proxy GetBlock)

requestBlock :: ClientConfig -> BlockId -> IO (Either ClientError Block)
requestBlock config blockID = runClient config (getBlock ChainIdMain blockID)
