{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DeriveGeneric #-}

module Caldera.Config(
    ServerConfig         (..)
  , PostgresConfig       (..)
  , CalderaAPIConfig     (..)
  , CacheConfig          (..)
  , ContractConfig       (..)
  , SynchronizationConfig(..)
  , WorkerConfig         (..)
  , TezosConfig          (..)
  , CalderaSyncConfig    (..)
  , getCalderaAPIConfig
  , getCalderaSyncConfig
  ) where

import           Data.Aeson (FromJSON(parseJSON), (.:), withObject)
import Options.Applicative
    ( (<**>),
      fullDesc,
      header,
      help,
      info,
      long,
      progDesc,
      short,
      strOption,
      value,
      execParser,
      helper,
      Parser,
      ParserInfo )

-- text and encoding 
import qualified Data.Text             as T
import qualified Data.Yaml             as Yaml
import Data.Int (Int64)


import System.Environment ()
import   qualified        System.Envy as Envy

newtype ServerConfig
    = ServerConfig {serverPort :: T.Text}
    deriving (Eq, Show)

instance FromJSON ServerConfig where
  parseJSON = withObject "ServerConfig" $ \o ->
    ServerConfig <$> o .: "port"

instance Envy.FromEnv ServerConfig where
  fromEnv _ =
    ServerConfig <$> Envy.envMaybe "CALDERA_PORT" Envy..!= "3000"

data PostgresConfig
  = PostgresConfig
  { psqlHost     :: T.Text
  , psqlUser     :: T.Text
  , psqlPassword :: T.Text
  , psqlDb       :: T.Text
  } deriving (Eq, Show)

instance FromJSON PostgresConfig where
  parseJSON = withObject "PostgresConfig" $ \o ->
    PostgresConfig <$> o .: "host"
                   <*> o .: "user"
                   <*> o .: "password"
                   <*> o .: "database"

instance Envy.FromEnv PostgresConfig where
  fromEnv _ =
    PostgresConfig <$> Envy.env "CALDERA_PG_HOST"
        <*> Envy.env "CALDERA_PG_USER"
        <*> Envy.env "CALDERA_PG_PASS"
        <*> Envy.env "CALDERA_PG_DB"

data TezosConfig
  = TezosConfig
  { tezosNodeHost   :: String
  , tezosNodePort :: Int
  } deriving (Eq, Show)

instance FromJSON TezosConfig where
  parseJSON = withObject "TezosConfig" $ \o ->
    TezosConfig <$> o .: "host"
                <*> o .: "port"

instance Envy.FromEnv TezosConfig where
  fromEnv _ =
    TezosConfig <$> Envy.env "CALDERA_TEZOS_HOST"
        <*> Envy.env "CALDERA_TEZOS_PORT"

data WorkerConfig
  = WorkerConfig
  { blockWorkerInterval :: Int
  , dailyPricesInterval :: Int
  }

instance FromJSON WorkerConfig where
  parseJSON = withObject "WorkerConfig" $ \o ->
    WorkerConfig <$> o .: "block-worker-interval"
                <*> o .: "daily-prices-interval"

instance Envy.FromEnv WorkerConfig where
  fromEnv _ =
    WorkerConfig <$> Envy.envMaybe "CALDERA_WORKER_BLOCK_INTERVAL" Envy..!= 60000000
        <*> Envy.envMaybe "CALDERA_WORKER_PRICES_INTERVAL" Envy..!= (60 * 1000000)

newtype CacheConfig = CacheConfig {cacheWorkerInterval :: Int} deriving (Eq, Show)

instance Envy.FromEnv CacheConfig where
  fromEnv _ =
    CacheConfig <$> Envy.envMaybe "CALDERA_CACHE_WORKER_INTERVAL" Envy..!= 60000000

data CalderaAPIConfig
  = CalderaAPIConfig
  { ccServerConfig   :: ServerConfig
  , ccPostgresConfig :: PostgresConfig
  , ccCacheConfig :: CacheConfig
  } deriving (Eq, Show)

getCalderaAPIConfig :: IO (Either String CalderaAPIConfig)
getCalderaAPIConfig = do
  _apiOptions <- execParser apiOpts
  -- FIXME: use apiOptions
  eitherServerConfig <- Envy.decodeEnv :: IO (Either String ServerConfig) 
  case eitherServerConfig of
    Left pe      -> return $ Left $ "Failed to get Port From Enviroment Variables: " ++ show pe
    Right serverConfig -> do
      eitherPostgresConfig <- Envy.decodeEnv :: IO (Either String PostgresConfig)
      case eitherPostgresConfig of
        Left pe      -> return $ Left $ "Failed to get Postgres Config From Enviroment Variables: " ++ show pe
        Right postgresConfig -> do
          eitherCacheConfig <- Envy.decodeEnv :: IO (Either String CacheConfig)
          case eitherCacheConfig of
            Left pe -> return $ Left $ "Failed to get Cache Config From Enviroment Variables: " ++ show pe
            Right cacheConfig -> do
              return $ Right CalderaAPIConfig
                      { ccServerConfig = serverConfig
                      , ccPostgresConfig = postgresConfig
                      , ccCacheConfig = cacheConfig
                      }

data CalderaSyncConfig
  = CalderaSyncConfig
  { csPostgresConfig :: PostgresConfig
  , csTezosConfig :: TezosConfig
  , csWorkerIntervals :: WorkerConfig
  , csSync :: SynchronizationConfig
  } 

instance FromJSON CalderaSyncConfig where
  parseJSON = withObject "CalderaSyncConfig" $ \o ->
    CalderaSyncConfig <$> o .: "postgresql"
                      <*> o .: "tezos"
                      <*> o .: "worker-intervals"
                      <*> o .: "synchronization"

data ContractConfig 
  = ContractConfig
  { dexterTokenName :: T.Text
  , dexterTokenSymbol :: T.Text
  , dexterContract :: T.Text
  , decimals :: Integer
  , tokenContract :: T.Text
  , dexterContractOrigination :: Int64
  , dexterBigMapId :: Integer
  }

instance FromJSON ContractConfig where
  parseJSON = withObject "ContractConfig" $ \o ->
    ContractConfig <$> o .: "token-name"
                   <*> o .: "token-symbol"
                   <*> o .: "contract"
                   <*> o .: "decimals"
                   <*> o .: "token-contract"
                   <*> o .: "origination"
                   <*> o .: "big-map-id"                      

newtype SynchronizationConfig
    = SynchronizationConfig {contractConfigs :: [ContractConfig]}

instance FromJSON SynchronizationConfig where
  parseJSON = withObject "SynchronizationConfig" $ \o ->
    SynchronizationConfig <$> o .: "dexter-contracts"

getCalderaSyncConfig :: IO (Either String CalderaSyncConfig)
getCalderaSyncConfig = do
  eitherTezosConfig <- Envy.decodeEnv :: IO (Either String TezosConfig) 
  case eitherTezosConfig of
    Left pe -> return $ Left $ "Failed to get Port From Enviroment Variables: " ++ show pe
    Right tezosConfig -> do
      eitherPostgresConfig <- Envy.decodeEnv :: IO (Either String PostgresConfig)
      case eitherPostgresConfig of
        Left pe -> return $ Left $ "Failed to get Postgres Config From Enviroment Variables: " ++ show pe
        Right postgresConfig -> do
          eitherWorkerIntervalConfig <- Envy.decodeEnv :: IO (Either String WorkerConfig)
          case eitherWorkerIntervalConfig of
            Left pe -> return $ Left $ "Failed to get Worker Interval Config From Enviroment Variables: " ++ show pe
            Right workerIntervalConfig -> do
              calderaOptions <- execParser syncOpts
              eConfig <- Yaml.decodeFileEither (T.unpack $ syncConfigFilePath calderaOptions) :: IO (Either Yaml.ParseException SynchronizationConfig)
              case eConfig of
                Left pe -> return $ Left $ "Failed to get sync config file: " ++ show pe
                Right file ->
                  return $ Right CalderaSyncConfig
                          { csPostgresConfig = postgresConfig
                          , csTezosConfig = tezosConfig
                          , csWorkerIntervals = workerIntervalConfig
                          , csSync = file
                      }

newtype CalderaAPIOption
    = CalderaAPIOption {apiConfigFilePath :: T.Text}
    deriving (Eq, Show)

calderaAPIOpts :: Parser CalderaAPIOption
calderaAPIOpts =
  CalderaAPIOption
  <$> strOption
  ( long "config"
    <> short 'c'
    <> help "Path to config yaml file"
    <> value "./config.yml")

apiOpts :: ParserInfo CalderaAPIOption
apiOpts =
   info (calderaAPIOpts <**> helper)
   (   fullDesc <> progDesc "The Caldera API for dexter related statistics and metrics."
       <> header "caldera-api: An API for querying dexter related information.")

newtype CalderaSyncOption
    = CalderaSyncOption {syncConfigFilePath :: T.Text}
    deriving (Eq, Show)

calderaSyncOpts :: Parser CalderaSyncOption
calderaSyncOpts =
  CalderaSyncOption
  <$> strOption
  ( long "config"
    <> short 'c'
    <> help "Path to sync config yaml file"
    <> value "./sync-config.yml")

syncOpts :: ParserInfo CalderaSyncOption
syncOpts =
   info (calderaSyncOpts <**> helper)
   (   fullDesc <> progDesc "The Caldera Syncronization process that indexes dexter related information."
       <> header "caldera-sync: An Indexer for Dexter metrics.")
