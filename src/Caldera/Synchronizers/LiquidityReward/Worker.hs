{-|
Module      : Caldera.Synchronizers.LiquidityReward.Worker
Description : Worker that inserts LiquidityPosition and LiquidityReward entries
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Synchronizers.LiquidityReward.Worker(
    start,
    getLiquidityPosition -- expose for testing
  ) where

-- basic
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Types as Aeson
import qualified Data.Set as Set
import qualified Data.Text as T
import Control.Concurrent (threadDelay)
import Control.Monad (forM_)
import Data.Bool   (bool)
import Data.Either (rights)
import Data.Text   (Text)
import GHC.Natural (Natural)
import Safe (maximumByMay)

-- database
import Database.PostgreSQL.Simple (Connection)

-- tezos
import qualified Tezos.Client.Types.Block     as Block
import qualified Tezos.Client.Types.Contract  as BigMap
import qualified Tezos.Client.Types.Micheline as Micheline
import Tezos.Client.Types.Core (Bignum, ContractId, unBignum, unContractId)

-- caldera
import qualified Caldera.Database.Query.DexterContract as DC
import qualified Caldera.Database.Query.DexterOperation as DO
import qualified Caldera.Database.Query.LiquidityPosition as LP
import qualified Caldera.Database.Query.LiquidityPositionAndOperation as LPO
import qualified Caldera.Database.Query.LiquidityReward as LR
import Caldera.Database.Query.DexterOperation (DexterTransactionError, parseDexterTransaction, safeParseBytes)
import Caldera.Database.Table.DexterContract
import Caldera.Database.Table.DexterOperation
import Caldera.Database.Table.LiquidityPositionAndOperation
import Caldera.Database (catchSqlErrors)
import Caldera.Database.Table.LiquidityPosition
import Caldera.Database.Table.LiquidityReward
import Dexter.Exchange.Transaction.Parameters (toText)
import Dexter.Exchange.LiquidityReward (getSingleTransactionLiquidityRewards)
import Dexter.Exchange.Transaction (DexterTransaction(..))

-- logging
import qualified Control.Concurrent.STM.TChan as Tchan
import qualified Logging.Logger as L

-- =============================================================================
-- Process changes in liquidity ownership from DexterOperation
-- =============================================================================

data ParseLiquidityBigMapError
  = UnexpectedKeyFormat Micheline.Expression
  | UnexpectedValueFormat (Maybe Micheline.Expression)
  | NotAnUpdate
  deriving (Eq, Show)

-- | The big map diff from a block tells us how much the liquidity has changed
getLiquidityFromBigMapDiff :: BigMap.BigMapDiff -> Either ParseLiquidityBigMapError (ContractId, Bignum)
getLiquidityFromBigMapDiff bm =
  case bm of
    BigMap.BigMapDiffUpdate _bigMapId _ key_ value_ -> do
      key <- maybe (Left $ UnexpectedKeyFormat key_) Right $ safeParseBytes key_
      case value_ of
        Just
          (Micheline.Expression
            (Micheline.PrimitiveData Micheline.PDPair)
            (Just
             [ Micheline.IntExpression lqt'
             , _allowances
             ]
            )
            _
          ) -> Right (key, lqt')
        _ -> Left $ UnexpectedValueFormat value_
    _ -> Left NotAnUpdate

data LiquidityPositionError
  = PayloadParserError Text
  | TransactionUnapplied
  | NoBigMapDiff
  deriving (Eq, Show)

-- | get big map diffs from a transaction operation
getBigMapDiffs :: Block.Transaction -> [BigMap.BigMapDiff]
getBigMapDiffs =
  \case
    Block.TransactionApplied _ bigMapDiff _ _ _ _ _ _ _ ->
      case bigMapDiff of
        Just bs -> bs
        _ -> []
    _ -> []

-- | process an operation for new liquidity positions
getLiquidityPosition :: DexterOperation -> Either LiquidityPositionError [LiquidityPosition]
getLiquidityPosition dexterOperation = do
  -- get the transaction and metadata
  (_transaction, metadata) <-
    either
    (Left . PayloadParserError . T.pack)
    Right
    (Aeson.parseEither Aeson.parseJSON $ doPayload dexterOperation
     :: Either String (Block.OperationTransaction, (Block.InternalOperationResultsMetadata Block.Transaction)))
  
  let bigMapDiffs      = getBigMapDiffs $ Block.operationResult metadata
  let liquidityUpdates = rights $ fmap getLiquidityFromBigMapDiff bigMapDiffs

  Right $
    fmap
    (\(contractId, lqt) ->
       LiquidityPosition
         { lpLiquidityPositionId = 0 -- this will be replaced by the database
         , lpAccountId           = toText . unContractId $ contractId
         , lpLqt                 = T.pack . show . unBignum $ lqt
         , lpDexterOperationId   = doDexterOperationId dexterOperation         
         }
       )
    liquidityUpdates

data LiquidityRewardError
  = OperationOccursBeforePosition
  | FailedToParseDexterTransaction DexterTransactionError
  | NotAnExchangeTransaction
  | NoLiquidityPosition DexterOperation
  deriving (Eq, Show)

isExchangeTransaction :: DexterTransaction -> Bool
isExchangeTransaction =
  \case
    XtzToToken _ -> True
    TokenToXtz _ -> True
    TokenToToken _ -> True
    AddLiquidity    _ -> False
    RemoveLiquidity _ -> False
    

-- | Given a dexter operation and the latest liquidity position on the same
-- contract, calculate the reward that belongs to this position.
getLiquidityReward :: DexterOperation -> LiquidityPositionAndOperation -> Either LiquidityRewardError LiquidityReward
getLiquidityReward dexterOperation liquidityPosition = do
  dexterTransaction <-
    either
    (Left . FailedToParseDexterTransaction)
    Right
    $ parseDexterTransaction dexterOperation

  _ <-
    bool
    (Left NotAnExchangeTransaction)
    (Right ())
    $ isExchangeTransaction dexterTransaction


  let (xtzReward, tokenReward) = getSingleTransactionLiquidityRewards dexterTransaction
      percentageOfLqt     =
        (read $ T.unpack $ lpoLqt liquidityPosition :: Double) /
        (read $ T.unpack $ doLqtTotal dexterOperation :: Double)
      personalXtzReward   = floor $ (fromIntegral xtzReward   :: Double) * percentageOfLqt :: Natural
      personalTokenReward = floor $ (fromIntegral tokenReward :: Double) * percentageOfLqt :: Natural

  Right $
    LiquidityReward
    { lrLiquidityRewardId = 0 -- this will be replaced by the database
    , lrAccountId         = lpoAccountId liquidityPosition    
    , lrXtzReward         = T.pack . show $ personalXtzReward
    , lrTokenReward       = T.pack . show $ personalTokenReward
    , lrDexterOperationId = doDexterOperationId dexterOperation
    }

getLiquidityRewardForAccountAtOperation
  :: [LiquidityPositionAndOperation]
  -> DexterOperation
  -> Either LiquidityRewardError LiquidityReward
getLiquidityRewardForAccountAtOperation lpos dexterOperation =
  case getClosestPositionForOperation lpos dexterOperation of
    Nothing -> Left $ NoLiquidityPosition dexterOperation
    Just liquidityPosition -> getLiquidityReward dexterOperation liquidityPosition

getClosestPositionForOperation :: [LiquidityPositionAndOperation] -> DexterOperation -> Maybe LiquidityPositionAndOperation
getClosestPositionForOperation lps dop =
  -- get liquidity positions that occur strictly before an operation (do not
  -- include those that occur at the same time)
  let positionsBeforeOperation =
        filter
        (\lp ->
           [lpoBlockLevel lp, fromIntegral $ lpoOperationX lp, fromIntegral $ lpoOperationY lp, fromIntegral $ lpoContentZ lp] <
           [doBlockLevel dop, fromIntegral $ doOperationX dop, fromIntegral $ doOperationY dop, fromIntegral $ doContentZ dop]
        )
        lps
   in maximumByMay
      (\x y ->
         if [lpoBlockLevel x, fromIntegral $ lpoOperationX x, fromIntegral $ lpoOperationY x, fromIntegral $ lpoContentZ x] >
         [lpoBlockLevel y, fromIntegral $ lpoOperationX y, fromIntegral $ lpoOperationY y, fromIntegral $ lpoContentZ y]
         then GT
         else LT
         )
      positionsBeforeOperation

-- | Only rerun this when the number of dexter operations changes.
run :: Tchan.TChan L.LogMsg -> Connection -> Int -> IO Int
run logchan connection previousOperationCount = do
  -- count the number of Dexter Operations
  eNewOperationCount <- catchSqlErrors (DO.count connection)

  case eNewOperationCount of
    Left err -> do
      logError logchan (T.pack err) []
      pure previousOperationCount

    Right Nothing -> do
      logError logchan (T.pack "DexterOperation.count query failed unexpectedly") []
      pure previousOperationCount

    Right (Just newOperationCount) -> do
      -- only run this logic if the count has changed
      if newOperationCount > previousOperationCount
        then do
        logInfo
          logchan
          "newOperationCount is greater than previousOperationCount"
          [("newOperationCount", T.pack . show $ newOperationCount) ,
           ("previousOperationCount", T.pack . show $ previousOperationCount)
          ]
        
        -- get the liquidity positions
        eDexterLiquidityOperations <-
          catchSqlErrors $ DO.selectAllLiquidityOperations connection

        -- addLiquidity and removeLiquidity
        -- build a history of how much liquidity a tz1 account owns at a particular
        -- block, store the result in liquidity position, which is valid until there
        -- is a change

        -- create a reward for each liquidity position when there is an xtzToToken, tokenToXtz
        -- or tokenToToken.

        -- individual wants cumulative reward information, it queries all liquidity rewards for
        -- a tz account and sums it up

        -- better for query time, but then denormalizes and requires worker
        -- to the total liquidity, but that is probably a commonly queried value.

        -- we also total liquidity rewards for all accounts.

        

        case eDexterLiquidityOperations of
          Left err -> do
            logError
              logchan
              (T.pack "DexterOperation.selectAllLiquidityOperations query failed unexpectedly")
              [("err", T.pack . show $ err)]
            pure previousOperationCount

          Right dexterLiquidityOperations -> do
            logInfo logchan "retrieved Dexter Liquidity Operations" []
        
            let liquidityPositions = concat $ rights $ fmap getLiquidityPosition dexterLiquidityOperations
            lpResults <- mapM (catchSqlErrors . LP.upsert connection) liquidityPositions

            logEitherErrors logchan "LiquidityPosition.upsert failed " lpResults

            -- get all dexter contract ids
            eDexterContracts <- catchSqlErrors (DC.selectAll connection)

            case eDexterContracts of
              Left err -> do
                logError
                  logchan
                  (T.pack "DexterContract.selectAll query failed unexpectedly")
                  [("err", T.pack . show $ err)]
                pure previousOperationCount

              Right dexterContracts -> do
                logInfo logchan "retrieved Dexter contracts" []
                
                let dexterContractIds = fmap dcDexterContractId dexterContracts

                forM_ dexterContractIds $ \dexterContractId -> do
                  -- get all the trade operations for a dexter account
                  eDexterTradeOperations <- catchSqlErrors $ DO.selectAllTradesByContractId connection dexterContractId

                  case eDexterTradeOperations of
                    Left err -> do
                      logError
                        logchan
                        (T.pack "DexterOperation.selectAllLiquidityOperations query failed unexpectedly")
                        [("err", T.pack . show $ err)]
                    Right dexterTradeOperations -> do
    
                      -- get a unique list of liquidity provider addresses for a particular dexterContract
                      let liquidityProviders =
                            Set.fromList
                            $ fmap doSource
                            $ filter (\dlo -> dexterContractId == doDexterContractId dlo) dexterLiquidityOperations

                      forM_ liquidityProviders $ \liquidityProvider -> do
                        -- get the liquidit positions for an individual liquidity provider
                        liquidityPositions' <- LPO.selectForAccount connection liquidityProvider

                        -- get a liquidity providers liquidity rewards for its previous positions and dexter trade operations
                        let liquidityRewards = getLiquidityRewardForAccountAtOperation liquidityPositions' <$> dexterTradeOperations

                        -- insert rewards in the database
                        lrResults <- mapM (catchSqlErrors . LR.upsert connection) $ rights liquidityRewards

                        logEitherErrors logchan "LiquidityReward.upsert failed " lrResults

                pure newOperationCount  

      else pure newOperationCount

start :: Tchan.TChan L.LogMsg -> Connection -> Int -> IO ()
start logchan connection interval = do
  logInfo logchan "starting up" []
  loop logchan connection 0 interval

loop :: Tchan.TChan L.LogMsg -> Connection -> Int -> Int -> IO ()
loop logchan connection previousOperationCount interval = do
  newOperationCount <- run logchan connection previousOperationCount
  threadDelay interval
  loop logchan connection newOperationCount interval

logInfo :: Tchan.TChan L.LogMsg -> Text -> [(Text,Text)] -> IO ()
logInfo = L.logInfo "LiquidityReward.Worker"

logError :: Tchan.TChan L.LogMsg -> Text -> [(Text,Text)] -> IO ()
logError = L.logError "LiquidityReward.Worker"

logEitherErrors :: (Show a) => Tchan.TChan L.LogMsg -> Text -> [Either a b] -> IO ()
logEitherErrors = L.logEitherErrors "LiquidityReward.Worker"
