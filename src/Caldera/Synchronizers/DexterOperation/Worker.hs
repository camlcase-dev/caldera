{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}

module Caldera.Synchronizers.DexterOperation.Worker(
    ReadOperationError    (..)
  , OperationsWorkerConfig(..)
  , run
  , toText
  , getDexterOperationForBlock
  ) where

-- base
import qualified Control.Concurrent.STM.TChan as Tchan
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Logging.Logger as L
import Control.Monad (void)
import Control.Monad.STM (atomically)
import Control.Concurrent (forkIO, threadDelay)
import Data.Int (Int32, Int64)
import Data.Text (Text)
import Data.Time (utc, utcToLocalTime, getCurrentTime)

-- serialization
import qualified Data.Aeson as Aeson

-- caldera
import qualified Caldera.Database.Query.DexterOperation as DO
import qualified Caldera.Database.Query.OperationBlock  as OB
import Caldera.Clients.Generic (ClientConfig)
import Caldera.Database (catchSqlErrors)
import Caldera.Database.Table.DexterOperation
import Caldera.Database.Table.OperationBlock
import Caldera.Synchronizers.Block.Parsing (parseLevel)
import Dexter.Contract.Storage
import Dexter.Exchange
import Caldera.Clients.Tezos.API ( requestBlock )

-- tezos
import qualified Tezos.Client.Types.Micheline as Micheline
import qualified Tezos.Client.Types.Block as Block
import Tezos.Client.Types.Core

-- database
import Database.PostgreSQL.Simple (Connection)


-- =============================================================================
-- parse data from the block
-- =============================================================================

-- | Get the Text of a UnistringText value
toText :: Unistring -> Text
toText =
  \case
    UnistringText t -> t
    _ -> ""

data ReadOperationError
  = NotADexterContract -- ^ ignore this
  | NotAnAppliedTransaction -- ^ ignore this
  | NoParameters OperationIdentifier -- ^ a dexter contract without parameters  
  | UnexpectedEntrypoint OperationIdentifier -- ^ a dexter contract with an unexpected entrypoint
  | NoOperationResult OperationIdentifier -- ^ probably mempool but should keep an eye on this
  | NoStorage OperationIdentifier
  | UnableToParseStorage OperationIdentifier
  deriving (Eq,Show)

data OperationIdentifier
  = OperationIdentifier
    { oiBlockLevel :: Int64
    , oiIndexX :: Int32
    , oiIndexY :: Int32
    , oiIndexZ :: Int32
    } deriving (Eq, Show)

-- | For a single operation, extract a DexterOperation table entry if possible
getDexterOperation
  :: [DexterExchange]
  -> Int64
  -> Text
  -> Text
  -> Timestamp
  -> Int32
  -> Int32
  -> Int32
  -> Block.OperationContentsAndResult
  -> Either ReadOperationError DexterOperation
getDexterOperation dexterExchanges blockLevel blockHash protocol timestamp indexX indexY indexZ op = do  
  -- get the operation transaction and metadata
  (transaction, metadata) <-
    maybe (Left $ NoOperationResult $ OperationIdentifier blockLevel indexX indexY indexZ) Right
    (case op of
       Block.OperationContentsAndResultTransaction transaction metadata -> Just (transaction, metadata)
       _ -> Nothing
    )

  -- check if it is an operation destined for a Dexter contract
  let destination = Block.destination (transaction :: Block.OperationTransaction)
  let source = Block.source (transaction :: Block.OperationTransaction)
  
  dexterExchange <- 
    maybe (Left NotADexterContract) Right (getDexterExchangeByContractId destination dexterExchanges)

  -- dexter contract should have parameters unless it is default, but we ignore default anyway
  parameters <-
    maybe
    (Left . NoParameters $ OperationIdentifier blockLevel indexX indexY indexZ)
    Right
    (Block.parameters (transaction :: Block.OperationTransaction))

  -- parse the dexter entry point
  entrypoint <-
    case Micheline.entrypoint parameters of
      EUnistring (UnistringText e) -> Right e
      EDefault -> Right "default"
      _ -> Left $ UnexpectedEntrypoint $ OperationIdentifier blockLevel indexX indexY indexZ

  -- get the storage
  dexterStorage <-
    case Block.operationResult metadata of
      Block.TransactionApplied mStorage _ _ _ _ _ _ _ _ ->
        case mStorage of
          Just storage ->
            maybe
            (Left $ UnableToParseStorage $ OperationIdentifier blockLevel indexX indexY indexZ)
            Right
            (expressionToStorage storage)
          Nothing -> Left (NoStorage $ OperationIdentifier blockLevel indexX indexY indexZ)
      _ -> Left NotAnAppliedTransaction
    
  
  Right $
    DexterOperation
      { doDexterOperationId = 0 -- insert will use autoincrement
      , doDexterContractId = toText . unContractId $ dexterContract dexterExchange
      , doTokenContractId  = toText . unContractId $ tokenContract dexterExchange
      , doBlockLevel = blockLevel
      , doBlockHash  = blockHash
      , doTimestamp  = utcToLocalTime utc timestamp
      , doProtocol   = protocol      
      , doOperationX = indexX
      , doOperationY = indexY
      , doContentZ   = indexZ
      , doSource     = toText . unContractId $ source
      , doEntrypoint = entrypoint
      , doLqtTotal   = T.pack . show . lqtTotal  $ dexterStorage
      , doXtzPool    = T.pack . show . unMutez . xtzPool $ dexterStorage
      , doTokenPool  = T.pack . show . tokenPool $ dexterStorage
      , doPayload    = Aeson.toJSON (transaction, metadata)
      }

getDexterOperationForBlock :: [DexterExchange] -> Block.Block -> [Either ReadOperationError DexterOperation]
getDexterOperationForBlock dexterExchanges block =
  concat . concat $ fmap (\(ops,indexX) -> getForOperations ops indexX) $ zip (Block.operations block) [0..]

  where
    header     = Block.header (block :: Block.Block)
    blockHash  = toText . unBlockHash $ Block.hash   (block :: Block.Block)
    blockLevel = Block.level  (header :: Block.RawBlockHeader)
    timestamp  = Block.timestamp  (header :: Block.RawBlockHeader)
    protocol   = toText . unProtocolHash $ Block.protocol (block :: Block.Block)
    
    getForOperations ops indexX =
      fmap (\(op,indexY) -> getForContentsAndResult indexX indexY op) $ zip ops [0..]

    getForContentsAndResult indexX indexY op =
      case op of
        Block.OperationAndResult _protocol _chainId _hash _branch contentsAndResult _signature ->
          fmap
          (\(car, indexZ) ->
              getDexterOperation dexterExchanges blockLevel blockHash protocol timestamp indexX indexY indexZ car)
          $ zip contentsAndResult [0..]
        _ -> []

-- =============================================================================
-- insert data into the DexterOperation table
-- =============================================================================

-- | Inserts a missing block into the operation_blocks table
insertMissing :: OperationsWorkerConfig -> Int64  -> IO ()
insertMissing config blkLevel = do
  dummy <- getCurrentTime
  let table = OperationBlock
              { obBlockLevel = blkLevel
              , obMissing = True
              , obTimestamp = utcToLocalTime utc dummy
              } 
  eitherInsert <- catchSqlErrors $ OB.insert (db config) table
  case eitherInsert of
    Left err -> do
      let logMap = Just $ M.fromList [("error", T.pack $ show err)]
      atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.ERROR, L.vals = logMap, L.msg = "Operations worker failed to insert into operation blocks table"}
    Right _ -> pure ()

-- | Inserts a found block into the operation_blocks table, if there is a conflict
-- | it will try to update the existing row.
insertProcessed :: OperationsWorkerConfig -> Int64 -> Timestamp -> IO ()
insertProcessed config blkLevel ts = do
  let table = OperationBlock
              { obBlockLevel = blkLevel
              , obMissing = False
              , obTimestamp = utcToLocalTime utc ts
              } 
  eitherInsert <- catchSqlErrors $ OB.insert (db config) table
  case eitherInsert of
    Right _ -> pure ()
    Left _ -> do
      upd <- catchSqlErrors $ OB.update (db config) table
      case upd of
        Left err -> do
          let logMap = Just $ M.fromList [("error", T.pack $ show err)]
          atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.ERROR, L.vals = logMap, L.msg = "Operations worker failed to insert/update into operation blocks table"}
        Right _ -> return ()

-- | Log a ReadOperationError
logReadOperationError :: Tchan.TChan L.LogMsg -> ReadOperationError -> IO ()
logReadOperationError logchan transactionError =
  case transactionError of
    NotADexterContract      -> pure ()
    NotAnAppliedTransaction -> pure ()
    NoParameters oi -> do
      let logMap = Just $ M.fromList [("error", T.pack $ show oi)]
      atomically $ Tchan.writeTChan logchan
        L.LogMsg { L.level = L.ERROR, L.vals = logMap , L.msg = "No parameters were found for this operation"}

    UnexpectedEntrypoint oi -> do
      let logMap = Just $ M.fromList [("error", T.pack $ show oi)]
      atomically $ Tchan.writeTChan logchan
        L.LogMsg { L.level = L.ERROR, L.vals = logMap , L.msg = "Encountered an unexpected Dexter entrypoint"}

    NoOperationResult _ -> pure ()

    NoStorage oi -> do
      let logMap = Just $ M.fromList [("error", T.pack $ show oi)]
      atomically $ Tchan.writeTChan logchan
        L.LogMsg { L.level = L.ERROR, L.vals = logMap , L.msg = "Dexter operation does not have storage"}

    UnableToParseStorage oi -> do
      let logMap = Just $ M.fromList [("error", T.pack $ show oi)]
      atomically $ Tchan.writeTChan logchan
        L.LogMsg { L.level = L.ERROR, L.vals = logMap , L.msg = "Unable to parse dexter storage from operation"}
  
-- | insert an operation or provide useful logging
processDexterOperation
  :: Tchan.TChan L.LogMsg
  -> Connection
  -> Block.Block
  -> Either ReadOperationError DexterOperation
  -> IO ()
processDexterOperation logchan conn block transactionResult = do
  let logMap = Just $ M.fromList [("block-level", T.pack $ show (parseLevel block))]
  case transactionResult of
    Left transactionError -> logReadOperationError logchan transactionError
    Right dexterOperation -> do
      eitherInsert <- catchSqlErrors $ DO.insert conn dexterOperation
      case eitherInsert of
        Left err -> do
          let logMap' = Just $ M.fromList [("block-level", T.pack $ show (parseLevel block)), ("error", T.pack $ show err)]
          atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.ERROR, L.vals = logMap', L.msg = "Operations worker failed to insert into the DexterOperation table"}
        Right _ -> atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.DEBUG , L.vals = logMap, L.msg = "Operations worker successfully inserted into the DexterOperation table"}

-- | Requests a block, parses dexter contracts, and inserts the result into the respective table
processOperation :: OperationsWorkerConfig -> Int64 -> IO ()
processOperation config blkLevel = do
  let logMap = Just $ M.fromList [("block-level", T.pack $ show blkLevel)]
  atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.INFO, L.vals = logMap , L.msg = "Operations worker processing block"}
  blk <- processBlock (logChan config) (client config) blkLevel
  case blk of
    Nothing -> do
      _ <- insertMissing config blkLevel
      pure ()
    Just b ->  do
      let operations = getDexterOperationForBlock (dexters config) b
      mapM_ (processDexterOperation (logChan config) (db config) b) operations
      _ <- insertProcessed config blkLevel $ Block.timestamp (Block.header b :: Block.RawBlockHeader)
      pure ()

-- querying the block from the tezos node is slow
processOperations :: OperationsWorkerConfig -> IO ()
processOperations config = do
  blockHead <- processHeadBlock (logChan config) (client config)
  case blockHead of
    Nothing -> do
      atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.ERROR, L.vals = Nothing , L.msg = "Operations worker failed to get current state of chain"}
    Just b -> do  
      let bLevel = parseLevel b
      eitherLatest <- catchSqlErrors $ OB.selectLatest (db config)
      case eitherLatest of
        Left err -> do
          let logMap = Just $ M.fromList [("error", T.pack $ show err)]
          atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.ERROR, L.vals = logMap , L.msg = "Operations worker failed to get last block processed"}
        Right latest -> do
          if null latest
          then do
            let blockLevels =  [(startLevel config) .. bLevel]
                logMap' = Just $ M.fromList [("start-level", T.pack $ show (startLevel config))]
            atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.INFO, L.vals = logMap' , L.msg = "Operations worker processing blocks from scratch"}
            mapM_ (processOperation config) blockLevels
          else do
            let lastLevel = obBlockLevel $ head latest
                logMap' = Just $ M.fromList [("start-level", T.pack $ show lastLevel)]
            atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.INFO, L.vals = logMap' , L.msg = "Operations worker processing blocks from last known level"}
            if lastLevel == bLevel
            then do
              atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.INFO, L.vals = logMap' , L.msg = "Operations worker in state"}
            else do
              let fromlevel = lastLevel + 1
              mapM_ (processOperation config) [fromlevel .. bLevel]

processHeadBlock :: Tchan.TChan L.LogMsg -> ClientConfig -> IO (Maybe Block.Block)
processHeadBlock logchan cli = do
        atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = Nothing, L.msg = "Requesting head block"}
        eitherBlock <- requestBlock cli BlockIdHead
        case eitherBlock of
            Left err -> do
                let logMap = Just $ M.fromList [("error", T.pack $ show err)]
                atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.ERROR, L.vals = logMap, L.msg = "Failed to request head block"}
                return Nothing
            Right headBlock -> do
                let level = parseLevel headBlock 
                    logMap = Just $ M.fromList [("block-level", T.pack $ show level)]
                atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.DEBUG, L.vals = logMap, L.msg = "Updating cache with head block"}
                return $ Just headBlock


-- | Requests a block at a specific level. Will first look for the block in the cache, 
-- if the block is not there it will request it and then insert it into the cache.
processBlock :: Tchan.TChan L.LogMsg  -> ClientConfig -> Int64 -> IO (Maybe Block.Block)
processBlock logchan tezosClient level = do
    let blockIdLevel = BlockIdLevel $ fromIntegral level
        logMap = Just $ M.fromList [("block-level", T.pack $ show level)]
    atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Requesting block"}

    eitherBlock <- requestBlock tezosClient blockIdLevel -- Request block at level
    case eitherBlock of
        Left err -> do -- Failed to get block
            let logMap' = Just $ M.fromList [("block-level", T.pack $ show level),("error", T.pack $ show err)]
            atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.ERROR, L.vals = logMap', L.msg = "Failed to request block"}
            return Nothing
        Right block -> do -- Insert block into cache
            atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.DEBUG , L.vals = logMap, L.msg = "Inserted block into cache"}
            return $ Just block

-- =============================================================================
-- config, threading and run
-- =============================================================================

data OperationsWorkerConfig =
    OperationsWorkerConfig
    { logChan :: Tchan.TChan L.LogMsg
    , client :: ClientConfig
    , db :: Connection 
    , startLevel :: Int64
    , dexters :: [DexterExchange]
    , workerInterval :: Int
    }

-- | Starts the recursive process of synchronizing operations from the blockchain
loop :: OperationsWorkerConfig -> Int -> IO ()
loop config interval = do
  threadDelay interval
  processOperations config
  loop config (workerInterval config)

run :: OperationsWorkerConfig -> IO ()
run config = do
  atomically $
    Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.INFO, L.vals = Nothing, L.msg = "Starting DexterOperation worker"}  
  void $ forkIO $ loop config 0
