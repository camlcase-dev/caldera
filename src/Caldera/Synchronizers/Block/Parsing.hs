{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Caldera.Synchronizers.Block.Parsing(
    parseLevel
  , textToContractId
  ) where 

import qualified Data.Text as T
import Data.Int (Int64)
import qualified Tezos.Client.Types.Core           as CoreTypes
import qualified Tezos.Client.Types.Block     as BLK


parseLevel :: BLK.Block -> Int64
parseLevel block = getBlockLevel $ BLK.header block

textToContractId :: T.Text -> CoreTypes.ContractId
textToContractId contractId = CoreTypes.ContractId{ unContractId = CoreTypes.UnistringText contractId}
        
getBlockLevel :: BLK.RawBlockHeader -> Int64
getBlockLevel blockHeader = BLK.level (blockHeader :: BLK.RawBlockHeader)
