{-|
Module      : Caldera.Synchronizers.DailyPrices.Worker
Description : Collect XTZ prices in various currencies and store in the database
Copyright   : (c) camlCase, 2020-2021
Maintainer  : brice@camlcase.io

-}

{-# LANGUAGE OverloadedStrings #-}

module Caldera.Synchronizers.DailyPrices.Worker
  ( PricesWorkerConfig(..)
  , startPricesWorker
  ) where

-- base
import qualified Control.Concurrent.STM.TChan as Tchan
import qualified Data.Text as T
import Control.Concurrent (threadDelay)
import Data.Text (Text)
import Data.Time.Clock (utctDay)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Data.Time (Day)

-- APIs used by caldera
import Caldera.Clients.Generic (ClientConfig(..))
import Caldera.Clients.Gecko.API (requestTezosPriceHistory, requestVsCurrencies)
import Caldera.Clients.Gecko.Types (MarketChart, prices, time, value)

-- logging
import qualified Logging.Logger as L

-- database 
import qualified Caldera.Database.Query.DailyXtzPrice as Daily
import Caldera.Database (catchSqlErrors)
import Caldera.Database.Table.DailyXtzPrice
import Database.PostgreSQL.Simple (Connection)

-- =============================================================================
-- types
-- =============================================================================

-- | Config type for this worker
data PricesWorkerConfig = PricesWorkerConfig
  { workerInterval :: Int -- ^ time to delay between each run of the main worker
  , clientConfig :: ClientConfig -- ^ API info for querying
  , databaseConnection :: Connection -- ^ connection to the database
  }

-- =============================================================================
-- main functions
-- =============================================================================

-- | Starting point that spawns all the price workers
startPricesWorker
  :: Tchan.TChan L.LogMsg -- ^ shared log channel between all workers
  -> PricesWorkerConfig
  -> Day -- ^ the day that the first dexter contract was originated, we do not need data earlier than this
  -> IO () 
startPricesWorker logchan config beginningOfDexter = do
  logInfo logchan "Price worker has started" []  
  eCurrencies <- requestVsCurrencies $ clientConfig config
  case eCurrencies of
    Left err -> do
      logError
        logchan
        "Prices worker failed to get list of supported currencies"
        [("error", T.pack $ show err)]

    Right currencies -> do
      mapM_ (getDataForCurrency logchan config beginningOfDexter) currencies

  -- wait before rerunning this process
  threadDelay $ workerInterval config
  startPricesWorker logchan config beginningOfDexter

-- | Get the last known entry for a particular currency by date, then get all
-- the data from CoinGecko from them until now for a particular currency.
-- getDataForCurrency
--   :: Tchan.TChan L.LogMsg
--   -> PricesWorkerConfig
--   -> Day
--   -> T.Text
--   -> IO ()
getDataForCurrency
  :: Tchan.TChan L.LogMsg
  -> PricesWorkerConfig
  -> Day
  -> T.Text
  -> IO ()
getDataForCurrency logchan config beginningOfDexter currency = do
  eMarketCharts <- requestTezosPriceHistory (clientConfig config) currency
 
  case eMarketCharts of 
    Left err -> do
      -- the request for data from CoinGecko failed
      logError
        logchan
        "Price worker failed to get historical data for 'tezos'"
        [("currency", currency), ("error", T.pack $ show err)]

    Right marketCharts -> do               
      -- this is likely slow, improve by running a single upsert
      mapM_ (\table -> do
                eUpsert <- catchSqlErrors $ Daily.upsert (databaseConnection config) table
                either
                  (\ex -> logError logchan ("Failed to upsert into daily_xtz_price: " <> T.pack ex) [])
                  (\_ -> pure ())
                  eUpsert
            ) $
        marketChartToDailyXtzPrices beginningOfDexter currency marketCharts

-- | Convert MarketChart (the value from the CoinGecko API) into DailyXtzPrice
-- (a caldera table for storing the value of XTZ in another currency at a
-- particular day). Remove any values that are less than the beginning of dexter.
marketChartToDailyXtzPrices :: Day -> T.Text -> MarketChart -> [DailyXtzPrice]
marketChartToDailyXtzPrices beginningOfDexter currency chart =
  -- only keep prices that occur after beginningOfDexter
  let dexterEraPrices =
        filter
        (\p ->
           (epochToDay . time) p >= beginningOfDexter
        )
        (prices chart) in

  fmap
  (\price ->
      DailyXtzPrice
      { dxpDailyXtzPriceId = 0 -- beam upsert will set this value
      , dxpVsCurrency = currency
      , dxpPrice = (realToFrac . value) price
      , dxpDate = (epochToDay . time) price
      }
  )
  dexterEraPrices

-- =============================================================================
-- utility functions
-- =============================================================================

-- | convert epoch time to a Day value 
epochToDay :: Integral a => a -> Day
epochToDay t = utctDay $ posixSecondsToUTCTime  (fromIntegral t/1000)

-- | L.logInfo with a "DailyPrices.Worker" tag
logInfo :: Tchan.TChan L.LogMsg -> Text -> [(Text,Text)] -> IO ()
logInfo = L.logInfo "DailyPrices.Worker"

-- | L.logError with a "DailyPrices.Worker" tag
logError :: Tchan.TChan L.LogMsg -> Text -> [(Text,Text)] -> IO ()
logError = L.logError "DailyPrices.Worker"
