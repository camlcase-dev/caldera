{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Caldera.API.Volume(
    getVolumeLatest
  , getVolumeAtInterval
  , getVolumeLatestAtContract
  , getVolumeAtIntervalForContract
  ) where

import qualified Control.Concurrent.STM.TChan as Tchan
import qualified Data.Cache as Cache
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Logging.Logger as L
import Control.Monad.STM (atomically)
import Data.Either (lefts, rights)
import Data.Functor ((<&>))
import Data.Maybe (catMaybes)
import Data.Time (LocalTime, utcToLocalTime, utc)
import Data.Time.Clock(getCurrentTime, utctDay)
import qualified Data.Set as S

-- database
import Database.PostgreSQL.Simple (Connection)

-- caldera
import qualified Caldera.API.Cache.Utils as Utils
import qualified Caldera.API.Cache.Volume as VolumeCache
import qualified Caldera.API.Types as Types
import qualified Caldera.Database.Query.DexterOperation as DO
import Caldera.Database.Table.DexterContract
import qualified Caldera.Database.Query.DexterContract as DC
import Caldera.Database (catchSqlErrors)
import Dexter.Exchange.Transaction

getVolumeAtInterval
  :: Cache.Cache (T.Text, T.Text) Types.Volume
  -> T.Text
  -> IO (Either String [[Types.Volume]])
getVolumeAtInterval cache interval = do
  case interval of
    "1w" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = Utils.getLastXDays 7 currentDay
      maybeVolumes <- mapM (getVolumeFromCache cache . T.pack . show) days
      pure $ Right (map catMaybes maybeVolumes)
    "1M" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = Utils.getLastXDays 30 currentDay
      maybeVolumes <- mapM (getVolumeFromCache cache . T.pack . show) days
      pure $ Right (map catMaybes maybeVolumes)
    "1y" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = Utils.getLastXDays 365 currentDay
      maybeVolumes <- mapM (getVolumeFromCache cache . T.pack . show) days
      pure $ Right (map catMaybes maybeVolumes)
    "all" -> do
      -- TODO: We need to figure out an efficient way to get all data. For now this is set at 1y because dexter is not 1y old. 
      currentDay <- getCurrentTime <&> utctDay
      let days = Utils.getLastXDays 365 currentDay
      maybeVolumes <- mapM (getVolumeFromCache cache . T.pack . show) days
      pure $ Right (map catMaybes maybeVolumes)
    _ -> pure $ Left "Unsupported interval."

getVolumeAtIntervalForContract
  :: Cache.Cache (T.Text, T.Text) Types.Volume
  -> T.Text
  -> T.Text
  -> IO (Either String [Types.Volume])
getVolumeAtIntervalForContract cache contractId interval = do
  case interval of
    "1w" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = Utils.getLastXDays 7 currentDay
      maybeVolumes <- mapM (getVolumeFromCacheByContract cache contractId . T.pack . show) days
      pure $ Right (catMaybes maybeVolumes)
    "1M" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = Utils.getLastXDays 30 currentDay
      maybeVolumes <- mapM (getVolumeFromCacheByContract cache contractId . T.pack . show) days
      pure $ Right (catMaybes maybeVolumes)
    "1y" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = Utils.getLastXDays 365 currentDay
      maybeVolumes <- mapM (getVolumeFromCacheByContract cache contractId . T.pack . show) days
      pure $ Right (catMaybes maybeVolumes)
    "all" -> do
      -- TODO: We need to figure out an efficient way to get all data. For now this is set at 1y because dexter is not 1y old. 
      currentDay <- getCurrentTime <&> utctDay
      let days = Utils.getLastXDays 365 currentDay
      maybeVolumes <- mapM (getVolumeFromCacheByContract cache contractId . T.pack . show) days
      pure $ Right (catMaybes maybeVolumes)
    _ -> pure $ Left "Unsupported interval."

getVolumeFromCache :: Cache.Cache (T.Text, T.Text) Types.Volume -> T.Text  -> IO [Maybe Types.Volume]
getVolumeFromCache cache day = do
    keys <- Cache.keys cache
    let contractIDs = mkUniq $ map fst keys
    mapM (getVolumeFromCacheByContract' cache day) contractIDs

getVolumeFromCacheByContract
  :: Cache.Cache (T.Text, T.Text) Types.Volume
  -> T.Text
  -> T.Text 
  -> IO (Maybe Types.Volume)
getVolumeFromCacheByContract cache contractId vId = do
    eitherVolume <- Cache.lookup cache (contractId, vId)
    case eitherVolume of 
      Nothing -> pure Nothing
      Just volume -> pure $ Just volume

getVolumeFromCacheByContract'
  :: Cache.Cache (T.Text, T.Text) Types.Volume
  -> T.Text 
  -> T.Text
  -> IO (Maybe Types.Volume)
getVolumeFromCacheByContract' cache vID contractId = getVolumeFromCacheByContract cache contractId vID

getVolumeLatest
  :: Tchan.TChan L.LogMsg
  -> Cache.Cache (T.Text, T.Text) Types.Volume
  -> Connection
  -> IO (Maybe [Types.Volume]) -- TODO: Better error handling to return to the client request
getVolumeLatest logchan cache db = do
  maybeVolumes <- getVolumeFromCache cache "current"
  let volumes = catMaybes maybeVolumes
  if not (null volumes)
    then return $ Just volumes
    else do
      currentTime <- getCurrentTime
      let currentLocalTime = utcToLocalTime utc currentTime
      dexterContracts <- DC.selectAll db
      volumes' <- mapM (get24HourVolumeForContract logchan db currentLocalTime) (fmap dcDexterContractId dexterContracts)
      if not (null volumes')
        then return $ Just volumes
        else return Nothing  

-- | Selects xtzToToken and tokenToXTZ transactions over the past 24 hours.
selectTradingTables24Hours
  :: Tchan.TChan L.LogMsg
  -> Connection
  -> T.Text
  -> LocalTime
  -> IO [DexterTransaction]
selectTradingTables24Hours logChan db contractId timestamp = do
  eitherOperations <- catchSqlErrors $ DO.selectTradesFor24HourPeriod db contractId timestamp
  case eitherOperations of
    Left err -> do
      let logMap = Just $ M.fromList [("error", T.pack (show err))]
      atomically $ Tchan.writeTChan logChan L.LogMsg { L.level = L.ERROR  , L.vals = logMap, L.msg = "Failed to select transactions over 24 hours"}
      pure []
    Right operations -> do
      let eTransansactions = fmap DO.parseDexterTransaction operations
          fails = lefts eTransansactions
          successes = rights eTransansactions
      if not (null fails)
      then do
        let logMap = Just $ M.fromList [("error", T.pack (show fails)), ("day", T.pack (show fails))]
        atomically $ Tchan.writeTChan logChan L.LogMsg { L.level = L.ERROR  , L.vals = logMap, L.msg = "Volume API failed to parse dexter transactions from operation"}
      else pure ()
      pure successes

-- | Selects the xtzToToken and tokenToXTZ transactions for a contract over 24 hours and converts the values into Types.Volume
get24HourVolumeForContract
  :: Tchan.TChan L.LogMsg
  -> Connection
  -> LocalTime
  -> T.Text
  -> IO Types.Volume
get24HourVolumeForContract logchan db day contractId = do
  transactions <- selectTradingTables24Hours logchan db contractId day
  let totalSold   = sum $ fmap VolumeCache.calculateVolumeSold transactions
      totalBought = sum $ fmap VolumeCache.calculateVolumeBought transactions
      totalVolume = totalSold + totalBought
  
  pure $
    Types.Volume
    { volumeContract = contractId
    , volumeTotal = T.pack $ show totalVolume
    , volumeSold = T.pack $ show totalSold
    , volumeBought = T.pack $ show totalBought
    , volumeDate = T.pack $ show day
    }

getVolumeLatestAtContract 
  :: Tchan.TChan L.LogMsg 
  -> Cache.Cache (T.Text, T.Text) Types.Volume
  -> Connection 
  -> T.Text 
  -> IO Types.Volume
getVolumeLatestAtContract logchan cache db contractId = do
    maybeVolume <- getVolumeFromCacheByContract cache contractId "current"
    case maybeVolume of
      Just volume -> return volume
      Nothing -> do
        currentTime <- getCurrentTime
        let currentLocalTime = utcToLocalTime utc currentTime
        get24HourVolumeForContract logchan db currentLocalTime contractId 

mkUniq :: Ord a => [a] -> [a]
mkUniq = S.toList . S.fromList