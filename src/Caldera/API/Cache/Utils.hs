{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Caldera.API.Cache.Utils(
    getLastXDays
  , dayRange
  , purge
  ) where

import Data.Time ( addDays, Day )
import qualified Data.Text as T
import qualified Data.Hashable as Hash
import qualified Data.Cache as Cache
import Control.Concurrent ( threadDelay )

-- | Gets the last (x) amount of days
getLastXDays :: Integer -> Day -> [Day]
getLastXDays x day = if x == 0
                        then []
                        else do
                            let next = addDays (-x) day
                            next:getLastXDays (x-1) day

dayRange :: [Day] -> T.Text 
dayRange days = do
    if null days
        then ""
        else do
            let f = head days
                l = last days
            T.pack $ show f ++ "/" ++ show l

purge :: (Eq k, Hash.Hashable k) => Cache.Cache k v -> Int -> IO ()
purge c i = do
    threadDelay i
    Cache.purgeExpired c
    purge c i 