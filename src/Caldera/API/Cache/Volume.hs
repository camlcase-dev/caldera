{-|
Module      : Caldera.API.Cache.Price
Copyright   : (c) camlCase, 2021
Maintainer  : brice@camlcase.io

The volume cache worker updates the yearly volume for all contracts over a year by day.
-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Strict #-} 

module Caldera.API.Cache.Volume(
    VolumeCacheConfig(..) 
  , calculateVolumeSold
  , calculateVolumeBought
  , start
  ) where

-- API
import qualified Caldera.API.Types as Types
import qualified Caldera.API.Cache.Utils as Utils

-- DATA
import qualified Data.Cache as Cache
import qualified Data.Text as T
import Control.Monad ( when )
import Data.Either (lefts, rights)
import Data.Time.Clock(getCurrentTime, utctDay)
import Data.Functor ( (<&>) )
import Data.Time (LocalTime, Day, utcToLocalTime, utc)


-- Concurrency
import qualified Control.Concurrent.STM.TChan as Tchan
import Control.Monad.STM (atomically)
import Control.Concurrent ( threadDelay, forkIO )

-- Database
import Database.PostgreSQL.Simple (Connection)
import Caldera.Database (catchSqlErrors)
import qualified Caldera.Database.Query.DexterOperation as DO
import qualified Caldera.Database.Query.DexterContract as DC
import Caldera.Database.Table.DexterContract

-- Logging
import qualified Logging.Logger as L
import qualified Data.Map as M
import Dexter.Exchange.Transaction
import Tezos.Client.Types.Core (unMutez) 

data VolumeCacheConfig =
    VolumeCacheConfig
    { logChan         :: Tchan.TChan L.LogMsg
    , db              :: Connection 
    , cache           :: Cache.Cache (T.Text, T.Text) Types.Volume 
    , workerInterval  :: Int
    }

-- | Caches volume for all contracts over the past year
start :: VolumeCacheConfig -> IO ()
start config = do
    atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.DEBUG , L.vals = Nothing, L.msg = "Starting volume cache worker."}
    _ <- forkIO $ Utils.purge (cache config) 10000000 
    _ <- forkIO $ start' config 0
    return ()

-- | Starts the recursive process of cacheing volume
start' :: VolumeCacheConfig -> Int -> IO ()
start' config i = do
    threadDelay i
    updateCurrentVolume config
    updateYearlyVolume config
    start' config (workerInterval config)

-- | Gets all the xtzToToken and tokenToXTZ transactions for the year and compiles the total sold, total bought
-- and total (sold + bought) on a day to day basis and then inserts each day into a cache.
updateYearlyVolume :: VolumeCacheConfig -> IO ()
updateYearlyVolume config = do
    currentDay <- getCurrentTime <&> utctDay
    let yearDays = Utils.getLastXDays 365 currentDay

    let logMap = Just $ M.fromList [("days", Utils.dayRange yearDays)]
    atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.DEBUG , L.vals = logMap, L.msg = "Volume cache worker updating cache with new days"}

    let updateCacheFuncs = map (updateCache config) yearDays
    dexterContracts <- DC.selectAll (db config)
    let contractIds = fmap dcDexterContractId dexterContracts
    mapM_ (`mapM_` contractIds) updateCacheFuncs

updateCurrentVolume :: VolumeCacheConfig -> IO ()
updateCurrentVolume config = do
  atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.DEBUG , L.vals = Nothing, L.msg = "Volume cache worker getting current volume"} 
  dexterContracts <- DC.selectAll (db config)
  mapM_ (updateCurrentVolume' config) (fmap dcDexterContractId dexterContracts)  


updateCurrentVolume'
  :: VolumeCacheConfig
  -> T.Text
  -> IO ()
updateCurrentVolume' config contractId = do
  currentTime <- getCurrentTime
  let currentLocalTime = utcToLocalTime utc currentTime
  transactions <- selectTradingTables24Hours (logChan config) (db config) contractId currentLocalTime
  let totalSold   = sum $ fmap calculateVolumeSold transactions
      totalBought = sum $ fmap calculateVolumeBought transactions
      totalVolume = totalSold + totalBought
      volume = Types.Volume
                { volumeContract = contractId
                , volumeTotal = T.pack $ show totalVolume
                , volumeSold = T.pack $ show totalSold
                , volumeBought = T.pack $ show totalBought
                , volumeDate = T.pack $ show currentLocalTime
                }
  Cache.insert (cache config) (contractId, "current") volume

-- | Selects xtzToToken and tokenToXTZ transactions over the past 24 hours.
selectTradingTables24Hours
  :: Tchan.TChan L.LogMsg
  -> Connection
  -> T.Text
  -> LocalTime
  -> IO [DexterTransaction]
selectTradingTables24Hours logchan dbc contractId timestamp = do
  eitherOperations <- catchSqlErrors $ DO.selectTradesFor24HourPeriod dbc contractId timestamp
  case eitherOperations of
    Left err -> do
      let logMap = Just $ M.fromList [("error", T.pack (show err))]
      atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.ERROR  , L.vals = logMap, L.msg = "Failed to select transactions over 24 hours"}
      pure []
    Right operations -> do
      let eTransansactions = fmap DO.parseDexterTransaction operations
          fails = lefts eTransansactions
          successes = rights eTransansactions
      if not (null fails)
      then do
        let logMap = Just $ M.fromList [("error", T.pack (show fails)), ("day", T.pack (show fails))]
        atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.ERROR  , L.vals = logMap, L.msg = "Volume API failed to parse dexter transactions from operation"}
      else pure ()
      pure successes


-- | Selects xtzToToken and tokenToXTZ transactions for a specific day and returns their maybe's in a pair.
selectTransactionsAtDay :: Tchan.TChan L.LogMsg -> Connection -> T.Text ->  Day -> IO [DexterTransaction]
selectTransactionsAtDay logchan pdb contractId day = do
  eitherOperations <- catchSqlErrors $ DO.selectOperationsForDay pdb contractId day
  case eitherOperations of
    Left err -> do
      let logMap = Just $ M.fromList [("error", T.pack (show err)), ("day", T.pack (show day))]
      atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.ERROR  , L.vals = logMap, L.msg = "Volume cache worker failed to get transactions for day"}
      pure []

    Right operations -> do
      let eTransansactions = fmap DO.parseDexterTransaction operations
          fails = lefts eTransansactions
          successes = rights eTransansactions
      if not (null fails)
      then do
        let logMap = Just $ M.fromList [("error", T.pack (show fails)), ("day", T.pack (show fails))]
        atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.ERROR  , L.vals = logMap, L.msg = "Volume cache worker failed to parse dexter transactions from operation"}
      else pure ()
      pure successes

-- | Selects the xtzToToken and tokenToXTZ transactions for a contract at a specific day and then inserts
-- the associated volume value into the cache
updateCache :: VolumeCacheConfig -> Day -> T.Text -> IO ()
updateCache config day contractId = do
    transactions <- selectTransactionsAtDay (logChan config) (db config) contractId day
    let totalSold   = sum $ fmap calculateVolumeSold transactions
        totalBought = sum $ fmap calculateVolumeBought transactions
        totalVolume = totalSold + totalBought

    when (totalVolume /= 0) $ do
        let volume = Types.Volume
                    { volumeContract = contractId
                    , volumeTotal = T.pack $ show totalVolume
                    , volumeSold = T.pack $ show totalSold
                    , volumeBought = T.pack $ show totalBought
                    , volumeDate = T.pack $ show day
                    }
        Cache.insert (cache config) (contractId, T.pack $ show day) volume

calculateVolumeBought :: DexterTransaction -> Integer
calculateVolumeBought =
  \case
    XtzToToken xtzToToken -> fromIntegral $ unMutez $ xtzSold (xtzToToken :: XtzToTokenTransaction)
    _ -> 0

calculateVolumeSold :: DexterTransaction -> Integer
calculateVolumeSold =
  \case
    TokenToXtz tokenToXtz -> fromIntegral $ unMutez $ totalXtzBought (tokenToXtz :: TokenToXtzTransaction)
    _ -> 0
