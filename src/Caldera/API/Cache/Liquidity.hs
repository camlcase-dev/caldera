{-|
Module      : Caldera.API.Cache.Liquidity
Copyright   : (c) camlCase, 2021
Maintainer  : brice@camlcase.io

The liquididty cache worker updates the yearly liquidity for all contracts over a year by day.
-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE Strict #-} 

module Caldera.API.Cache.Liquidity(
    start
  , LiquidityCacheConfig(..)
  ) where

import qualified Data.Cache as Cache
import qualified Data.Text as T
import Data.Time.Clock(getCurrentTime, utctDay)
import Data.Time ( Day )
import Data.Functor ( (<&>) )

-- Concurrency
import qualified Control.Concurrent.STM.TChan as Tchan
import Control.Monad.STM (atomically)
import Control.Concurrent ( threadDelay, forkIO ) 

import qualified Caldera.API.Types as Types
import qualified Logging.Logger as L
import qualified Data.Map as M
import qualified Caldera.API.Cache.Utils as Utils

-- database
import Database.PostgreSQL.Simple (Connection)
import Caldera.Database.Table.DexterContractAndOperation
import qualified Caldera.Database.Query.DexterContractAndOperation as DCO

data LiquidityCacheConfig =
  LiquidityCacheConfig
    { logChan        :: Tchan.TChan L.LogMsg
    , db             :: Connection 
    , cache          :: Cache.Cache (T.Text, T.Text) Types.Liquidity
    , workerInterval :: Int
    }

-- | Caches liquidity for all contracts over the past year
start :: LiquidityCacheConfig -> IO ()
start config = do
  atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.INFO, L.vals = Nothing, L.msg = "Starting liquidity cache worker."}
  _ <- forkIO $ Utils.purge (cache config) 10000000 
  _ <- forkIO $ start' config 0
  pure ()

-- | Starts the recursive process of cacheing liquidity
start' :: LiquidityCacheConfig -> Int -> IO ()
start' config i = do
  threadDelay i
  updateCurrentLiquidity config
  updateYearlyLiquidity config
  start' config (workerInterval config)

-- | Stores Liquidity in a cache where the keys are the contract address. The cache returns a cache of 
-- days for that contract, where the value is liquidity.
updateYearlyLiquidity :: LiquidityCacheConfig -> IO ()
updateYearlyLiquidity config = do
  currentDay <- getCurrentTime <&> utctDay
  let yearDays = Utils.getLastXDays 365 currentDay
  let logMap = Just $ M.fromList [("days", Utils.dayRange yearDays)]
  atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.DEBUG , L.vals = logMap, L.msg = "Liquidity cache worker updating cache with new days"}
  mapM_ (updateCache config) yearDays


updateCurrentLiquidity :: LiquidityCacheConfig -> IO ()
updateCurrentLiquidity config = do
  atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.DEBUG , L.vals = Nothing, L.msg = "Liquidity cache worker getting current liquidity"}
  eitherDexters <- DCO.selectLatest (db config)
  case eitherDexters of
    Left err -> do
      let logMap = Just $ M.fromList [("error", T.pack (show err))]
      atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.ERROR  , L.vals = logMap, L.msg = "Liquidity cache worker failed to get current liquidity"}
    Right dexters -> do
      currentDay <- getCurrentTime <&> utctDay
      let liquidities = fmap (contractToLiquidity currentDay) dexters
      mapM_ (updateCurrentLiquidity' config) liquidities

updateCurrentLiquidity' :: LiquidityCacheConfig -> Types.Liquidity -> IO ()
updateCurrentLiquidity' config liquidity = do
      Cache.insert (cache config) (Types.liquidityContract liquidity, "current") liquidity 

contractToLiquidity
  :: Day
  -> DexterContractAndOperation
  -> Types.Liquidity
contractToLiquidity day do_ =
  Types.Liquidity
    { liquidityName      = "XTZ/" <> dcoTokenSymbol do_
    , liquidityDecimals  = fromIntegral $ dcoDecimals do_
    , liquidityDay       = T.pack $ show day    
    , liquidityContract  = dcoDexterContractId do_
    , liquidityLqtTotal  = dcoLqtTotal do_
    , liquidityTokenPool = dcoTokenPool do_
    , liquidityXtzPool   = dcoXtzPool do_
    }

-- | Updates the cache with a specific day
updateCache :: LiquidityCacheConfig -> Day -> IO ()
updateCache config day = do
  eitherStorage <- DCO.selectOperationsForDay (db config) day
  case eitherStorage of
    Left err -> do
      let logMap = Just $ M.fromList [("error", T.pack (show err)), ("day", T.pack (show day))]
      atomically $ Tchan.writeTChan (logChan config) L.LogMsg { L.level = L.ERROR  , L.vals = logMap, L.msg = "Liquidity cache worker failed to get list of unique contracts"}
    Right storage -> do
      mapM_ (updateCache' (cache config) day) storage

-- | Takes a contract storage table and a day and inserts it into the cache after transforming it into Liquidity
updateCache'
  :: Cache.Cache (T.Text, T.Text) Types.Liquidity 
  -> Day
  -> DexterContractAndOperation
  -> IO ()
updateCache' lcache day table = do
      Cache.insert lcache (dcoDexterContractId table, T.pack $ show day)
        $ Types.Liquidity 
          { liquidityName = "XTZ/" <> dcoTokenSymbol table
          , liquidityContract = dcoDexterContractId table
          , liquidityLqtTotal = dcoLqtTotal table
          , liquidityTokenPool = dcoTokenPool table
          , liquidityXtzPool = dcoXtzPool table
          , liquidityDecimals = fromIntegral $ dcoDecimals table
          , liquidityDay = T.pack $ show day
          } 
