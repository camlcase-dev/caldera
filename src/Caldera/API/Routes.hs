{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Caldera.API.Routes
  ( CalderaAPI
  , API
  , calderaSwagger
  , writeSwaggerJSON
  ) where

import qualified Data.ByteString.Lazy.Char8 as BL8

import Caldera.API.Types (Contract, Liquidity, Price, Volume)
import Caldera.API.Types.LiquidityPosition (IndividualLiquidityPosition)
import Caldera.API.Types.LiquidityReward (CumulativeLiquidityReward, ExchangeLiquidityReward, IndividualLiquidityReward)
import Control.Lens
import Data.Aeson.Encode.Pretty (encodePretty)
import Data.Swagger
import Data.Text (Text)
import Servant
import Servant.Swagger
import Servant.Swagger.UI

-- | Servant type-level API, generated from the Swagger spec for Caldera.
type CalderaAPI =
  
   -- contractsH
  Summary "get data for all dexter contracts at the current or selected block level"
  :> "contracts" :> QueryParam "block" Text :> Verb 'GET 200 '[JSON] [Contract]

  -- contractsContractIDGetH
  
  :<|> Summary "get data for a dexter contract by its KT1 address at the current or selected block level"
       :> "contracts" :> Capture "contractID" Text :> QueryParam "block" Text :> Verb 'GET 200 '[JSON] Contract

  -- pricesVsCurrencyIDGetH
  :<|> Summary "get the price of XTZ in a target currency for a given day"
       :> "prices" :> Capture "vsCurrency" Text :> QueryParam "timeframe" Text :> Verb 'GET 200 '[JSON] [Price] 

  -- liquidityRewardsContractIdGetCurrentH
  :<|> Summary "get the cumulative liquidity rewards for a dexter contract by its KT1 address"
       :> "liquidity" :> "rewards" :> Capture "dexter-contract-id" Text :> "current" :> Verb 'GET 200 '[JSON] [ExchangeLiquidityReward] 

  -- getLiquidityRewardsAccountH
  :<|> Summary "get the cumulative liquidity rewards for an account by its tz address"
       :> "liquidity" :> "rewards" :> "account" :> Capture "address" Text :> "current" :> Verb 'GET 200 '[JSON] [IndividualLiquidityReward] 

  -- getLiquidityRewardsH
  :<|> Summary "get the cumulative liquidity rewards for all dexter contracts"
       :> "liquidity" :> "rewards" :> Verb 'GET 200 '[JSON] CumulativeLiquidityReward 

  -- liquidityGetAccountCurrentH
  :<|> Summary "get most recent liquidity position of a tz address"
       :> "liquidity" :> "account" :> Capture "address" Text :> "current" :> Verb 'GET 200 '[JSON] [IndividualLiquidityPosition]

  -- liquidityGet 
  :<|> Summary "get liquidity for all dexter contracts in a given timeframe"
       :> "liquidity" :> QueryParam "timeframe" Text :> Verb 'GET 200 '[JSON] [[Liquidity]] 

  -- liquidityGetCurrentH
  :<|> Summary "get latest liquidity amount for all dexter contracts"
       :> "liquidity" :> "current" :> Verb 'GET 200 '[JSON] [Liquidity] 

  -- liquidityContractIDGet 
  :<|> Summary "get the liquidity for a contract by its KT1 address in a give timeframe"
       :> "liquidity" :> Capture "contractID" Text :> QueryParam "timeframe" Text :> Verb 'GET 200 '[JSON] [Liquidity] 

  -- liquidityContractIDGetCurrent 
  :<|> Summary "get latest liquidity amount for a contract by its KT1 address"
      :> "liquidity" :> Capture "contractID" Text :> "current" :> Verb 'GET 200 '[JSON] Liquidity 

  :<|> Summary "get the number of transactions an account has performed over all dexter contracts"
       :> "transactions" :> "account" :> Capture "accountId" Text :> "current" :> Verb 'GET 200 '[JSON] Int

  -- transactionsGet
  :<|> Summary "get the number of transactions in the last 24 hours"
       :> "transactions" :> "current" :> Verb 'GET 200 '[JSON] Int 

  -- volumeGet 
  :<|> Summary "get the trade volume for all dexter contracts in a give timeframe"
       :> "volume" :> QueryParam "timeframe" Text :> Verb 'GET 200 '[JSON] [[Volume]] 

  -- volumeGetCurrent 
  :<|> Summary "get the trade volume for all dexter contracts in the last 24 hours"
       :> "volume" :> "current" :> Verb 'GET 200 '[JSON] [Volume] 

  -- volumeContractIDGet 
  :<|> Summary "get the trade volume for a contract by its KT1 address in a give timeframe"
       :> "volume" :> Capture "contractID" Text :> QueryParam "timeframe" Text :> Verb 'GET 200 '[JSON] [Volume] 

  -- volumeContractIDGetCurrent 
  :<|> Summary "get the trade volume for a contract by its KT1 address for the last 24 hours"
       :> "volume" :> Capture "contractID" Text :> "current" :> Verb 'GET 200 '[JSON] Volume 

-- | Combined Swagger and Caldera API
type API = SwaggerSchemaUI "swagger" "swagger.json" :<|> CalderaAPI

calderaSwagger :: Swagger
calderaSwagger = toSwagger (Proxy :: Proxy CalderaAPI)
  & info.title   .~ "Caldera API"
  & info.version .~ "1.0"
  & info.description ?~ "Provide data and statistics for Dexter contracts and accounts"
  & info.license ?~ ("BSD3" & url ?~ URL "https://opensource.org/licenses/BSD-3-Clause")


-- | Output generated @swagger.json@ file for @'Caldera.API'@.
writeSwaggerJSON :: IO ()
writeSwaggerJSON = BL8.writeFile "swagger.json" (encodePretty calderaSwagger)
