{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}

module Caldera.API.Prices
  ( Interval(..)
  , parseInterval
  , getPricesAtInterval
  ) where

import qualified Caldera.API.Types as Types
import qualified Data.Text as T

import Caldera.Database.Query.DailyXtzPrice (selectAllCurrencyPrices, selectCurrencyPricesForRange)
import Data.Time (addDays)
import Data.Time.Clock(getCurrentTime, utctDay)
import Database.PostgreSQL.Simple (Connection)

data Interval
  = OneWeek
  | OneMonth
  | OneYear
  | All
  deriving (Eq,Show)

parseInterval :: T.Text -> Maybe Interval
parseInterval t =
  case T.toLower t of
    "1w" -> Just OneWeek
    "1m" -> Just OneMonth
    "1y" -> Just OneYear
    "all" -> Just All
    _ -> Nothing 

getPricesAtInterval :: Connection -> Interval -> T.Text -> IO [Types.Price]
getPricesAtInterval conn interval vsCurrency = do
  today <- utctDay <$> getCurrentTime

  let eStart =
        case interval of
          OneWeek  -> Right $ addDays (-7) today
          OneMonth -> Right $ addDays (-30) today
          OneYear  -> Right $ addDays (-365) today
          All      -> Left ()

  dailyXtzPrices <-
    case eStart of
      Right start -> selectCurrencyPricesForRange conn vsCurrency start today
      Left _ -> selectAllCurrencyPrices conn vsCurrency


  pure $ fmap Types.dailyXtzPriceToPrice dailyXtzPrices
