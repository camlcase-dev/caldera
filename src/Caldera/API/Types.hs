{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE OverloadedStrings          #-}

module Caldera.API.Types
  ( Contract (..)
  , Liquidity (..)
  , Price (..)
  , Volume (..)
  , dailyXtzPriceToPrice
  ) where

import qualified Data.Text as T

import Caldera.Database.Table.DailyXtzPrice
import Control.Lens ((&), (?~), mapped)
import Data.Aeson (FromJSON(..), ToJSON(..), genericToJSON, genericParseJSON, withObject, object, (.:), (.=))
import Data.Aeson.Types (Options(..), defaultOptions)
import Data.List (stripPrefix)
import Data.Maybe (fromMaybe)
import Data.Int
import Data.Swagger
import Data.Text (Text)
import Data.Time (Day, fromGregorian)
import GHC.Generics (Generic)

-- | Dexter contract details at a particular block.
data Contract = Contract
  { contractName :: Text -- ^ the name of the dexter exchange
  , contract :: Text -- ^ the KT1 address
  , blockLevel :: Int64 -- ^ the block level it was queried it
  , blockHash :: Text -- ^ the hash of the block that this contract was queried at
  , contractLqtTotal :: Text -- ^ the lqtTotal at blockLevel
  , contractTokenPool :: Text -- ^ the tokenPool at blockLevel
  , contractXtzPool :: Text -- ^ the xtzPool at blockLevel
  , contractDecimals :: Int64 -- ^ the number of decimals, this is static
  , contractTimestamp :: Text -- ^ the timestamp of blockLevel
  } deriving (Show, Eq, Generic)

instance FromJSON Contract where
  parseJSON = withObject "Contract" $ \o ->
    Contract <$> o .: "name"
             <*> o .: "contract"
             <*> o .: "level"
             <*> o .: "hash"
             <*> o .: "lqtTotal"
             <*> o .: "tokenPool"
             <*> o .: "xtzPool"
             <*> o .: "decimals"
             <*> o .: "timestamp"

instance ToJSON Contract where
  toJSON (Contract contractName' contract' blockLevel' blockHash' contractLqtTotal' contractTokenPool' contractXtzPool' contractDecimals' contractTimestamp') =
    object
      [ "name"      .= contractName'
      , "contract"  .= contract'
      , "level"     .= blockLevel'
      , "hash"      .= blockHash'
      , "lqtTotal"  .= contractLqtTotal'
      , "tokenPool" .= contractTokenPool'
      , "xtzPool"   .= contractXtzPool'
      , "decimals"  .= contractDecimals'
      , "timestamp" .= contractTimestamp'
      ]

instance ToSchema Contract where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "Dexter contract details at a particular block"
    & mapped.schema.example ?~
    toJSON
    (Contract
     { contractName = "XTZ/tzBTC"
     , contract = "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N"
     , blockLevel = 1420018
     , blockHash = "BM9gYKtE91GJj9J1rnrVW819o4Cy2yFxassgECodiuuJrPGTcrW"
     , contractLqtTotal = "42513397911"
     , contractTokenPool = "377458796"
     , contractXtzPool = "35661094057"
     , contractDecimals = 8
     , contractTimestamp = "2021-04-08 12:27:39"
     }    
    )

-- | The amount of XTZ/Token/lqtTotal of a particular dexter contract
data Liquidity = Liquidity
  { liquidityName :: Text -- ^ name of dexter pair
  , liquidityContract :: Text -- ^ dexter contract id
  , liquidityLqtTotal :: Text -- ^ lqtTotal at day
  , liquidityTokenPool :: Text -- ^ tokenPool at day
  , liquidityXtzPool :: Text -- ^ xtzPool at day
  , liquidityDecimals :: Int -- ^ decimals (static)
  , liquidityDay :: Text -- ^ day this set of data belongs to
  } deriving (Show, Eq, Generic)

instance FromJSON Liquidity where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "liquidity")

instance ToJSON Liquidity where
  toJSON = genericToJSON (removeFieldLabelPrefix False "liquidity")

instance ToSchema Liquidity where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "The amount of XTZ/Token/lqtTotal of a particular dexter contract"
    & mapped.schema.example ?~
    toJSON
    (Liquidity
     { liquidityName = "XTZ/KUSD"
     , liquidityContract = "KT1AbYeDbjjcAnV1QK7EZUUdqku77CdkTuv6"
     , liquidityLqtTotal = "93590243159"
     , liquidityTokenPool = "420138186345629996045245"
     , liquidityXtzPool = "78579856702"
     , liquidityDecimals = 18
     , liquidityDay = "2021-03-09"
     }    
    )

-- | the price of XTZ in a given currency on a given day
data Price = Price
  { vsCurrency :: Text -- ^ usd, eur, btc, etc
  , price :: Double -- ^ the value of one XTZ in vsCurrency
  , date :: Day -- ^ the calendar day of the price
  } deriving (Show, Eq, Generic)

instance FromJSON Price
instance ToJSON Price
instance ToSchema Price where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "The price of XTZ in a given currency on a given day"
    & mapped.schema.example ?~ toJSON (Price "usd" 6.12 (fromGregorian 2021 4 8))
    
dailyXtzPriceToPrice :: DailyXtzPrice -> Price
dailyXtzPriceToPrice dxp =
  Price
    { vsCurrency = dxpVsCurrency dxp
    , price = dxpPrice dxp
    , date = dxpDate dxp
    }
  
-- | The trade volume of a particular dexter contract
data Volume = Volume
  { volumeContract :: Text -- ^ the KT1 address of the dexter contract
  , volumeTotal :: Text -- ^ sum of volumeSold and volumeBought
  , volumeSold :: Text -- ^ the amount of currency sold for volumeContract in XTZ
  , volumeBought :: Text -- ^ the amount of currency bought for volumeContract in XTZ
  , volumeDate :: Text -- ^ the calendar day
  } deriving (Show, Eq, Generic)

instance FromJSON Volume where
  parseJSON = genericParseJSON (removeFieldLabelPrefix True "volume")

instance ToJSON Volume where
  toJSON = genericToJSON (removeFieldLabelPrefix False "volume")

instance ToSchema Volume where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "The trade volume of a particular dexter contract"
    & mapped.schema.example ?~
    toJSON
    (Volume
      { volumeContract = "KT1AbYeDbjjcAnV1QK7EZUUdqku77CdkTuv6"
      , volumeTotal = "9981743344"
      , volumeSold = "4956501946"
      , volumeBought = "5025241398"
      , volumeDate = "2021-04-08 14:16:43.959309927"
      }
    )

-- | Remove a field label prefix during JSON parsing.
-- Also perform any replacements for special characters.
removeFieldLabelPrefix :: Bool -> String -> Options
removeFieldLabelPrefix forParsing prefix =
  defaultOptions
  {Data.Aeson.Types.fieldLabelModifier =
   fromMaybe (error ("did not find prefix " ++ prefix)) . stripPrefix prefix . replaceSpecialChars}
  where
    replaceSpecialChars field = foldl (&) field (map mkCharReplacement specialChars)
    specialChars =
      [ ("@", "'At")
      , ("\\", "'Back_Slash")
      , ("<=", "'Less_Than_Or_Equal_To")
      , ("\"", "'Double_Quote")
      , ("[", "'Left_Square_Bracket")
      , ("]", "'Right_Square_Bracket")
      , ("^", "'Caret")
      , ("_", "'Underscore")
      , ("`", "'Backtick")
      , ("!", "'Exclamation")
      , ("#", "'Hash")
      , ("$", "'Dollar")
      , ("%", "'Percent")
      , ("&", "'Ampersand")
      , ("'", "'Quote")
      , ("(", "'Left_Parenthesis")
      , (")", "'Right_Parenthesis")
      , ("*", "'Star")
      , ("+", "'Plus")
      , (",", "'Comma")
      , ("-", "'Dash")
      , (".", "'Period")
      , ("/", "'Slash")
      , (":", "'Colon")
      , ("{", "'Left_Curly_Bracket")
      , ("|", "'Pipe")
      , ("<", "'LessThan")
      , ("!=", "'Not_Equal")
      , ("=", "'Equal")
      , ("}", "'Right_Curly_Bracket")
      , (">", "'GreaterThan")
      , ("~", "'Tilde")
      , ("?", "'Question_Mark")
      , (">=", "'Greater_Than_Or_Equal_To")
      ]
    mkCharReplacement (replaceStr, searchStr) = T.unpack . replacer (T.pack searchStr) (T.pack replaceStr) . T.pack
    replacer =
      if forParsing
        then flip T.replace
        else T.replace
