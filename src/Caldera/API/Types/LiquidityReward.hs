{-|
Module      : Caldera.API.Types.LiquidityReward
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

The payload for Dexter liquidity rewards that get passed to the frontend.
Used for monitoring the history of liquidity rewards for Dexter trades
(tokenToToken, xtzToToken, tokenToXtz).

-}

{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}

module Caldera.API.Types.LiquidityReward
  ( ExchangeLiquidityReward(..)
  , parseExchangeLiquidityReward

  , CumulativeLiquidityReward(..)
  , CumulativeTokenLiquidityReward(..)
  , calculateCumulativeLiquidityReward

  , IndividualLiquidityReward(..)
  , liquidityRewardAndOperationToIndividualLiquidityReward
  ) where

import qualified Data.Map as Map
import qualified Data.Text as T

import Control.Lens ((&), (?~), mapped)
import Data.Aeson (FromJSON(..), ToJSON(..), object, withObject, (.:), (.=))
import Data.Maybe (fromMaybe)
import Data.Swagger
import Data.Text (Text)
import Data.Time (LocalTime)
import Dexter.Exchange.LiquidityReward (calculatePoolReward, tokenToMutez)
import GHC.Generics (Generic)
import GHC.Natural (Natural)
import Safe (readMay)

-- tezos
import Tezos.Client.Types (unBignum, unMutez)

-- caldera
import qualified Dexter.Exchange.Transaction as T
import Caldera.Database.Table.DexterOperation
import Caldera.Database.Query.DexterOperation
import Caldera.Database.Table.LiquidityRewardAndOperation

-- | This assumes that all DexterOperation belong to a single contract id, there
-- are slightly different considerations for cumulative and single pairs.
parseExchangeLiquidityReward :: Text -> DexterOperation -> Maybe ExchangeLiquidityReward
parseExchangeLiquidityReward dexterContractId dexterOperation =
  case parseDexterTransaction dexterOperation of
    Left _ -> Nothing
    Right dexterTransaction ->
      case dexterTransaction of
        T.XtzToToken tr ->
          Just $
          ExchangeLiquidityReward
          dexterContractId
          (calculatePoolReward . fromIntegral . unMutez $ T.xtzSold (tr :: T.XtzToTokenTransaction))
          0
          (doTimestamp dexterOperation)
          
        T.TokenToXtz tr ->
          Just $
          ExchangeLiquidityReward
          dexterContractId
          0
          (calculatePoolReward . fromIntegral . unBignum $ T.tokensSold (tr :: T.TokenToXtzTransaction))
          (doTimestamp dexterOperation)
          
        T.TokenToToken tr ->
          -- this is an edge case that will need a better solution, we should also capture
          -- xtz that come into this contract when this token is the outgoing value.
          -- right now the number of tokenToToken transactions are minimal so we can revisit
          -- this later
          Just $
          ExchangeLiquidityReward
          dexterContractId
          0
          (calculatePoolReward . fromIntegral . unBignum $ T.tokensSold (tr :: T.TokenToTokenTransaction))
          (doTimestamp dexterOperation)
          
        T.AddLiquidity _ -> Nothing
        T.RemoveLiquidity _ -> Nothing 
          
-- | LiquidityReward for a single exchange pair in regards to an exchange operation
data ExchangeLiquidityReward =
  ExchangeLiquidityReward
    { tlrDexterContractId :: Text -- ^ Dexter contract id
    , tlrXtzReward        :: Natural -- ^ XTZ reward amount
    , tlrTokenReward      :: Natural -- ^ FA1.2 or FA2 reward amount
    , tlrTimestamp        :: LocalTime -- ^ time the trade took place that created this reward
    } deriving (Eq,Show,Generic)

instance ToJSON ExchangeLiquidityReward where
  toJSON tlr =
    object
      [ "dexterContractId" .= tlrDexterContractId tlr
      , "xtzReward"        .= tlrXtzReward   tlr
      , "tokenReward"      .= tlrTokenReward tlr
      , "timestamp"        .= tlrTimestamp   tlr
      ]

instance FromJSON ExchangeLiquidityReward where
  parseJSON = withObject "ExchangeLiquidityReward" $ \o ->
    ExchangeLiquidityReward <$> o .: "dexterContractId"
                            <*> o .: "xtzReward"
                            <*> o .: "tokenReward"
                            <*> o .: "timestamp"

instance ToSchema ExchangeLiquidityReward where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "LiquidityReward for a single exchange pair in regards to an exchange operation"
    & mapped.schema.example ?~
    toJSON
    (ExchangeLiquidityReward
      { tlrDexterContractId = "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N"
      , tlrXtzReward        = 300000
      , tlrTokenReward      = 0
      , tlrTimestamp        = read "2021-04-08 14:28:44.772557472" -- dangerous but static
      }
    )

-- | the liquidity rewards earned by Dexter contracts across all of the
-- exchanges
data CumulativeLiquidityReward =
  CumulativeLiquidityReward
    { clrCumululativeXtzReward :: Natural -- ^ the sum of all ctlrXtzReward and ctlrTokenRewardInXtz from clrTokenRewards
    , clrTokenRewards          :: Map.Map Text CumulativeTokenLiquidityReward -- ^ details of liquidity rewards for each exchange
    } deriving (Eq,Show,Generic)

instance ToJSON CumulativeLiquidityReward where
  toJSON clr =
    object
      [ "cumulativeRewards" .= clrCumululativeXtzReward clr 
      , "tokenRewards"      .= clrTokenRewards clr
      ]

instance FromJSON CumulativeLiquidityReward where
  parseJSON = withObject "CumulativeLiquidityReward" $ \o ->
    CumulativeLiquidityReward <$> o .: "cumulativeRewards"
                              <*> o .: "tokenRewards"

instance ToSchema CumulativeLiquidityReward where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "The liquidity rewards earned by Dexter contracts across all of the exchanges"
    & mapped.schema.example ?~
    toJSON
    (CumulativeLiquidityReward
      { clrCumululativeXtzReward = 1000
      , clrTokenRewards          =
        Map.fromList
        [("XTZ/tzBTC",
        CumulativeTokenLiquidityReward
         { ctlrDexterContractId = "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N"
         , ctlrXtzReward   = 1000
         , ctlrTokenReward = 0
         , ctlrTokenRewardInXtz = 0
         , ctlrTokenPool   = 399929392393939
         , ctlrXtzPool     = 12312000000
         } 
        )]
      }
    )

-- | Cumulative liquidity rewards for a Dexter contract
data CumulativeTokenLiquidityReward =
  CumulativeTokenLiquidityReward
    { ctlrDexterContractId :: Text -- ^ the token's dexter contract id
    , ctlrXtzReward   :: Natural -- ^ the XTZ reward
    , ctlrTokenReward :: Natural -- ^ the token reward
    , ctlrTokenRewardInXtz :: Natural -- ^ the token reward expressed in XTZ based on the latest market rate
    , ctlrTokenPool   :: Natural -- ^ the amount of XTZ that ctlrDexterContractId has in the latest block
    , ctlrXtzPool     :: Natural -- ^ the amount of XTZ that ctlrDexterContractId has in the latest block
    } deriving (Eq,Show,Generic)

instance ToJSON CumulativeTokenLiquidityReward where
  toJSON ctlr =
    object
      [ "dexterContractId" .= ctlrDexterContractId ctlr 
      , "xtzReward"        .= ctlrXtzReward ctlr
      , "tokenReward"      .= ctlrTokenReward ctlr
      , "tokenRewardInXtz" .= ctlrTokenRewardInXtz ctlr
      , "tokenPool"        .= ctlrTokenPool ctlr
      , "xtzPool"          .= ctlrXtzPool ctlr
      ]
  
instance FromJSON CumulativeTokenLiquidityReward where
  parseJSON = withObject "CumulativeLiquidityReward" $ \o ->
    CumulativeTokenLiquidityReward <$> o .: "dexterContractId"
                                   <*> o .: "xtzReward"
                                   <*> o .: "tokenReward"
                                   <*> o .: "tokenRewardInXtz"
                                   <*> o .: "tokenPool"
                                   <*> o .: "xtzPool"

instance ToSchema CumulativeTokenLiquidityReward where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "Cumulative liquidity rewards for a Dexter contract"
    & mapped.schema.example ?~
    toJSON
    (CumulativeTokenLiquidityReward
      { ctlrDexterContractId = "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N"
      , ctlrXtzReward        = 1000
      , ctlrTokenReward      = 0
      , ctlrTokenRewardInXtz = 0
      , ctlrTokenPool        = 399929392393939
      , ctlrXtzPool          = 12312000000
      }
    )

-- | Cumulative liquidity rewards for a Dexter contract
data IndividualLiquidityReward =
  IndividualLiquidityReward
    { ilrDexterContractId  :: Text
    , ilrXtzReward         :: Text
    , ilrTokenReward       :: Text
    } deriving (Eq,Show,Generic)

instance ToJSON IndividualLiquidityReward where
  toJSON IndividualLiquidityReward{..} =
    object
      [ "dexterContractId" .= ilrDexterContractId
      , "xtzReward"        .= ilrXtzReward
      , "tokenReward"      .= ilrTokenReward
      ]

instance ToSchema IndividualLiquidityReward where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "Cumulative liquidity rewards for a Dexter contract"
    & mapped.schema.example ?~
    toJSON
    (IndividualLiquidityReward
      { ilrDexterContractId  = "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N"
      , ilrXtzReward         = "1454"
      , ilrTokenReward       = "3"
      }
    )

liquidityRewardAndOperationToIndividualLiquidityReward
  :: [LiquidityRewardAndOperation]
  -> [IndividualLiquidityReward]
liquidityRewardAndOperationToIndividualLiquidityReward xs =  
  let m =
        foldr
        (\x acc ->
        Map.insertWith
          (\(accXtz, accTok) (newXtz, newTok) -> (accXtz + newXtz, accTok + newTok))
          (lroDexterContractId x)
          ( fromMaybe 0 . readMay . T.unpack . lroXtzReward $ x
          , fromMaybe 0 . readMay . T.unpack . lroTokenReward $ x
          )
          acc
        )
        (Map.empty :: Map.Map Text (Integer, Integer))
        xs
  in (\(dexterContractId, (xtzReward, tokReward)) ->
        IndividualLiquidityReward
          dexterContractId
          (T.pack . show $ xtzReward)
          (T.pack . show $ tokReward)  
        ) <$> Map.toList m
      
calculateCumulativeLiquidityReward
  :: [DexterOperation]
  -> Map.Map Text (Natural, Natural) -- (XTZ, token)
  -> CumulativeLiquidityReward
calculateCumulativeLiquidityReward dexterOperations poolsMap =
  let intermediateValues =
        foldr
        parseIntermediate
        Map.empty
        dexterOperations :: Map.Map Text Intermediate
      xs =
        Map.fromList $
        fmap
        (\(x,i) ->
           let tokenReward = iTokenReward i
               tokenPool   = maybe 0 snd $ Map.lookup (iDexterContractId i) poolsMap
               xtzPool     = maybe 0 fst $ Map.lookup (iDexterContractId i) poolsMap
           in
             ( x
             , CumulativeTokenLiquidityReward
               { ctlrDexterContractId = iDexterContractId i
               , ctlrXtzReward        = (iXtzReward i)
               , ctlrTokenReward      = tokenReward
               , ctlrTokenRewardInXtz = tokenToMutez tokenReward xtzPool tokenPool
               , ctlrTokenPool        = tokenPool
               , ctlrXtzPool          = xtzPool
               }
             )
        )
        (Map.toList intermediateValues)
      totalRewards =
        foldr (\z accum -> accum + (ctlrXtzReward z) + (ctlrTokenRewardInXtz z)) 0 (Map.elems xs)
  in CumulativeLiquidityReward
       { clrCumululativeXtzReward = totalRewards
       , clrTokenRewards          = xs
       }

-- | This is an intermediate representation of CumulativeTokenLiquidityReward
-- to help build the complete version. Internal module use only
data Intermediate =
  Intermediate
    { iDexterContractId :: Text
    , iXtzReward        :: Natural
    , iTokenReward      :: Natural
    } deriving (Eq,Show,Generic)

-- | Add the intermediate representation of CumulativeTokenLiquidityReward from
-- a DexterOperation and add it to a map of DexterContractId to Intermediate.
-- Internal module use only.
parseIntermediate :: DexterOperation -> Map.Map Text Intermediate -> Map.Map Text Intermediate
parseIntermediate dexterOperation xs =
  case parseDexterTransaction dexterOperation of
    Left _ -> xs
    Right dexterTransaction ->
      case dexterTransaction of
        T.XtzToToken tr ->
          Map.alter
          (\case
              Just x ->
                Just $
                Intermediate
                (iDexterContractId x)
                ((iXtzReward x) + (calculatePoolReward . fromIntegral . unMutez $ T.xtzSold (tr :: T.XtzToTokenTransaction)))
                (iTokenReward x)                 
              Nothing ->
                Just $
                Intermediate
                (doDexterContractId dexterOperation)
                (calculatePoolReward . fromIntegral . unMutez $ T.xtzSold (tr :: T.XtzToTokenTransaction))
                0
          ) (doDexterContractId dexterOperation) xs

        T.TokenToXtz tr ->
          Map.alter
          (\case
              Just x ->
                Just $
                Intermediate
                (iDexterContractId x)
                (iXtzReward x)
                ((iTokenReward x) + (calculatePoolReward . fromIntegral . unBignum $ T.tokensSold (tr :: T.TokenToXtzTransaction)))
              Nothing ->
                Just $
                Intermediate
                (doDexterContractId dexterOperation)
                0
                (calculatePoolReward . fromIntegral . unBignum $ T.tokensSold (tr :: T.TokenToXtzTransaction))
          ) (doDexterContractId dexterOperation) xs

        T.TokenToToken tr ->
          -- update token amount in source
          Map.alter
          (\case
              Just x ->
                Just $
                Intermediate
                (iDexterContractId x)
                (iXtzReward x)
                ((iTokenReward x) + (calculatePoolReward . fromIntegral . unBignum $ T.tokensSold (tr :: T.TokenToTokenTransaction)))
              Nothing ->
                Just $
                Intermediate
                (doDexterContractId dexterOperation)
                0
                (calculatePoolReward . fromIntegral . unBignum $ T.tokensSold (tr :: T.TokenToTokenTransaction))
          )
          (doDexterContractId dexterOperation)
          -- update xtz amount in target
          $ 
          Map.alter
          (\case
              Just x ->
                Just $
                Intermediate
                (iDexterContractId x)
                (iXtzReward x)
                ((iTokenReward x) + (calculatePoolReward . fromIntegral . unMutez $ T.internalXtzSold (tr :: T.TokenToTokenTransaction)))
              Nothing ->
                Just $
                Intermediate
                (T.outputDexterContract tr)
                (calculatePoolReward . fromIntegral . unMutez $ T.internalXtzSold (tr :: T.TokenToTokenTransaction))
                0
          )
          (T.outputDexterContract tr) xs
          
        T.AddLiquidity _ -> xs
        T.RemoveLiquidity _ -> xs
