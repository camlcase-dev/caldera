{-|
Module      : Caldera.API.Types.Transaction
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

The payload for Dexter transactions that get passed to the frontend.
Used for monitoring the history of Dexter transactions.

-}

{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Caldera.API.Types.Transaction where

import qualified Data.Map as Map
import Data.Aeson
import Data.Maybe   (fromMaybe)
import Data.Text    (Text)
import Data.Time    (LocalTime)
import GHC.Generics (Generic)
import GHC.Natural  (Natural)

-- tezos
import Tezos.Client.Types (unBignum, unMutez)

-- caldera
import Caldera.Database.Table.DexterOperation
import Caldera.Database.Query.DexterOperation
import qualified Dexter.Exchange.Transaction as T

-- | Convert a dexter operation into a transaction
parseTransaction :: Map.Map Text Text -> DexterOperation -> Maybe Transaction
parseTransaction contractToSymbol dexterOperation =
  case parseDexterTransaction dexterOperation of
    Left _ -> Nothing
    Right dexterTransaction ->
      case dexterTransaction of
        T.XtzToToken tr ->
          Just . TransactionExchange $
          Exchange
            (T.to_ (tr :: T.XtzToTokenTransaction))
            "XTZ"
            (fromMaybe "" $ Map.lookup (T.contract (tr :: T.XtzToTokenTransaction)) contractToSymbol)
            (fromIntegral . unMutez . T.xtzSold $ (tr :: T.XtzToTokenTransaction))
            (fromIntegral . unBignum $ T.totalTokensBought (tr :: T.XtzToTokenTransaction))
            (doTimestamp dexterOperation)
            
        T.TokenToXtz tr ->
          Just . TransactionExchange $
          Exchange
            (T.to_ (tr :: T.TokenToXtzTransaction))
            (fromMaybe "" $ Map.lookup (T.contract (tr :: T.TokenToXtzTransaction)) contractToSymbol)
            "XTZ"
            (fromIntegral . unBignum $ T.tokensSold (tr :: T.TokenToXtzTransaction))            
            (fromIntegral . unMutez . T.totalXtzBought $ (tr :: T.TokenToXtzTransaction))
            (doTimestamp dexterOperation)

        T.TokenToToken tr ->
          Just . TransactionExchange $
          Exchange
            (T.to_ (tr :: T.TokenToTokenTransaction))
            (fromMaybe "" $ Map.lookup (T.contract (tr :: T.TokenToTokenTransaction)) contractToSymbol)
            (fromMaybe "" $ Map.lookup (T.outputDexterContract tr) contractToSymbol)
            (fromIntegral . unBignum $ T.tokensSold (tr :: T.TokenToTokenTransaction))
            (fromIntegral . unBignum $ T.totalTokensBought (tr :: T.TokenToTokenTransaction))
            (doTimestamp dexterOperation)

        T.AddLiquidity tr ->
          Just . TransactionAddLiquidity $
          AddLiquidity
            (fromMaybe "" $ Map.lookup (T.contract (tr :: T.AddLiquidityTransaction)) contractToSymbol)
            (fromIntegral . unMutez . T.xtzDeposited $ (tr :: T.AddLiquidityTransaction))
            (fromIntegral . unBignum $ T.totalTokensDeposited (tr :: T.AddLiquidityTransaction))
            (doTimestamp dexterOperation)

        T.RemoveLiquidity tr ->
          Just . TransactionRemoveLiquidity $
          RemoveLiquidity
            (fromMaybe "" $ Map.lookup (T.contract (tr :: T.RemoveLiquidityTransaction)) contractToSymbol)
            (fromIntegral . unBignum $ T.lqtBurned (tr :: T.RemoveLiquidityTransaction))            
            (fromIntegral . unMutez $ T.totalXtzWithdrawn (tr :: T.RemoveLiquidityTransaction))
            (fromIntegral . unBignum $ T.totalTokensWithdrawn (tr :: T.RemoveLiquidityTransaction))
            (doTimestamp dexterOperation)

-- | One of the five kinds of Dexter transactions (entrypoints) we monitor.
data Transaction
  = TransactionExchange Exchange
  | TransactionAddLiquidity AddLiquidity
  | TransactionRemoveLiquidity RemoveLiquidity
  deriving (Eq,Show,Generic)

instance ToJSON Transaction
instance FromJSON Transaction

-- | Represents one of the three exchange types: tokenToToken, tokenToXtz,
-- and xtzToToken.
data Exchange =
  Exchange
    { eDestination :: Text
    , eSymbolIn    :: Text
    , eSymbolOut   :: Text    
    , eValueIn     :: Natural
    , eValueOut    :: Natural
    , eTimestamp   :: LocalTime
    } deriving (Eq,Show,Generic)

instance ToJSON Exchange where
  toJSON e =
    object
      [ ("destination" .= eDestination e)
      , ("symbolIn"    .= eSymbolIn e)
      , ("symbolOut"   .= eSymbolOut e)
      , ("valueIn"     .= eValueIn e)
      , ("valueOut"    .= eValueOut e)
      , ("timestamp"   .= eTimestamp e)
      ]

instance FromJSON Exchange where
  parseJSON = withObject "Exchange" $ \o ->
    Exchange <$> o .: "destination"
             <*> o .: "valueIn"
             <*> o .: "valueOut"
             <*> o .: "symbolIn"
             <*> o .: "symbolOut"
             <*> o .: "timestamp"

-- | An addLiquidity transaction
data AddLiquidity =
  AddLiquidity
    { alSymbol  :: Text
    , alTokenIn :: Natural
    , alXtzIn   :: Natural
    , alTimestamp :: LocalTime  
    } deriving (Eq,Show,Generic)

instance ToJSON AddLiquidity where
  toJSON al =
    object
      [ ("symbol"    .= alSymbol al)
      , ("tokenIn"   .= alTokenIn al)
      , ("xtzIn"     .= alXtzIn al)
      , ("timestamp" .= alTimestamp al)      
      ]

instance FromJSON AddLiquidity where
  parseJSON = withObject "AddLiquidity" $ \o ->
    AddLiquidity <$> o .: "symbol"
                 <*> o .: "tokenIn"
                 <*> o .: "xtzIn"
                 <*> o .: "timestamp"

-- | A removeLiquidity transaction
data RemoveLiquidity =
  RemoveLiquidity
    { rlSymbol    :: Text
    , rlLqtBurned :: Natural
    , rlTokenOut  :: Natural
    , rlXtzOut    :: Natural
    , rlTimestamp   :: LocalTime    
    } deriving (Eq,Show,Generic)

instance ToJSON RemoveLiquidity where
  toJSON rl =
    object
      [ ("symbol"    .= rlSymbol rl)
      , ("lqtBurned" .= rlLqtBurned rl)
      , ("tokenOut"  .= rlTokenOut rl)
      , ("xtzOut"    .= rlXtzOut rl)
      , ("timestamp" .= rlTimestamp rl)      
      ]

instance FromJSON RemoveLiquidity where
  parseJSON = withObject "RemoveLiquidity" $ \o ->
    RemoveLiquidity <$> o .: "symbol"
                    <*> o .: "lqtBurned"
                    <*> o .: "tokenOut"
                    <*> o .: "xtzOut"
                    <*> o .: "timestamp"
