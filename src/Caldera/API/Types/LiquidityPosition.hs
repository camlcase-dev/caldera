{-|
Module      : Caldera.API.Types.LiquidityPosition
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

-}

{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Caldera.API.Types.LiquidityPosition
  ( IndividualLiquidityPosition (..)
  , lpoToIndividualLiquiditPosition
  ) where

import Caldera.Database.Table.LiquidityPositionAndOperation
import Control.Lens ((&), (?~), mapped)
import Data.Aeson
import Data.Swagger
import Data.Text
import GHC.Generics (Generic)

-- | The liquidity position of an account in regards to a Dexter contract
data IndividualLiquidityPosition =
  IndividualLiquidityPosition
    { ilpAccountId        :: Text
    , ilpLqt              :: Text
    , ilpDexterContractId :: Text
    } deriving (Eq, Generic, Show)

instance ToJSON IndividualLiquidityPosition where
  toJSON IndividualLiquidityPosition{..} =
    object
      [ "accountId" .= ilpAccountId
      , "lqt"       .= ilpLqt
      , "dexterContractId" .= ilpDexterContractId
      ]

instance ToSchema IndividualLiquidityPosition where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "The liquidity position of an account in regards to a Dexter contract"
    & mapped.schema.example ?~
    toJSON
    (IndividualLiquidityPosition
      { ilpAccountId        = "tz1fSkEwBCgTLas8Y82SYpEGW9aFZPBag8uY"
      , ilpLqt              = "4658528"
      , ilpDexterContractId = "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N"
      }
    )

lpoToIndividualLiquiditPosition
  :: LiquidityPositionAndOperation
  -> IndividualLiquidityPosition
lpoToIndividualLiquiditPosition lpo =
  IndividualLiquidityPosition
    { ilpAccountId        = lpoAccountId lpo
    , ilpLqt              = lpoLqt lpo
    , ilpDexterContractId = lpoDexterContractId lpo
    }
