{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}

module Caldera.API.Liquidity(
    getLiquidityAtInterval
  , getLiquidityLatest
  , getLiquidityAtIntervalForContract
  , getLiquidityLatestAtContract
  ) where

-- base
import qualified Data.Cache as Cache
import qualified Data.Text as T
import Data.Functor ( (<&>) )
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Time ( addDays, Day )
import Data.Time.Clock(getCurrentTime, utctDay)
import Data.Either (rights, lefts)
import qualified Data.Set as S

-- database
import Database.PostgreSQL.Simple (Connection)

-- caldera
import qualified Caldera.API.Types as Types
import Caldera.Database.Table.DexterContractAndOperation
import qualified Caldera.Database.Query.DexterContractAndOperation as DCO
import Caldera.Database (catchSqlErrors)

getLiquidityAtInterval
  :: Cache.Cache (T.Text, T.Text) Types.Liquidity
  -> T.Text
  -> IO (Either String [[Types.Liquidity]])
getLiquidityAtInterval cache interval = do
  case interval of
    "1w" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = getLastXDays 7 currentDay
      maybeLiquidities <- mapM (getLiquidityFromCache cache) days
      pure $ Right (map catMaybes maybeLiquidities)
    "1M" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = getLastXDays 30 currentDay
      maybeLiquidities <- mapM (getLiquidityFromCache cache) days
      pure $ Right (map catMaybes maybeLiquidities)
    "1y" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = getLastXDays 365 currentDay
      maybeLiquidities <- mapM (getLiquidityFromCache cache) days
      pure $ Right (map catMaybes maybeLiquidities)
    "all" -> do
      -- TODO: We need to figure out an efficient way to get all data. For now this is set at 1y because dexter is not 1y old. 
      currentDay <- getCurrentTime <&> utctDay
      let days = getLastXDays 365 currentDay
      maybeLiquidities <- mapM (getLiquidityFromCache cache) days
      pure $ Right (map catMaybes maybeLiquidities)
    _ -> pure $ Left "Unsupported interval."

getLiquidityAtIntervalForContract
  :: Cache.Cache (T.Text, T.Text) Types.Liquidity
  -> T.Text
  -> T.Text -> IO (Either String [Types.Liquidity])
  
getLiquidityAtIntervalForContract cache contract interval = do
  case interval of
    "1w" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = getLastXDays 7 currentDay
      maybeLiquidities <- mapM (getLiquidityFromCacheByContract cache contract . T.pack . show ) days
      pure $ Right (catMaybes maybeLiquidities)
    "1M" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = getLastXDays 30 currentDay
      maybeLiquidities <- mapM (getLiquidityFromCacheByContract cache contract . T.pack . show) days
      pure $ Right (catMaybes maybeLiquidities)
    "1y" -> do
      currentDay <- getCurrentTime <&> utctDay
      let days = getLastXDays 365 currentDay
      maybeLiquidities <- mapM (getLiquidityFromCacheByContract cache contract . T.pack . show) days
      pure $ Right (catMaybes maybeLiquidities)
    "all" -> do
      -- TODO: We need to figure out an efficient way to get all data. For now this is set at 1y because dexter is not 1y old. 
      currentDay <- getCurrentTime <&> utctDay
      let days = getLastXDays 365 currentDay
      maybeLiquidities <- mapM (getLiquidityFromCacheByContract cache contract . T.pack . show) days
      pure $ Right (catMaybes maybeLiquidities)
    _ -> pure $ Left "Unsupported interval."

getLiquidityLatest
  :: Connection
  -> Cache.Cache (T.Text, T.Text) Types.Liquidity
  -> IO (Either String [Types.Liquidity])
getLiquidityLatest db cache = do
  keys <- Cache.keys cache
  let contractIDs = mkUniq $ map fst keys
  liquidities <-  mapM (getLiquidityLatestAtContract db cache) contractIDs
  let errors = lefts liquidities
  if null errors
    then pure $ Right (rights liquidities) 
    else pure $ Left (head errors)
  

getLiquidityLatestAtContract
  :: Connection
  -> Cache.Cache (T.Text, T.Text) Types.Liquidity
  -> Text
  -> IO (Either String Types.Liquidity)
getLiquidityLatestAtContract db cache contractId = do
  maybeLiquidity <- getLiquidityFromCacheByContract cache contractId "current"
  case maybeLiquidity of
    Nothing -> do
      eitherContractStorage <- catchSqlErrors $ DCO.selectLatestForContract db contractId
      currentDay <- getCurrentTime <&> utctDay
      case eitherContractStorage of
        Left err -> pure $ Left err
        Right Nothing -> pure . Left $ "No DexterOperation"
        Right (Just storage) -> 
          pure . Right $ contractToLiquidity currentDay storage
    Just liquidity -> pure . Right $ liquidity

contractToLiquidity
  :: Day
  -> DexterContractAndOperation
  -> Types.Liquidity
contractToLiquidity day do_ =
  Types.Liquidity
    { liquidityName      = "XTZ/" <> dcoTokenSymbol do_
    , liquidityDecimals  = fromIntegral $ dcoDecimals do_
    , liquidityDay       = T.pack $ show day    
    , liquidityContract  = dcoDexterContractId do_
    , liquidityLqtTotal  = dcoLqtTotal do_
    , liquidityTokenPool = dcoTokenPool do_
    , liquidityXtzPool   = dcoXtzPool do_
    }
    
-- | Gets the last (x) amount of days
getLastXDays :: Integer -> Day -> [Day]
getLastXDays x day =
  if x == 0
  then []
  else do
    let next = addDays (-x) day
    next:getLastXDays (x-1) day

getLiquidityFromCache :: Cache.Cache (T.Text, T.Text) Types.Liquidity -> Day -> IO [Maybe Types.Liquidity]
getLiquidityFromCache cache day = do
  keys <- Cache.keys cache
  let contractIDs = mkUniq $ map fst keys
  mapM (getLiquidityFromCacheByContract' cache day) contractIDs

getLiquidityFromCacheByContract ::  Cache.Cache (T.Text, T.Text) Types.Liquidity -> T.Text -> T.Text  -> IO (Maybe Types.Liquidity)
getLiquidityFromCacheByContract cache contract key = do
    maybeLiquidity <- Cache.lookup cache (contract, key)
    case maybeLiquidity of
        Nothing -> pure Nothing
        Just liquidity -> return $ Just liquidity

getLiquidityFromCacheByContract' :: Cache.Cache (T.Text, T.Text) Types.Liquidity  -> Day -> T.Text -> IO (Maybe Types.Liquidity)
getLiquidityFromCacheByContract' cache day contract = getLiquidityFromCacheByContract cache contract (T.pack $ show day)

mkUniq :: Ord a => [a] -> [a]
mkUniq = S.toList . S.fromList