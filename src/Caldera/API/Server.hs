{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module Caldera.API.Server
  ( server
  ) where

import qualified Control.Concurrent.STM.TChan as Tchan
import qualified Data.Map.Strict as M
import qualified Data.Text as T
import qualified Logging.Logger as L
import Control.Monad.IO.Class (liftIO)
import Control.Monad.STM (atomically)
import Data.Aeson (encode)
import Data.Maybe (catMaybes,fromMaybe)
import Servant
    ( throwError,
      err404,
      err500,
      type (:<|>)((:<|>)),
      Server,
      ServerError(errBody) )
import Servant.Swagger.UI

-- cache
import qualified Data.Cache as Cache

-- database
import Database.PostgreSQL.Simple (Connection)

-- caldera
import Caldera.Database (catchSqlErrors)
import qualified Caldera.Database.Query.DexterContract as DC
import qualified Caldera.Database.Query.DexterOperation as DO
import qualified Caldera.Database.Query.DexterContractAndOperation as DCO
import qualified Caldera.Database.Query.LiquidityPositionAndOperation as LPO
import qualified Caldera.Database.Query.LiquidityRewardAndOperation as LRO
import qualified Caldera.API.Liquidity as LiquidityAPI
import qualified Caldera.API.Prices as PricesAPI
import qualified Caldera.API.Types.LiquidityPosition as LP
import qualified Caldera.API.Types.LiquidityReward as LR
import qualified Caldera.API.Types.Transaction as Transaction
import qualified Caldera.API.Types as Types
import qualified Caldera.API.Volume as VolumeAPI
import Caldera.Database.Table.DexterContract
import Caldera.Database.Table.DexterOperation
import Caldera.API.Routes (API, calderaSwagger)
import Safe (readMay)

server
  :: Tchan.TChan L.LogMsg
  -> Connection
  -> Cache.Cache (T.Text, T.Text) Types.Liquidity
  -> Cache.Cache (T.Text, T.Text) Types.Volume
  -> Server API
server logchan conn liquidityCache volumeCache
  = (swaggerSchemaUIServer calderaSwagger) :<|>
  (    contractsH
  :<|> contractsContractIDGetH
  :<|> pricesVsCurrencyIDGetH
  :<|> liquidityRewardsContractIdGetCurrentH
  :<|> getLiquidityRewardsAccountH
  :<|> getLiquidityRewardsH
  :<|> liquidityGetAccountCurrentH
  :<|> liquidityGetH
  :<|> liquidityGetCurrentH
  :<|> liquidityContractIDGetH
  :<|> liquidityContractIDGetCurrentH  
  :<|> transactionsAccountH
  :<|> transactionsH
  :<|> volumeGetH
  :<|> volumeGetCurrentH
  :<|> volumeContractIDGet
  :<|> volumeContractIDGetCurrentH
  )
      
  where
    contractsH _block = do
      case _block of
        Nothing -> do
          let logMap = Just $ M.fromList [("path", "/contracts")]
          liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}
          eitherStorage <- liftIO $ DCO.selectLatest conn
          case eitherStorage of
            Left err -> throwError err500 { errBody = encode (T.pack err) }
            Right storage -> pure $ fmap DCO.toContract storage
      
        Just block -> do
          let logMap = Just $ M.fromList [("path", "/contracts"), ("block", block)]
          liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}
          eitherStorage <- liftIO $ DCO.selectClosestBlockLevel conn $ fromMaybe 0 $ readMay $ T.unpack block
          case eitherStorage of
            Left err -> throwError err500 { errBody = encode (T.pack err) }
            Right storage -> pure $ fmap DCO.toContract storage

    contractsContractIDGetH _contractId _block = do 
      case _block of
        Nothing -> do
          let logMap = Just $ M.fromList [("path", "/contracts"), ("contract", _contractId)]
          liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}
          eitherStorage <- liftIO $ catchSqlErrors $ DCO.selectLatestForContract conn _contractId
          case eitherStorage of 
            Left err -> do
              if err == "Not Found"
                then do
                  let logMap' = Just $ M.fromList [("path", "/contracts"), ("contract", _contractId)]
                  liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap', L.msg = "Failed to find resource for received request"}
                  throwError err404
                else throwError err500 { errBody = encode (T.pack err) }
            Right Nothing -> throwError err404
            Right (Just storage) -> pure $ DCO.toContract storage
        Just block -> do
          let logMap = Just $ M.fromList [("path", "/contracts"), ("contract", _contractId), ("block", block)]
          liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}
          eitherStorage <-
            liftIO
            $ catchSqlErrors
            $ DCO.selectClosestBlockLevelForContract conn (fromMaybe 0 $ readMay $ T.unpack block)
            _contractId
          case eitherStorage of 
            Left err -> if err == "Not Found"
                          then do
                            let logMap' = Just $ M.fromList [("path", "/contracts"), ("contract", _contractId), ("block", block)]
                            liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap', L.msg = "Failed to find resource for received request"}
                            throwError err404
                          else throwError err500 { errBody = encode (T.pack err) }
            Right Nothing -> throwError err404
            Right (Just storage) -> pure $ DCO.toContract storage

    liquidityRewardsContractIdGetCurrentH dexterContractId = do
      -- confirm that exchange exists for token symbol
      mTokenContract <- liftIO $ DC.getContractByDexterContractId conn dexterContractId
      tokenContract  <- maybe (throwError err404) pure mTokenContract
      
      -- get trade DexterOperation for tokenSymbol for the last 24 hours
      dexterOperations <- liftIO $ DO.selectContractTradesLast24Hours conn (dcDexterContractId tokenContract)

      -- parse ExchangeLiquidityReward out of relevant DexterOperations
      pure . catMaybes $ fmap (LR.parseExchangeLiquidityReward dexterContractId) dexterOperations

    pricesVsCurrencyIDGetH vsCurrency intervalString = do
      let timeframe =
            maybe
              (PricesAPI.OneWeek)
              (fromMaybe PricesAPI.OneWeek . PricesAPI.parseInterval)
              intervalString
          logMap = Just $ M.fromList [("path", "/prices"), ("vsCurrency", vsCurrency), ("timeframe", T.pack $ show timeframe)]

      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}

      liftIO $ PricesAPI.getPricesAtInterval conn timeframe vsCurrency

    liquidityGetAccountCurrentH accountId = do
      dexterContracts <- liftIO $ DC.selectAll conn

      mPositions <-
        mapM
        (liftIO . LPO.selectMostRecentPositionForAccount conn accountId)
        (fmap dcDexterContractId dexterContracts)

      pure $ fmap LP.lpoToIndividualLiquiditPosition $ catMaybes mPositions
      
          
    liquidityGetH  _timeframe = do
      let timeframe = fromMaybe "1w" _timeframe
          logMap = Just $ M.fromList [("path", "/liquidity"), ("timeframe", timeframe)]
      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}

      eitherLiquidity <- liftIO $  LiquidityAPI.getLiquidityAtInterval liquidityCache timeframe
      case eitherLiquidity of 
        Left err -> throwError err500 { errBody = encode (T.pack err) }
        Right liquidity -> pure $ filter (not . null) liquidity

    liquidityGetCurrentH = do
      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = Nothing , L.msg = "Received request"}
      eitherLiquidity <- liftIO $ LiquidityAPI.getLiquidityLatest conn liquidityCache
      case eitherLiquidity of 
        Left err -> throwError err500 { errBody = encode (T.pack err) }
        Right liquidity -> pure liquidity
    
    liquidityContractIDGetH _contractId _timeframe = do
      let timeframe = fromMaybe "1w" _timeframe
          logMap = Just $ M.fromList [("path", "/liquidity"), ("contract", _contractId), ("timeframe", timeframe)]

      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}

      eitherLiquidity <- liftIO $  LiquidityAPI.getLiquidityAtIntervalForContract liquidityCache _contractId timeframe
      case eitherLiquidity of 
        Left err -> throwError err500 { errBody = encode (T.pack err) }
        Right liquidity -> pure liquidity

    liquidityContractIDGetCurrentH _contractId = do
      let logMap = Just $ M.fromList [("path", "/liquidity"), ("contract", _contractId)]
      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}

      eitherLiquidity <- liftIO $ LiquidityAPI.getLiquidityLatestAtContract conn liquidityCache _contractId
      case eitherLiquidity of 
        Left err -> throwError err500 { errBody = encode (T.pack err) }
        Right liquidity -> pure liquidity

    transactionsAccountH accountId = do
      operations <- liftIO $ DO.selectSourceTransactions conn accountId
      pure . length $ operations

    -- get all dexter contract transactions for the last 24 hours
    transactionsH = do
      -- get lookup map
      contractToSymbol <- liftIO $ DC.getContractToSymbolMap conn
      -- get operations
      operations <- liftIO $ DO.selectLast24Hours conn
      -- convert to transactions
      pure . length . catMaybes $ fmap (Transaction.parseTransaction contractToSymbol) operations      

    getLiquidityRewardsAccountH address =
      fmap LR.liquidityRewardAndOperationToIndividualLiquidityReward
      $ liftIO $ LRO.selectForAccount conn address
    
    getLiquidityRewardsH = do
      -- get all trade operations that incur rewards
      ops <- liftIO $ DO.selectAllTrades conn
      -- get all contarcts
      dexterContracts <- liftIO $ DC.selectAll conn
      -- get latest operation for each contract to get the xtzPool and tokenPool values
      latestDexterOperations <-
        fmap catMaybes $ liftIO $
        mapM (DO.selectLatestByContractId conn) (fmap dcDexterContractId dexterContracts)
      -- create map of DexterContractId to (xtzPool,lqtPool)
      let dexterToPools =
            M.fromList $
            fmap
            (\dop ->
               ( doDexterContractId dop
               , ( fromMaybe 0 . readMay . T.unpack . doXtzPool $ dop
                 , fromMaybe 0 . readMay . T.unpack . doTokenPool $ dop)
               )
            )
            latestDexterOperations
            
      pure $ LR.calculateCumulativeLiquidityReward ops dexterToPools
      
    volumeGetH  _timeframe = do
      let timeframe = fromMaybe "1w" _timeframe
          logMap = Just $ M.fromList [("path", "/volume"), ("timeframe", timeframe)]

      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}

      eitherVolume <- liftIO $ VolumeAPI.getVolumeAtInterval volumeCache timeframe
      case eitherVolume of 
        Left err -> throwError err500 { errBody = encode (T.pack err) }
        Right volume -> pure $ filter (not . null) volume

    volumeGetCurrentH = do
      let logMap = Just $ M.fromList [("path", "/volume/current")]
      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}
      maybeVolumes <- liftIO $ VolumeAPI.getVolumeLatest logchan volumeCache conn
      case maybeVolumes of
        Nothing -> throwError err500 { errBody = encode (T.pack "failed to get current volume") }
        Just volumes -> pure volumes
    
    volumeContractIDGet _contractId _timeframe = do
      let timeframe = fromMaybe "1w" _timeframe
          logMap = Just $ M.fromList [("path", "/volume/current"), ("contract", _contractId), ("timeframe", timeframe)]

      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}

      eitherVolume <- liftIO $  VolumeAPI.getVolumeAtIntervalForContract volumeCache _contractId timeframe 
      case eitherVolume of 
        Left err -> throwError err500 { errBody = encode (T.pack err) }
        Right volume -> pure volume

    volumeContractIDGetCurrentH _contractId = do
      let logMap = Just $ M.fromList [("path", "/volume/current"), ("contract", _contractId)]
      liftIO $ atomically $ Tchan.writeTChan logchan L.LogMsg { L.level = L.INFO, L.vals = logMap, L.msg = "Received request"}
      liftIO $ VolumeAPI.getVolumeLatestAtContract logchan volumeCache conn _contractId

