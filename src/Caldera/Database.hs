{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Caldera.Database(
    CalderaDb(..)
  , calderaDb
  , catchSqlErrors
  ) where

import Caldera.Database.Table.DailyXtzPrice
import Caldera.Database.Table.DexterContract
import Caldera.Database.Table.DexterOperation
import Caldera.Database.Table.LiquidityPosition
import Caldera.Database.Table.LiquidityReward
import Caldera.Database.Table.OperationBlock
import Database.Beam
import Database.Beam.Backend.URI (BeamResourceNotFound, BeamOpenURIInvalid)
import Database.Beam.Backend.SQL.Row (BeamRowReadError)
  
import Control.Exception (Handler(Handler), catches)
import Database.PostgreSQL.Simple (SqlError, FormatError, QueryError, ResultError)
import Database.PostgreSQL.Simple.Errors (ConstraintViolation)
import Database.PostgreSQL.Simple.Ok (ManyErrors)
  
data CalderaDb f =
  CalderaDb
    { calderaDailyXtzPrice          :: f (TableEntity DailyXtzPriceT)
    , calderaDexterContract         :: f (TableEntity DexterContractT)
    , calderaDexterOperation        :: f (TableEntity DexterOperationT)
    , calderaLiquidityPosition      :: f (TableEntity LiquidityPositionT)
    , calderaLiquidityReward        :: f (TableEntity LiquidityRewardT)    
    , calderaOperationBlock         :: f (TableEntity OperationBlockT)
    } deriving (Generic, Database be)

calderaDb :: DatabaseSettings be CalderaDb
calderaDb = defaultDbSettings

catchSqlErrors :: IO a -> IO (Either String a)
catchSqlErrors run =
  fmap Right run
    `catches`
    [ Handler (\(ex :: FormatError) -> pure $ Left $ show ex) 
    , Handler (\(ex :: SqlError) -> pure $ Left $ show ex)
    , Handler (\(ex :: BeamResourceNotFound) -> pure $ Left $ show ex)
    , Handler (\(ex :: BeamOpenURIInvalid) -> pure $ Left $ show ex)
    , Handler (\(ex :: BeamRowReadError) -> pure $ Left $ show ex)
    , Handler (\(ex :: ManyErrors) -> pure $ Left $ show ex)
    , Handler (\(ex :: ResultError) -> pure $ Left $ show ex)
    , Handler (\(ex :: QueryError) -> pure $ Left $ show ex)
    , Handler (\(ex :: ConstraintViolation) -> pure $ Left $ show ex)    
    ] 
