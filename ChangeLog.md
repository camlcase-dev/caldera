# Changelog for caldera

# 0.22.0.0 -- 2021-04-08
* Add dependency swagger2, servant-swagger and servant-swagger-ui.
* Add system dependency liblzma-dev to Dockerfile.
* Server swagger file at /swagger.json and swagger UI at /swagger.
* Reduce LiquidityReward.Worker rerun time to wait a minute, five minutes was too long.
* Create an executable that outputs ./swagger.json for use by the swagger api.

# 0.21.0.0 -- 2021-04-05
* Delete Caldera.API.Cache.Price and pricesCache. The caching system was not efficient.
* Update getPricesAtInterval to use SQL queries instead of caching.
* Add Interval type to model the valid intervals for querying currency values.
* Update Caldera.Synchronizers.DailyPrices.Worker to only upsert everything.
  There was a broken edge case that never allowed previous values to get updated.
  Revisit a more efficient approach in the future.

# 0.20.0.0 -- 2021-03-25
* Restrict daily price query date to earliest 2021-02-20.
* Make DailyPrices.Worker less aggressive in terms of resources and no longer
  spawn a process per currency (this overloaded the PostgreSQL connections).
* Add route 'transactions/account/<:accountId>/current'.
* Add route 'liquidity/account/<address>/current'.
* Change route 'liquidity/rewards/account/<:address>' to 'liquidity/rewards/account/<:address>/current'
* Remove unused function getMarketChartRange.
* Add a new migration that cleans up daily_xtz_price table (removes missing
  column and index, adds UNIQUE (vs_currency, date) and index on
  (vs_currency, date)).
* Rename migration files.

# 0.19.0.0 -- 2021-03-22
* Band aid fix for performance: create a postgresql connection per thread.
* Replace `Cache.Cache T.Text (Cache.Cache T.Text Types.Prices)` with
  `MVar (Map.Map (T.Text, T.Text) Types.Prices)`.

# 0.18.0.0 -- 2021-03-18
* liquidity/rewards/account/<address> now returns a list of liquidity rewards in
  xtz and token for each dexter contract id.
* Update LTS to 16.31.

# 0.17.0.0 -- 2021-03-12
* Remove unused code Caldera.Synchronizers.LiquidityReward.Worker for reading and
  writing liquidity rewards for a single account to an excel file. (Can be retrieved
  from previous commit if needed in the future).
* Remove CalderaBackend type since it was unused.
* Initial implementation of the Liquidity Rewards worker.
* Add individual liquidity rewards route at liquidity/rewards/account/<address>

# 0.16.1.0 -- 2021-03-12
* Removed the cache within a cache.
* Memory profiling/improvements.

# 0.16.0.0 -- 2021-03-10
* Change the returned value of /transactions/current to the count of
  transactions in the last 24 hours.

# 0.15.2.0 -- 2021-03-05
* Support CORS for local development.

# 0.15.1.0 -- 2021-03-01
* Updated the operations worker start level to the earliest block of of the earliest originated liquidity contract.

# 0.15.0.0 -- 2021-03-01
* Updated the sync-config to point to the new contracts.

# 0.14.1.0 -- 2021-02-28
* Rename liquidity routes to follow our API patterns.

# 0.14.0.0 -- 2021-02-19
* Double quoted key fix in volume API cache lookup.

# 0.13.0.0 -- 2021-02-18
* Fix parameter parsing for xtzToToken in Edo.

# 0.12.0.0 -- 2021-02-18
* Fix parameter parsing for tokenToXtz in Edo.

# 0.11.0.0 -- 2021-02-17
* Added cacheing in the API Layer for /current endpoints.
* Removed the block cache in the synchronizer.

# 0.10.0.0 -- 2021-02-17
* Fix Edo storage parsing for dexter operations.

# 0.9.0.0 -- 2021-02-08
* Dexter contract storage decoding supports Edo.
* Fix encoding of Mutez doXtzPool into the database.
* Fix raw query DexterOperation.selectOperationsForDay.
* Add route /liquidity/rewards
* Add route /exchanges/<dexter-contract-id>/liquidity/rewards/recent
* Begin work on LiquidityReward worker, but not ready to run yet. Still need to resolve long term how it runs.
* Add LiquidityPosition tables.
* Add LiquidityReward table.

# 0.8.0.0 -- 2021-02-05
* Completely redesign and simplify the database while maintaining the same functionality (excluding any bugs).
* Drop ContractStorage worker. Now it is the responsibility of DexterOperation to hold contract information.

# 0.7.0.0 -- 2021-01-31
* Replace most of the raw PostgreSQL with the Haskell library beam.
* Split table and query code into two separate files.
* Update table names to match beam's expectations.
* Maintain the same names and types for all the file fields.
* Add primary keys to all of the tables as required by beam.
* Create a generic error catching function for sql: `catchSqlErrors`.
  Apply to all areas that were using `safe` queries. All logging should
  remain the same.
* Change database to_ fields to receiver because there were some
  incompatibilities with a final underscore in beam fields/columns.
* Add big map ids to the sync-config.yml.
* Add primary keys to the raw PostgreSQL queries that we have kept.

# 0.6.1.0 -- 2021-01-29
* Refactor code related to Dexter.
* Only store dexter transactions with an applied status.

# 0.6.0.0 -- 2020-10-30
* Repurpose caldera as a statistical server for the Tezos Dexter contracts.
  It depends on the TZKT API.
* Dexter.Contract.Storage parses storages values from Expression.

# 0.5.0.0 -- 2020-05-19
* Separate XTZ request errors. 404 means the user cannot request again.
  409 means something went wrong and they can try again.

# 0.4.0.0 -- 2020-05-19
* Updated Amount sent to fund request from 100 to 500.

# 0.3.0.0 -- 2020-05-19
* Improve command line arguments. Add config file path to arguments.
* Config file now requires a path to the tezos client.

# 0.2.0.0 -- 2020-05-18
* Write a simple server that allows the user to transfer XTZ to their address.
* Support CORS.
* Support TLS or http depending on config.
* Config for server and postgresql.

# 0.1.0.0 -- 2020-05-18
* Init the haskell project.
