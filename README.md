# caldera

Is an open source API for [dexter.exchange](https://dexter.exchange) that gives insights to normalized trading data such as prices, volume, and liquifity. 

## build

```
stack build
```

## Feature Roadmap

### Phase I
* Indexed dexter contract storage
* Indexed historic prices for dexter assets in multiple fiat and cryptocurrency pairs.
* Indexed historic volume for dexter assets in multiple fiat and cryptocurrency pairs.
* Indexed historic liquidity for dexter assets in multiple fiat and cryptocurrency pairs.

### Phase II
* Indexed staking rewards for liquidity providers
* Transaction history

### Phase III
* Value of participating addresses over time
* Profit and Loss assessments
* slippage