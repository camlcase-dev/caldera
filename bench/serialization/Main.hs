{-|
Module      : Main
Description : Benchmarks on key functions of the caldera worker
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

-}

module Main where

import Criterion.Main
import Data.Aeson
import Data.ByteString.Lazy as BS
import qualified Tezos.Client.Types.Block as Block

main :: IO ()
main = do
  addLiquidityFile    <- BS.readFile "test/block_1253664_addLiquidity.json"
  removeLiquidityFile <- BS.readFile "test/block_1255568_removeLiquidity.json"  
  xtzToTokenFile      <- BS.readFile "test/block_1257788_xtzToToken.json"
  tokenToXtzFile      <- BS.readFile "test/block_1256652_tokenToXtz.json"

  defaultMain
    [ bgroup "decode" 
      [ bench "decode block with addLiquidity" $
        whnf (eitherDecode' :: BS.ByteString -> Either String Block.Block) addLiquidityFile

      , bench "decode block with removeLiquidity" $
        whnf (eitherDecode' :: BS.ByteString -> Either String Block.Block) removeLiquidityFile 

      , bench "decode block with xtzToTokenLiquidity" $
        whnf (eitherDecode' :: BS.ByteString -> Either String Block.Block) xtzToTokenFile

      , bench "decode block with tokenToXtzLiquidity" $
        whnf (eitherDecode' :: BS.ByteString -> Either String Block.Block) tokenToXtzFile
      ]
    ]

-- main = defaultMain [
--     bench "readFile" $ nfIO (readFile "GoodReadFile.hs")
--   ]
