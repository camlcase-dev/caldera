CREATE TABLE liquidity_reward (
  liquidity_reward_id BIGSERIAL PRIMARY KEY,
  account_id          VARCHAR(36)  NOT NULL,
  xtz_reward          VARCHAR(100) NOT NULL,
  token_reward        VARCHAR(100) NOT NULL,
  dexter_operation_id INT8  NOT NULL,
  CONSTRAINT liquidity_reward_fk_dexter_operation FOREIGN KEY(dexter_operation_id) REFERENCES dexter_operation(dexter_operation_id),
  UNIQUE (account_id, dexter_operation_id)
);

COMMENT on table liquidity_reward is 'The amount of rewards (in XTZ) after a xtzToToken, tokenToXtz or tokenToToken operation if the account held liquidity.';

CREATE INDEX idx_liquidity_reward_account_id ON liquidity_reward(account_id);
CREATE INDEX idx_liquidity_reward_dexter_operation_id ON liquidity_reward(dexter_operation_id);

CREATE TABLE liquidity_position (
  liquidity_position_id BIGSERIAL PRIMARY KEY,
  account_id            VARCHAR(36)  NOT NULL,
  lqt                   VARCHAR(100) NOT NULL,
  dexter_operation_id INT8  NOT NULL,
  CONSTRAINT liquidity_position_fk_dexter_operation FOREIGN KEY(dexter_operation_id) REFERENCES dexter_operation(dexter_operation_id),
  UNIQUE (account_id, dexter_operation_id)
);

CREATE INDEX idx_liquidity_position_account_id ON liquidity_position(account_id);
CREATE INDEX idx_liquidity_position_dexter_operation_id ON liquidity_position(dexter_operation_id);

COMMENT on table liquidity_position is 'The amount of liquidity that a provide owns at the end of an operation. Only changes after an addLiquidity or removeLiquidity operation.';
COMMENT on column liquidity_position.liquidity_position_id is 'Autoincremented primary key. Comes from the db, not the block.';
COMMENT on column liquidity_position.account_id is 'Autoincremented primary key. Comes from the db, not the block.';
COMMENT on column liquidity_position.lqt is 'The amount of lqt held by account_id at this particular block_level.operation_x.operation_y.content_z';
COMMENT on column liquidity_position.dexter_operation_id is 'Foreign key to the dexter operation table.';
