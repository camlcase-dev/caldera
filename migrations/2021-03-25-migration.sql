/* the old daily price worker is storing way too much data.
 * we do not need anything before 2021-03-01, but let's have a buffer of two
 * days for timezone considerations
 */
DELETE FROM daily_xtz_price WHERE date < '2021-02-27';

/* we have simplified the worker code so the missing column is no longer necessary */
DROP INDEX idx_daily_xtz_price_missing;
ALTER TABLE daily_xtz_price DROP COLUMN missing;

/* there should only be one price per day. It makes sense to add a uniqueness constraint 
 * to daily_xtz_price so we prevent data repetitions. 
 */
ALTER TABLE daily_xtz_price ADD CONSTRAINT unique_daily_xtz_price_vs_currency_and_date UNIQUE (vs_currency,date);

/* we have a query that filters based on vs_currency and a data range */
CREATE INDEX idx_daily_xtz_price_vs_currency_and_date ON daily_xtz_price(vs_currency, date);
