CREATE TABLE dexter_contract (
  token_name         VARCHAR(100) NOT NULL,
  token_symbol       VARCHAR(100) NOT NULL,
  dexter_contract_id VARCHAR(36) PRIMARY KEY,
  token_contract_id  VARCHAR(36) NOT NULL,
  origination        INTEGER NOT NULL,
  big_map_id         INTEGER NOT NULL,
  decimals           INTEGER NOT NULL
);

CREATE INDEX idx_dexter_contract_dexter_contract_id ON dexter_contract(dexter_contract_id);
CREATE INDEX idx_dexter_contract_token_contract_id ON dexter_contract(token_contract_id);

COMMENT on table dexter_contract is 'Store static information about dexter contracts.';
COMMENT on column dexter_contract.token_name is 'The name of the dexter trading pair.';
COMMENT on column dexter_contract.dexter_contract_id is 'The KT1 address of the dexter contract this operation corresponds to.';
COMMENT on column dexter_contract.token_contract_id is 'The KT1 address of the token contract that this dexter operation interacts with.';
COMMENT on column dexter_contract.origination is 'The block level at which this contract was originated.';
COMMENT on column dexter_contract.big_map_id is 'The big map id of this dexter contract.';
COMMENT on column dexter_contract.decimals is 'The number of decimal points the corresponding dexter token has.';

CREATE TABLE dexter_operation (
  dexter_operation_id BIGSERIAL PRIMARY KEY,
  dexter_contract_id  VARCHAR(36) NOT NULL,
  token_contract_id   VARCHAR(36) NOT NULL, 
  block_level         INTEGER NOT NULL,
  block_hash          VARCHAR(100) NOT NULL,
  timestamp           TIMESTAMP NOT NULL,
  protocol            VARCHAR(100) NOT NULL,
  operation_x         INTEGER NOT NULL,
  operation_y         INTEGER NOT NULL,
  content_z           INTEGER NOT NULL,
  source              VARCHAR(100) NOT NULL,
  entrypoint          VARCHAR(100) NOT NULL,
  lqt_total           VARCHAR(100) NOT NULL,
  token_pool          VARCHAR(100) NOT NULL,
  xtz_pool            VARCHAR(100) NOT NULL,
  payload             JSON NOT NULL,
  UNIQUE (block_level, operation_x, operation_y, content_z)
);

CREATE INDEX idx_dexter_operation_dexter_contract_id ON dexter_operation(dexter_contract_id);
CREATE INDEX idx_dexter_operation_token_contract_id ON dexter_operation(token_contract_id);
CREATE INDEX idx_dexter_operation_block_level ON dexter_operation(block_level);
CREATE INDEX idx_dexter_operation_block_hash ON dexter_operation(block_hash);
CREATE INDEX idx_dexter_operation_source ON dexter_operation(source);
CREATE INDEX idx_dexter_operation_entrypoint ON dexter_operation(entrypoint);
CREATE INDEX idx_dexter_operation_timestamp ON dexter_operation(timestamp);

COMMENT on table dexter_operation is 'Store dexter operations from tezos node blocks.';
COMMENT on column dexter_operation.dexter_operation_id is 'Autoincremented primary key. Comes from the db, not the block.';
COMMENT on column dexter_operation.dexter_contract_id is 'The KT1 address of the dexter contract this operation corresponds to.';
COMMENT on column dexter_operation.token_contract_id is 'The KT1 address of the token contract that this dexter operation interacts with.';
COMMENT on column dexter_operation.block_level is 'Block level in which this dexter operation occurred.';
COMMENT on column dexter_operation.timestamp is 'Time at which the block was processed.';
COMMENT on column dexter_operation.protocol is 'Protocol with which the block was processed.';
COMMENT on column dexter_operation.operation_x is 'X index of this operation.';
COMMENT on column dexter_operation.operation_y is 'Y index of this operation.';
COMMENT on column dexter_operation.content_z is 'Z index of this operation.';
COMMENT on column dexter_operation.source is 'Address that called this.';
COMMENT on column dexter_operation.entrypoint is 'Dexter entrypoint that was called.';
COMMENT on column dexter_operation.lqt_total is 'Dexter storage lqt_total after this operation.';
COMMENT on column dexter_operation.token_pool is 'Dexter storage xtz_total after this operation.';
COMMENT on column dexter_operation.xtz_pool is 'Dexter storage token_pool after this operation.';
COMMENT on column dexter_operation.payload is 'The entire json payload at operations.x.y.contents.z for an applied operation.';


CREATE TABLE daily_xtz_price (
  daily_xtz_price_id BIGSERIAL PRIMARY KEY,
  vs_currency VARCHAR(20) NOT NULL,
  price FLOAT NOT NULL,
  date DATE NOT NULL,
  missing BOOLEAN NOT NULL
);

CREATE INDEX idx_daily_xtz_price_date ON daily_xtz_price(date);
CREATE INDEX idx_daily_xtz_price_missing ON daily_xtz_price(missing);
CREATE INDEX idx_daily_xtz_price_vs_currency ON daily_xtz_price(vs_currency);

COMMENT on table daily_xtz_price is 'XTZ to currency prices.';
COMMENT on column daily_xtz_price.daily_xtz_price_id is 'Autoincremented primary key. Comes from the db.';
COMMENT on column daily_xtz_price.vs_currency is 'The name of the currency XTZ is compared to.';
COMMENT on column daily_xtz_price.price is 'The value of XTZ in vs_currency.';
COMMENT on column daily_xtz_price.date is 'The calendar date at which the price is capture.';
COMMENT on column daily_xtz_price.missing is 'If true we have captured this information from the API, if false wedo not have this information.';

CREATE TABLE operation_block ( 
  block_level INTEGER NOT NULL PRIMARY KEY,
  missing BOOLEAN NOT NULL,
  timestamp TIMESTAMP NOT NULL
);

CREATE INDEX idx_operation_block_missing ON operation_block(missing);
CREATE INDEX idx_operation_block_timestamp ON operation_block(timestamp);

COMMENT on table operation_block is 'Block information to help synchronize all dexter operations from a Tezos node.';
COMMENT on column operation_block.block_level is 'The block level of a Tezos block.';
COMMENT on column operation_block.missing is 'If true we have processed its operations into dexter_operation, if false we have not.';
COMMENT on column operation_block.timestamp is 'If missing is true than the timestamp from the block, otherwise a dummy value.';
