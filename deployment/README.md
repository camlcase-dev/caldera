# Deploying Caldera With CI
## Dev

### Step I:
Because the Caldera API takes some time fill it's cache (~10m), it's important to deploy the stats maintenance page. To do this you can manually trigger the [caldera-frontend dev maintenance ci](https://app.buddy.works/camlcase/caldera-frontend/pipelines/pipeline/317496/executions).


### Step II:
When you are ready to deploy Caldera to Dev it is just a matter of merging into the `develop` branch. The CI will automatically build, tag, and push a docker image based off the version recorded in package.yaml. 

The CI will then deploy the API and the Synchronizer to `k8s-dev` in the `caldera-dev` namespace. You can verify the cache is filled by querying for the following endpoints and verifying they have no empty values: 

* https://dev.caldera.dexter.exchage/liquidity
* https://dev.caldera.dexter.exchange/volume

### Step III:
Once you have verified that the cache has been filled correctly you can retrigger the [caldera-frontend dev ci](https://app.buddy.works/camlcase/caldera-frontend/pipelines/pipeline/292789/executions). This will redeploy the latest commit on the dev branch and take down the maintenance page. 

### Verifying the CI Worked
If you notice something not working correctly you may need to check the pods in the k8s-dev to see if they are in a `Running` state. 

```
➜  caldera git:(deploy_docs) ✗ kubectl get pods -n caldera-dev
NAME                                      READY   STATUS    RESTARTS   AGE
caldera-api-deployment-74d6fdbdb7-ljm8l   0/1     Evicted   0          122m
caldera-api-deployment-74d6fdbdb7-zsbzm   1/1     Running   0          13m
caldera-sync-deployment-bbfcd9675-2wm72   1/1     Running   0          64m
caldera-sync-deployment-bbfcd9675-6qlln   0/1     Evicted   0          122m
```

You can then check the logs of a pod by running the following: 
```
kubectl logs -f caldera-sync-deployment-bbfcd9675-2wm72 -n caldera-dev
```

If you would like to get the logs of a previously crashed pod you can do the following:
```
kubectl logs -f --previous aldera-sync-deployment-bbfcd9675-2wm72 -n caldera-dev
```

### Rollback
If you are unable to solve the problem you will need to redo `Step I` and deploy the maintenance page for stats. Then you can run the [dev rollback](https://app.buddy.works/camlcase/caldera/pipelines/pipeline/317465/executions). Once you have successfully rolled back, you can redo `Step III`. 

## Prod

### Step I:
Because the Caldera API takes some time fill it's cache (~10m), it's important to deploy the stats maintenance page. To do this you can manually trigger the [caldera-frontend prod maintenance ci](https://app.buddy.works/camlcase/caldera-frontend/pipelines/pipeline/317499/executions).


### Step II:
When you are ready to deploy Caldera to Prod it is just a matter of merging into the `master` branch. The CI will automatically build, tag, and push a docker image based off the version recorded in package.yaml. 

The CI will then deploy the API and the Synchronizer to `k8s-prod` in the `caldera` namespace. You can verify the cache is filled by querying for the following endpoints and verifying they have no empty values: 

* https://caldera.dexter.exchage/liquidity
* https://caldera.dexter.exchange/volume

### Step III:
Once you have verified that the cache has been filled correctly you can retrigger the [caldera-frontend prod ci](https://app.buddy.works/camlcase/caldera-frontend/pipelines/pipeline/297773/executions). This will redeploy the latest commit on the master branch and take down the maintenance page. 

### Verifying the CI Worked
If you notice something not working correctly you may need to check the pods in the k8s-prod to see if they are in a `Running` state. 

```
➜  caldera git:(deploy_docs) ✗ kubectl get pods -n caldera
NAME                                      READY   STATUS    RESTARTS   AGE
caldera-api-deployment-74d6fdbdb7-ljm8l   0/1     Evicted   0          122m
caldera-api-deployment-74d6fdbdb7-zsbzm   1/1     Running   0          13m
caldera-sync-deployment-bbfcd9675-2wm72   1/1     Running   0          64m
caldera-sync-deployment-bbfcd9675-6qlln   0/1     Evicted   0          122m
```

You can then check the logs of a pod by running the following: 
```
kubectl logs -f caldera-sync-deployment-bbfcd9675-2wm72 -n caldera
```

If you would like to get the logs of a previously crashed pod you can do the following:
```
kubectl logs -f --previous aldera-sync-deployment-bbfcd9675-2wm72 -n caldera
```

### Rollback
If you are unable to solve the problem you will need to redo `Step I` and deploy the maintenance page for stats. Then you can run the [prod rollback](https://app.buddy.works/camlcase/caldera/pipelines/pipeline/317471/actions). Once you have successfully rolled back, you can redo `Step III`. 

# Deploying Caldera Manually

# Step 1: Build the Images

Take a look at what the current version of Caldera is by opening package.yml. You will want to tag the docker images with the correct version.

The following assumes the version says `0.18.0.0`. You can find the camlcasetezos credentials in Bitwarden.

## Dev
```
docker build -t camlcasetezos/caldera:v0.18.0.0-dev .
docker push camlcasetezos/caldera:v0.18.0.0-dev
```

## Prod
```
docker build -t camlcasetezos/caldera:v0.18.0.0 .
docker push camlcasetezos/caldera:v0.18.0.0
```

# Step 2: Deployment
In `./deployment` you will notice a bunch of kubernetes manifests files. You shouldn't need to mess with any of them with exclusion of the `api_deployment.yml` and `worker_deployment.yml` files. 

* `api-deployment.yml`: this file contains the deployment configuration for Caldera API. This will deploy the pod(s) which contain the docker container running the service. 
* `configmap.yml`: this file contains the Caldera config. It gets mounted to the worker and api deployments to be read in by the service. If you change this file you will need to redeploy the worker and the api.
* `ingress.yml`: This file contains the Ingress, DNS routing internally in kubernetes, you should only ever look at this file if there is a certificate issue.
* `service.yml`: This file connected the ingress to the pods running the API. 
* `worker-deployment`: This file contains the deployment for the synchronizer. Make sure to only ever run on replica of this deployment (its default).

In addition to the above file there is a `secrets.yml` file in our DevOps repository that contains the Postgres credentials that are needed for the Services to connect to RDS. You shouldn't need to mess with this either, but it's good to know it's there. 

## Dev
In `deployment/dev` open `api-deployment.yml` and replace the line that says image with the image pushed. 

```
image: camlcasetezos/caldera:v0.16.1.0
----->
image: camlcasetezos/caldera:v0.18.0.0-dev
```

Similarly replace the image in `worker-deployment` with the one pushed. 

```
image: camlcasetezos/caldera:v0.16.1.0
----->
image: camlcasetezos/caldera:v0.18.0.0-dev
```

If the changes in Caldera require a new database migration you will need to connect the `caldera_dev`, drop all the tables, and run the migrations. This will cause Caldera to sync from the begining. 

When you're ready to deploy verify that you are on the `k8s-dev` AWS cluster. Then do the following steps:
```
cd ./deployment/dev
kubectl apply -f api-deployment.yml
kubectl apply -f worker-deployment.yml
```

Verify that each service started up correctly by looking at the logs. Specifically check for psql errors. The API seems to not crash but doesn't retry and the synchronizer seems to crash over and over and self heal if it can't make the connection. It doesn't happen very often but it does you will need to delete the pod over and over until it establishes a connection. When you delete a pod it will automatically restart.

To get a list of pods:
```
kubectl get pods -n caldera-dev
```

To get the logs:
```
kubectl get logs -f <pod_name> -n caldera-dev
```
To delete a pod: 
```
kubectl delete pod <pod_name> -n caldera-dev
```

The API takes ~10m to fill the cache. If you don't get the right responses, wait a bit, it's probably just processing everything. 

## Prod
In `deployment/prod` open `api-deployment.yml` and replace the line that says image with the image pushed. 

```
image: camlcasetezos/caldera:v0.16.1.0
----->
image: camlcasetezos/caldera:v0.18.0.0
```

Similarly replace the image in `worker-deployment` with the one pushed. 

```
image: camlcasetezos/caldera:v0.16.1.0
----->
image: camlcasetezos/caldera:v0.18.0.0
```

If the changes in Caldera require a new database migration you will need to connect the `caldera`, drop all the tables, and run the migrations. This will cause Caldera to sync from the begining. 

When you're ready to deploy verify that you are on the `k8s-prod` AWS cluster. Then do the following steps:
```
cd ./deployment/dev
kubectl apply -f api-deployment.yml
kubectl apply -f worker-deployment.yml
```

Verify that each service started up correctly by looking at the logs. Specifically check for psql errors. The API seems to not crash but doesn't retry and the synchronizer seems to crash over and over and self heal if it can't make the connection. It doesn't happen very often but it does you will need to delete the pod over and over until it establishes a connection. When you delete a pod it will automatically restart.

To get a list of pods:
```
kubectl get pods -n caldera-dev
```

To get the logs:
```
kubectl get logs -f <pod_name> -n caldera-dev
```
To delete a pod: 
```
kubectl delete pod <pod_name> -n caldera-dev
```

The API takes ~10m to fill the cache. If you don't get the right responses, wait a bit, it's probably just processing everything. 
